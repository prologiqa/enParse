:-writeln("to start it from the black DOS window..."),
 writeln(">swipl -g debug(parser),parser:start('regression.txt'),halt boot.pl").

:- writeln("user:file_search_path(prolog,'..')").

%:- set_prolog_flag(autoload, false).

user:file_search_path(prolog,'../..').

%to denote an instance is belonging to a class
:- op(400,yfx,#).


:- set_prolog_flag(encoding,iso_latin_1).

%application dependent imports
:- use_module(enGen).
:- use_module(matchSyn).

%
:- use_module(enDict).

%general (library) imports
:- use_module(library(debug)).

:- debug(parser).

