:- module(enCheck,[checkAll/0]).

/** <module> Consistency checking operations
 *  for English Parser (enParser) text engine
 *  @author Kili�n Imre, 2018.
 */



:- use_module(enDict).


enMeta(D,S,det,det(S,D,_,_)).
enMeta(A,S,conj,conj(S,A)).
enMeta(O,NR,ord,ord(NR,O)).
enMeta(C,NR,card,card(C,NR)).
enMeta(I,S,indef,indef(S,I,_,_)).
enMeta(I,S,interr,interr(S,I)).
enMeta(I,S,interj,interj(S,I)).
enMeta(A,S,aux,aux(S,A,_)).
enMeta(A,S,aux,aux(S,A,_,_,_)).
enMeta(P,S,prep,prep(S,P)).
enMeta(A,S,adj,adj(S,A)).
enMeta(A,A,adjGr,adjGr(A,_,_)).
enMeta(A,S,adv,adv(S,_,A)).
enMeta(A,K,art,art(K,A)).
enMeta(P,S,proper,proper(S,P)).
enMeta(N,S,noun,noun(S,N)).
enMeta(N,S,noun,noun(S,N,_)).
enMeta(N,S,plnoun,plnoun(S,N)).
enMeta(V,S,verb,verb(S,V,_)).
enMeta(V,S,verb,verb(S,V,_,_)).
enMeta(V,S,verb,verb(S,V,_,_,_,_)).
enMeta(V,S,verb,verb(S,V,_,_,_,_,_,_)).
enMeta(PP,PP,persPron,persPron(PP,_,_,_,_)).
enMeta(RP,RP,reflPron,reflPron(RP,_,_)).



member(X,[X|_],Y,[Y|_]).
member(X,[_|L1],Y,[_|L2]):- member(X,L1,Y,L2).


anyNoun(NOUN,SEM):- enDict:noun(NOUN,SEM).
anyNoun(PLNOUN,SEM):- enDict:plnoun(PLNOUN,SEM).





root(N^NODE,N^_^EDGE):- NODE, \+ EDGE.

duplicateFact(FACT):-
    clause(FACT,true,REF), clause(FACT,true,REF1), REF\=REF1.

acyclic(N^NODE,X^Y^EDGE):-
    findall(N,NODE,NODES), findall(X-Y,EDGE,EDGES),
    vertices_edges_to_ugraph(NODES,EDGES,UGRAPH),
    top_sort(UGRAPH,_TOPSORT).


case(nom).
case(acc).
case(dat).

verbConnection(to).
verbConnection(ing).



enVerbFrameCorrect(CASE):- atom(CASE), case(CASE), !.
enVerbFrameCorrect(?(FRAME)):- !, enVerbFrameCorrect(FRAME).
enVerbFrameCorrect(EXPR):- EXPR=..[NAME,ARG], !,
    (enDict:prep(NAME,_), case(ARG)->true;
     ARG=verb, verbConnection(NAME)).
enVerbFrameCorrect(subclause).
word(SEM,WORD,adj):- adj(SEM,WORD).




enVerbArgError(VERB,FRAMES,VERB-FR-frameMistake):-
    member(FR,FRAMES), \+ enVerbFrameCorrect(FR).


enVerbArgSynType(noun).
enVerbArgSynType(verb).
enVerbArgSynType(subclause).


enVerbArgSynItemCorrect(SYN):- enVerbArgSynType(SYN), !.
enVerbArgSynItemCorrect(?(SYN)):- enVerbArgSynType(SYN), !.
enVerbArgSynItemCorrect(SYN):- is_list(SYN), !,
    forall(member(S,SYN), (atom(S), enVerbArgSynItemCorrect(S))).


enVerbArgError(VERB,SYNS,_,VERB-SYN-synArgMistake):-
    member(SYN,SYNS), \+ enVerbArgSynItemCorrect(SYN).


%checkEnLex
%Checks the following points in the English Grammatic KB
%- duplicated clauses
%- checking verb signatures (syntactic)
%
checkEnLex:-
    nl,nl,writeln('******** CHECKING English LEXICON ********'),nl,fail;

    writeln('Checking multiple lexical items:...'),
      findall(POS-EN,(enMeta(EN,_SEM,POS,CALL),
                         duplicateFact(enDict:CALL)),ERRORS),
      writeln(ERRORS), fail;
    writeln('Check syntactic frame signatures of English words:...'),
      findall(LABEL,(enMeta(EN,_SEM,verb,CALL),enDict:CALL,
                  arg(3,CALL,SYNS),
                  enVerbArgError(EN,SYNS,LABEL))
             ,LABELS),
      write('\t'),writeln(LABELS), fail;

    true.


checkEnBasicLines(STREAM):-
    repeat, read_line_to_codes(STREAM,CODES),
      (CODES=end_of_file, !;
      CODES=[C|ODES], char_type(CUP,to_upper(C)),
       char_type(CLO,to_lower(C)), atom_codes(ODES_,ODES),
       atom_concat(CLO,ODES_,LOWORD), atom_concat(CUP,ODES_,UPWORD),
       \+ enDict:word(_,LOWORD), \+ enDict:word(_,UPWORD),
       atom_codes(WORD,CODES), write(WORD), write(' '), fail).

checkEnBasic(FILE):-
    nl,nl,writeln('******** CHECKING Basic-English Words ********'),nl,fail;
    writeln('******** Missing Basic-English Words ********'),nl,fail;
    open(FILE,read,IN), write('[ '),
      checkEnBasicLines(IN), writeln(' ]'), close(IN).


checkAll:-
    checkEnLex, fail;
    checkEnBasic('basicEnglish.txt'), fail;
    true.

























