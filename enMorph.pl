:- module(enMorph, [
              morphAnal/2
          ]).


%:- use_module(enDict).
%dynamic import - module should be explicitely loaded by boot
%and here only the module name is used (instead of file name)
:- add_import_module(enMorph,enDict,start).

:- use_module(library(lists)).

kind2FormNumPers(sg/3,inf,sg,3):- !.
kind2FormNumPers(KIND,KIND,_,_).

%verb: NUM _ ; sg/3... if sg/3==>FORM=inf
%verb:FORM inf;past;perfect;past(participle)
%morphAnal(+WORD,?INF).
%+WORD form, being read from input
%?INF abstract form
%
%
%measurement unit abbreviation
morphAnal(ABBR,measure(ABBR,QUANT,abbr)):- measure(_,ABBR,QUANT,_).

%infinite particle ('to')
morphAnal(PART,inf(PART)):- infPart(PART).

%ordinal tag ('st';'nd';'rd';'th')
morphAnal(TAG,tag(TAG)):- enDict:ordTag(TAG).
%ordinal with '.'
morphAnal(ORD,ord(NR)):- sub_atom(ORD,_,1,0,'.'),
    atom_concat(CARD,'.',ORD), atom_number(CARD,NR), !.
%cardinal digits as characters ('123')
morphAnal(CARD,card(CARD,NR)):-
    atom(CARD), atom_number(CARD,NR), !.
%ordinal literally ('first'...'seventh', etc.)
morphAnal(ORD,ord(NR)):-
    ord(ORD,NR).
%cardinal literally
morphAnal(CARD,card(CARD,NR)):-
    card(CARD,NR).
%cardinal as a number
morphAnal(INT,card(INT,INT)):-
    integer(INT), !.
%indefinite numerals
morphAnal(INDEF,indef(INDEF,NUM,COUNT,KIND)):-
    indef(INDEF,INDEF,NUM,COUNT,KIND).


%common nouns (single nouns or consisting of at most three tags)
morphAnal(WORD,common(STEM,NUM,CASE)):-
    noun_(STEM,NUM,CASE,WORD,_).
morphAnal(WORD,common(WORD,NUM,CASE,1,[WORD|ST])):-
    noun_(_STEM,NUM,CASE,[WORD|ST],_).
morphAnal(WORD,common(WORD,NUM,CASE,2,[W1,WORD|ST])):-
    noun_(_STEM,NUM,CASE,[W1,WORD|ST],_).
morphAnal(WORD,common(WORD,NUM,CASE,3,[W1,W2,WORD|ST])):-
    noun_(_STEM,NUM,CASE,[W1,W2,WORD|ST],_).
%nouns (consisting of 2 tags, detached by a dash
morphAnal(WORD,common(WORD,NUM,CASE,1,WORD-W)):-
    noun_(_STEM,NUM,CASE,WORD-W,_).
morphAnal(WORD,common(WORD,NUM,CASE,2,W-WORD)):-
    noun_(_STEM,NUM,CASE,W-WORD,_).


%proper nouns (single nouns or consisting of at most three tags)
%connected maybe by dashes, eg. T-shirt
%morphAnal('Eyeore'==>proper('Eyeore',donkey,sg,_))
morphAnal(WORD,proper(STEM,CLASS,NUM,CASE)):-
    proper_(STEM,NUM,CASE,WORD,CLASS,_).
% morphAnal('Winnie'==>
%           proper('Winnie',bear,sg,_,1,['Winnie',the,'Pooh'])).
morphAnal(WORD,proper(WORD,CLASS,NUM,CASE,1,[WORD|ST])):-
    proper_(_STEM,NUM,CASE,[WORD|ST],CLASS,_).
morphAnal(WORD,proper(W,CLASS,NUM,?,2,[W1,W])):-
    atom(WORD), atom_concat(W,'\'',WORD),
    proper_(_STEM,NUM,_,[W1,W],CLASS,_).
morphAnal(WORD,proper(WORD,CLASS,NUM,CASE,2,[W1,WORD|ST])):-
    proper_(_STEM,NUM,CASE,[W1,WORD|ST],CLASS,_).
morphAnal(WORD,proper(W,CLASS,NUM,?,3,[W1,W2,W])):-
    atom(WORD), atom_concat(W,'\'',WORD),
    proper_(_STEM,NUM,_,[W1,W2,W],CLASS,_).
morphAnal(WORD,proper(WORD,CLASS,NUM,CASE,3,[W1,W2,WORD|ST])):-
    proper_(_STEM,NUM,CASE,[W1,W2,WORD|ST],CLASS,_).

%personal pronouns (I, you, hers, etc.)
morphAnal(WORD,pronoun(NUMPERS,SEX,CASE)):-
    persPronoun(NUMPERS,SEX,CASE,WORD).

%reflexive pronouns (myself, themselves etc.)
morphAnal(WORD,refl(NUMPERS,SEX)):-
    reflPronoun(NUMPERS,SEX,WORD).


%indefinite pronouns (someone, one, this etc.)
%indefPronoun(SEM,NUM,SQ) - SEMantics, NUMber, SQ: singlequoted
morphAnal(WORD,indefPronoun(SEM,NUM,false)):-
    indefPronoun(SEM,WORD,NUM).
morphAnal(WORD,indefPronoun(WORD,NUM,1,[WORD|LIST])):-
    indefPronoun(_SEM,[WORD|LIST],NUM).
morphAnal(WORD,indefPronoun(WORD,NUM,2,[W1,WORD|LIST])):-
    indefPronoun(_SEM,[W1,WORD|LIST],NUM).
morphAnal(WORD,indefPronoun(SEM,NUM,true)):-
    atom_concat(INDEF,'\'',WORD), indefPronoun(SEM,INDEF,NUM).


%verbs with particles
morphAnal(WORD,verb(STEM,FORM,NUM/PERS)):-
% %%supposing, there are no verbal homonyms (different forms)
    verb_(STEM,KIND,NUM/PERS,WORD,_),
    kind2FormNumPers(KIND,FORM,NUM,PERS).
%modal verb particles (can, must, ought to...)
morphAnal(MODAL,modal(MODAL,MODE,present,FR)):-
    modal(MODAL,_,MODE,FR).
morphAnal(MODPAST,modal(MODAL,MODE,past,FR)):-
    modal(MODAL,MODPAST,MODE,FR).
morphAnal(MODAL,modal(MODAL,MODE,past,FR)):-
    modal(MODAL,MODE,FR).


%adjectives (consisting of at most of 3 tags)
morphAnal(WORD,adj(ADJ,GRAD)):-
    adj_(ADJ,GRAD,WORD,_).
morphAnal(WORD,adj(WORD,GRAD,1,[WORD|LIST])):-
    adj_(_ADJ,GRAD,[WORD|LIST],_).
morphAnal(WORD,adj(WORD,GRAD,2,[W1,WORD|LIST])):-
    adj_(_ADJ,GRAD,[W1,WORD|LIST],_).
morphAnal(WORD,adj(WORD,GRAD,3,[W1,W2,WORD|LIST])):-
    adj_(_ADJ,GRAD,[W1,W2,WORD|LIST],_).
%adjectives (consisting of 2 tags, detached by a dash
morphAnal(WORD,adj(WORD,GRAD,1,WORD-W)):-
    adj_(_ADJ,GRAD,WORD-W,_).
morphAnal(WORD,adj(WORD,GRAD,2,W-WORD)):-
    adj_(_ADJ,GRAD,W-WORD,_).


%adverbs
morphAnal(ADV,adv(ADV,KIND,GRADE)):-
    adv_(_,KIND,GRADE,ADV).
morphAnal(WORD,adv(WORD,KIND,GRADE,1,[WORD|LIST])):-
    adv_(_ADJ,KIND,GRADE,[WORD|LIST]).
morphAnal(WORD,adv(WORD,KIND,GRADE,2,[W1,WORD|LIST])):-
    adv_(_ADJ,KIND,GRADE,[W1,WORD|LIST]).
morphAnal(WORD,adv(WORD,KIND,GRADE,3,[W1,W2,WORD|LIST])):-
    adv_(_ADV,KIND,GRADE,[W1,W2,WORD|LIST]).


%articles
morphAnal(ART,art(ART,NUM,KIND)):-
    art(KIND,ART,NUM).

%determiners
morphAnal(DET,det(DET,NUM,KIND)):-
    det(DET,DET,NUM,KIND).
morphAnal(WORD,det(WORD,NUM,NUM0,KIND,1,[WORD|LIST])):-
    det(_,NUM,[WORD|LIST],NUM0,KIND).
morphAnal(WORD,det(WORD,NUM,NUM0,KIND,2,[W1,WORD|LIST])):-
    det(_,NUM,[W1,WORD|LIST],NUM0,KIND).
morphAnal(WORD,det(WORD,NUM,NUM0,KIND,3,[W1,W2,WORD|LIST])):-
    det(_,NUM,[W1,W2,WORD|LIST],NUM0,KIND).
morphAnal(WORD,det(WORD,NUM,NUM0,KIND,4,[W1,W2,W4,WORD|LIST])):-
    det(_,NUM,[W1,W2,W4,WORD|LIST],NUM0,KIND).


%interrogatives
morphAnal(INTERR,interr(SEM,KIND)):-
    interr(SEM,INTERR,KIND).
morphAnal(WORD,interr(WORD,KIND,N,[LI|ST])):-
    interr(_SEM,[LI|ST],KIND), nth1(N,[LI|ST],WORD).

%prepositions, postpositions
morphAnal(PREP,prep(PREP)):-
    prep(_,PREP).
morphAnal(POSTP,postp(POSTP)):-
    postp(_,POSTP).

morphAnal(CONJ,conj(CONJ,KIND)):-
    conj(_,CONJ,KIND).
morphAnal(INTERJ,interj(INTERJ,IJ)):-
    interj(INTERJ,IJ).

morphAnal(s,token(s)).















