persPron(pl/1,_,we,us,our,ours).
persPron(_NUM/2,_,you,you,your,yours).
persPron(pl/3,_,they,them,their,theirs).
persPron(sg/1,_,'I',me,my,mine).
persPron(sg/3,femaleSex,she,her,her,hers).
persPron(sg/3,maleSex,he,him,his,his).
persPron(sg/3,neutral,it,it,its,its).

reflPron(pl/1,_,ourselves).
reflPron(pl/2,_,yourselves).
reflPron(pl/3,_,themselves).
reflPron(sg/1,_,myself).
reflPron(sg/2,_,yourself).
reflPron(sg/3,femaleSex,herself).
reflPron(sg/3,maleSex,himself).
reflPron(sg/3,neutral,itself).


infPart(to).

prep(about,about).
prep(above,above).
prep(across,across).
prep(after,after).
prep(against,against).
prep(ago,ago).
prep(along,along).
prep(among,among).
prep(around,around).
prep(as,as).
prep(at,at).
prep(before,before).
prep(behind,behind).
prep(below,below).
prep(between,between).
prep(before,before).
prep(behind,behind).
prep(below,below).
prep(beside,beside).
prep(between,between).
prep(by,by).
prep(down,down).
prep(during,during).
prep(except,except).
prep(for,for).
prep(from,from).
prep(in,in).
prep(including,including).
prep(inside,inside).
prep(into,into).
prep(like,like).
prep(minus,minus).
prep(near,near).
prep(of,of).
prep(on,on).
prep(opposite,opposite).
prep(outside,outside).
prep(over,over).
prep(past,past).
prep(per,per).
prep(plus,plus).
prep(round,round).
prep(since,since).
prep(through,through).
prep(till,till).
prep(to,to).
prep(under,under).
prep(until,until).
prep(up,up).
prep(versus,versus).
prep(with,with).
prep(without,without).

postp(ago,ago).
postp(away,away).

art(def,the,_). %the Watsons / the car
art(indef,a,_). %an apple/a few minutes
art(indef,an,_).


%measure(NAME,ABBR,QUANTITY,ADJS)
measure(degree,'',temperature,[cold,warm,hot]).
measure(dollar,$,money,[expensive,cheap]).
measure(meter,m,length,[long,short]).
measure(metre,m,length,[long,short]).
measure(kilometer,km,distance,[far]).
measure(kilometre,km,distance,[far]).
measure(hour,h,time,[long,short]).
measure(minute,min,time,[long,short]).
measure(year,'',time,[old,long]).
measure([of,the,clock],'',time,[late]).
measure(gigabyte,'GB',memory,[]).


%independent/coordinating conjunctions
conj(and,and,coordinating). %the combination of two similar thoughts
conj(but,but,coordinating). %contrasting between two thoughts
conj(so,so,coordinating).   %logical implication of two thoughts
conj(yet,yet,coordinating). %contrasting between two thoughts
conj(or,or,coordinating).   %indicating alternative thoughts
conj(nor,nor,coordinating). %when neither thought is true


% both...and...
% either...or... sentence either you will eat your dinner of you will go
% to bed
% hardly...when... He had hardly begun to work when he was interrupted
% just as...so...
% neither...nor... INVERTED
% no sooner...INVERTED than...
% No sooner had I received the corner than the bus came
% Scarcely... INVERTED... than...
%
% not only...INVERTED... but also...
% Not only do I love this band, but I have also seen the concert twice.
% rather...or... whether...or...

%if...then...  wo 'then' is applicable too...
%If that is true, then what happened is not surprising.


%correlative conjunctions
correlative(noSoonerThan,no,sooner,inverse,than,straight).

correlative(eitherOr,either,straight,or,straight).
correlative(neitherNor,neither,straight,nor,inverse).
correlative(ifThen,if,straight,then,straight).
correlative(firstThen,first,straight,then,straight).
correlative(scarcelyWhen,scarcely,inverse,when,straight).

correlative(eitherOr,either,or,nounPhrase).
correlative(neitherNor,neither,nor,nounPhrase).
correlative(bothAnd,both,and,nounPhrase).

%correlative(ifThen,if,straight,then,straight).
%correlative(scarcelyWhen,scarcely,inverse,when,straight).

conj(bothAnd,both,and,coordinating,phrase).

conj(that,that,subclause(that)).
conj(if,if,subclause(if)).
conj(whether,whether,subclause(if)).


%subordinating conjunction
conj(so,so,subordinating).   %the second follows from the first
conj(because,because,subordinating). %the first follows from the second
conj(for,for,subordinating). %the first follows from the second
conj(if,if,subordinating).
conj(when,when,subordinating).
conj(while,while,subordinating).
conj(although,although,subordinating).
conj(as,as,subordinating).



%comparison conjunction
conj(than,than,comp).
conj(as,as,comp).


conj(or,or,corr).   %indicating alternative thoughts
conj(nor,nor,corr). %when neither thought is true
conj(either,either,corr).
conj(neither,neither,corr).
conj(if,if,corr).   %indicating alternative thoughts
conj(then,then,corr).   %indicating alternative thoughts
conj(scarcely,scarcely,corr).
conj(when,when,corr).
conj(both,both,corr).
conj(and,and,corr).


%conj(please,please).


%demonstratives
det(this,this,sg,numNP).
det(that,that,sg,numNP).
det(these,these,pl,numNP).
det(those,those,pl,numNP).
det(enough,enough,sg,numNP).
det(certain,certain,pl,numNP).
det(other,other,pl,numNP).
det(another,another,sg,numNP).


det(theWholeOf,sg,[the,whole,of],_,possNP).
det(allThe,sg,[all,the],_,possNP).
det(fewOf,pl,[few,of],pl,possNP).
det(alotOf,pl,[a,lot,of],_,possNP).
det(lotsOf,pl,[lots,of],pl,possNP).
det(neitherOf,pl,[neither,of],pl,possNP).
det(noneOf,pl,[none,of],pl,possNP).
det(eitherOf,sg,[either,of],pl,possNP).
det(anyOf,sg,[any,of],pl,possNP).
det(coupleOf,pl,[a,couple,of],pl,possNP).
det(tonsOf,pl,[tons,of],pl,possNP).
det(plentyOf,pl,[plenty,of],sg,possNP).
det(bitOf,sg,[a,bit,of],sg,possNP).
det(numberOf,pl,[a,number,of],pl,possNP).
det(pairOf,pl,[a,pair,of],pl,possNP).
det(greatDealOf,sg,[a,great,deal,of],sg,possNP).
det(suchA,sg,[such,a],sg,possNP).
det(whatA,sg,[what,a],sg,possNP).
det(quiteA,sg,[quite,a],sg,possNP).
det(ratherA,sg,[rather,a],sg,possNP).


indef(all,all,_,count,possNP).
indef(each,each,_,count,adjNP).
indef(some,some,_,count,adjNP).
indef(many,many,pl,count,adjNP).
indef(much,much,sg,uncount,adjNP).
indef(more,more,sg,_,adjNP).
indef(most,most,pl,_,adjNP).
indef(any,any,_,_,adjNP).
indef(few,few,_,_,adjNP).
indef(fewer,fewer,_,_,adjNP).
indef(fewest,fewest,sg,_,adjNP).
indef(little,little,sg,uncount,adjNP).
indef(less,less,_,_,adjNP).
indef(least,least,sg,_,adjNP).
indef(every,every,sg,count,adjNP).
indef(several,several,pl,count,adjNP).
indef(no,no,_,_,adjNP).

indef(both,both,pl,count,possNP).
indef(half,half,sg,count,possNP).


indefPronoun(someone,someone,sg).
indefPronoun(somebody,somebody,sg).
indefPronoun(something,something,sg).
indefPronoun(anyone,anyone,sg).
indefPronoun(anybody,anybody,sg).
indefPronoun(anything,anything,sg).
indefPronoun(everyone,everyone,pl).
indefPronoun(everybody,everybody,pl).
indefPronoun(everything,everything,pl).
indefPronoun(nobody,nobody,sg).
indefPronoun(nothing,nothing,sg).
indefPronoun(many,many,pl).
indefPronoun(some,some,pl).
indefPronoun(each,each,pl).
indefPronoun(few,few,pl).
indefPronoun(aFew,[a,few],pl).
indefPronoun(any,any,pl).
indefPronoun(all,all,pl).
indefPronoun(lot,lot,sg).
indefPronoun(aLot,[a,lot],sg).
indefPronoun(one,one,sg).
indefPronoun(this,this,sg).
indefPronoun(that,that,sg).
indefPronoun(theSame,[the,same],sg).


interj(hello,hello).
interj(please,please).
interj(no,no).
interj(ok,ok).
interj(oh,oh).
interj(whoa,whoa).
interj(wow,wow).
interj(yes,yes).

interr(how,how,adv(manner)).
interr(what,what,_). %it can be nom or acc as well
interr(when,when,adv(time)).
interr(where,where,adv(place)).
interr(which,which,adj). %which shoes
interr(whom,whom,acc).
interr(who,who,nom).
interr(why,why,adv(reason)).

interr(howMany,[how,many],num).
interr(howMuch,[how,much],num).
interr(whatAbout,[what,about],pred).


ord(first,1).
ord(second,2).
ord(third,3).
ord(fourth,4).
ord(fifth,5).

card(one,1).
card(two,2).
card(three,3).
card(four,4).
card(five,5).
card(six,6).
card(seven,7).
card(eight,8).
card(nine,9).
card(ten,10).
card(eleven,11).
card(twelve,12).
card(twenty,20).
card(thirty,30).
card(fourty,40).
card(fifty,50).
card(sixty,60).
card(seventy,70).
card(eighty,80).
card(ninety,90).
card(houndred,100).
card(thousand,1000).
card(million,1000000).
card(billion,1000000000).



adj(able,able).
adj(acid,acid).
adj(academic,academic).
adj(accidental,accidental).
adj(accurate,accurate).
adj(adult,adult).
adj(advanced,advanced).
adj(afraid,afraid).
adj(aged,aged).
adj(alone,alone).
adj(alright,alright).
adj(amazing,amazing).
adj(american,'American').
adj(angry,angry).
%adj(apparent,apparent).
%adj(applicable,applicable).
%adj(approved,approved).
%adj(approximate,approximate).
adj(asian,'Asian').
adj(attractive,attractive).
%adj(automatic,automatic).
%adj(auxiliary,auxiliary).
adj(available,available).
%adj(average,average).
%adj(awake,awake).
adj(awesome,awesome).
adj(awful,awful).


adj(back,back).
adj(bad,bad).
%adj(basic,basic).
adj(beautiful,beautiful).
adj(big,big).
%adj(bitter,bitter).
adj(black,black).
adj(blond,blond).
%adj(blocked,blocked).
adj(blue,blue).
adj(blunt,blunt).
%%adj(boiled,boiled)... past-participle
adj(bored,bored).
adj(boring,boring).
adj(born,born).
adj(brave,brave).
adj(bright,bright).
adj(brilliant,brilliant).
%%adj(broken,broken). PP
adj(brown,brown).
adj(busy,busy).


adj(careful,careful).
%adj(certain,certain).
adj(cheap,cheap).
adj(chemical,chemical).
adj(chief,chief).
adj(circular,circular).
adj(clean,clean).
adj(clear,clear).
adj(clever,clever).
adj(close,close).
%%adj(closed,closed). PP
adj(cloudy,cloudy).
%adj(clueless,clueless).
adj(cold,cold).
%adj(colorful,colorful).
adj(comfortable,comfortable).
%adj(common,common).
%adj(comparable,comparable).
%adj(complete,complete).
%adj(complex,complex).
%adj(confined,confined).
%adj(conscious,conscious).
%adj(constant,constant).
%adj(continuous,continuous).
adj(cool,cool).
adj(correct,correct).
%adj(cruel,cruel).
%adj(curious,curious).
adj(crazy,crazy).
adj(cream,cream).
adj(crowded,crowded).
%adj(current,current).


adj(daily,daily).
%adj(damaged,damaged).
adj(dangerous,dangerous).
adj(dark,dark).
adj(dead,dead).
%adj(deaf,deaf).
adj(dear,dear).
adj(deep,deep).
%adj(defective,defective).
%adj(delicate,delicate).
adj(delicious,delicious).
%adj(dependent,dependent).
%adj(diagonal,diagonal).
adj(different,different).
adj(difficult,difficult).
adj(digital,digital).
%adj(dim,dim).
adj(dirty,dirty).
adj(diving,diving).
%adj(divorced,divorced).
%adj(dizzy,dizzy).
adj(double,double).
%%adj(dressed,dressed). PP
adj(dry,dry).


adj(early,early).
adj(east,east).
adj(easy,easy).
%adj(elastic,elastic).
adj(electric,electric).
%adj(electronic,electronic).
adj(empty,empty).
adj(english,'English').
%adj(equal,equal).
adj(european,'European').
adj(excellent,excellent).
adj(excited,excited).
adj(exciting,exciting).
%adj(expanded,expanded).
adj(expensive,expensive).
%adj(experienced,experienced).
%adj(expert,expert).
%adj(expired,expired).
%adj(explosive,explosive).
%adj(external,external).
adj(extra,extra).


adj(fair,fair).
%adj(false,false).
%adj(familiar,familiar).
adj(famous,famous).
adj(fantastic,fantastic).
%adj(fancy,fancy).
adj(far,far).
adj(fast,fast).
adj(fat,fat).
adj(favorite,favorite).
adj(favorite,favourite).
%adj(feeble,feeble). %HU: gyenge
adj(female,female).
adj(fertile,fertile).
adj(few,few).
adj(final,final).
adj(fine,fine).
adj(firstClass,first-class).
adj(fixed,fixed).
adj(fit,fit).
%adj(flat,flat).
%adj(flexible,flexible).
adj(foggy,foggy).
%adj(foolish,foolish).
%adj(fond,[fond,of]).
adj(foreign,foreign).
adj(free,free).
adj(fresh,fresh).
%adj(frequent,frequent).
%%adj(fried,fried). PP
adj(friendly,friendly).
adj(frightened,frightened).
%adj(front,front).
%%adj(frozen,frozen). PP
adj(full,full).
adj(funny,funny).
adj(further,further).
%adj(future,future).


%adj(general,general).
adj(glad,glad).
adj(glass,glass).
adj(gold,gold).
adj(golden,golden).
adj(good,good).
adj(goodlooking,[good,looking]).
adj(great,great).
adj(green,green).
adj(grey,grey).
adj(grilled,grilled).
%adj(gross,gross).


adj(halfprice,[half,price]).
adj(happy,happy).
adj(hard,hard).
adj(healthy,healthy).
adj(heavy,heavy).
adj(high,high).
%adj(hollow,hollow). %HU:�reges
adj(honest,honest).
adj(horrible,horrible).
adj(horror,horror).
adj(hot,hot).
adj(hungry,hungry).
adj(hurt,hurt).


adj(ill,ill).
adj(important,important).
%adj(impossible,impossible).
adj(indoor,indoor).
%adj(included,included).
%adj(incorrect,incorrect).
%adj(independent,independent).
%adj(initial,initial).
adj(interested,interested).
adj(interesting,interesting).
%adj(internal,internal).
%adj(international,international).
%adj(irregular,irregular).
%
%
%adj(kind,kind).
%%adj(known,known). PP


adj(large,large).
adj(last,last).
adj(late,late).
adj(lazy,lazy).
adj(leather,leather).
adj(left,left).
adj(leftHand,left-hand).
adj(light,light).
%adj(liquid,liquid).
adj(little,little).
adj(local,local).
%adj(lonely,lonely).
adj(long,long).
%adj(loose,loose).
adj(lost,lost).
adj(loud,loud).
%adj(lousy,lousy).
adj(lovely,lovely).
adj(low,low).
adj(lucky,lucky).


adj(mad,mad).
%adj(male,male).
%adj(mandatory,mandatory).
%adj(manual,manual).
%%adj(married,married). PP
%adj(material,material).
%adj(medical,medical).
adj(metal,metal).
%adj(middle,middle).
%adj(military,military).
adj(missing,missing).
%adj(mixed,mixed).
adj(modern,modern).
adj(monthly,monthly).
adj(musical,musical).


%adj(narrow,narrow).
adj(national,national).
%adj(natural,natural).
%adj(near,near).
%adj(nearby,nearby).
%adj(necessary,necessary).
%adj(negative,negative).
%adj(net,net).
adj(new,new).
adj(next,next).
adj(nice,nice).
adj(noisy,noisy).
adj(normal,normal).
adj(north,north).


%adj(odd,odd).
%adj(offline,offline).
adj(old,old).
adj(oneWay,one-way).
adj(online,online).
adj(only,only).
adj(open,open).
%adj(opposite,opposite).
%adj(optional,optional).
adj(orange,orange).
adj(outdoor,outdoor).
%adj(outer,outer).
%adj(overall,overall).
adj(own,own).


adj(pale,pale).
adj(paper,paper).
%adj(parallel,parallel).
%adj(past,past).
adj(perfect,perfect).
%adj(permanent,permanent).
%adj(personal,personal).
%adj(physical,physical).
adj(pink,pink).
adj(plastic,plastic).
adj(pleasant,pleasant).
adj(pleased,pleased).
adj(polite,polite).
%adj(political,political).
adj(poor,poor).
adj(popular,popular).
adj(possible,possible).
%adj(positive,positive).
%adj(pregnant,pregnant).
%adj(present,present).
%adj(preserved,preserved).
adj(pretty,pretty).
%adj(primary,primary).
%adj(private,private).
%adj(probable,probable).
%adj(professional,professional).
%adj(poisonous,poisonous).
%adj(positive,positive).
%adj(possible,possible).
%adj(public,public).
adj(purple,purple).


adj(quick,quick).
adj(quiet,quiet).


%adj(rare,rare).
adj(ready,ready).
adj(real,real).
%adj(rear,rear).
%adj(recent,recent).
adj(red,red).
%adj(regular,regular).
%adj(related,related).
%adj(remaining,remaining).
%adj(rescued,rescued).
%adj(resistant,resistant).
%adj(responsible,responsible).
adj(rich,rich).
adj(right,right).
adj(rightHand,right-hand).
adj(roast,roast).
%adj(rough,rough). %HU:durva
adj(round,round). %HU:kerek
%adj(rusty,rusty).
%
%
adj(sad,sad).
adj(safe,safe).
adj(same,same).
%adj(satisfactory,satisfactory).
%%adj(saved,saved). PP
adj(scared,scared).
adj(scary,scary).
%adj(secondary,secondary).
%adj(secret,secret).
%adj(senior,senior).
%adj(sensitive,sensitive).
%adj(separate,separate).
%adj(serious,serious).
%adj(sharp,sharp).
adj(short,short).
%adj(shiny,shiny).
%adj(shut,shut).
adj(sick,sick).
adj(silver,silver).
adj(simple,simple).
adj(single,single).
adj(slim,slim).
adj(slow,slow).
adj(small,small).
%adj(smart,smart).
%adj(smooth,smooth).
%adj(social,social).
adj(soft,soft).
%adj(solid,solid).
adj(sorry,sorry).
%adj(sour,sour).
adj(south,south).
adj(spare,spare).
adj(special,special).
%adj(specific,specific).
%adj(specified,specified).
%adj(spherical,spherical).
adj(square,square).
%adj(sticky,sticky). %HU:ragad�s
%adj(stable,stable).
adj(straight,straight). %HU: egyenes
adj(strange,strange). %HU furcsa, k�l�n�s
%adj(stressed,stressed).
adj(striped,striped).
adj(strong,strong).
%adj(structural,structural).
%adj(stupid,stupid).
adj(successful,successful).
%adj(sudden,sudden).
adj(sunny,sunny).
adj(sure,sure).
adj(surprised,surprised).
%adj(symmetrical,symmetrical).
%adj(synchronzed,synchronized).
adj(sweet,sweet).


adj(tall,tall).
%adj(temporary,temporary).
adj(terrible,terrible).
%adj(tertiary,tertiary).
%adj(thick,thick). %HU: vastag
adj(thin,thin).
adj(thirsty,thirsty).
adj(tidy,tidy).
%adj(tight,tight).
adj(tired,tired).
adj(total,total).
%adj(transparent,transparent).
adj(true,true).


%adj(ugly,ugly).
adj(underground,underground).
adj(unfortunate,unfortunate).
%adj(unsatisfactory,unsatisfactory).
%adj(unservicable,unservicable).
%adj(unknown,unknown).
adj(unusual,unusual).
%adj(unwanted,unwanted).
adj(upset,upset).
adj(useful,useful).
adj(usual,usual).


adj(various,various).
%adj(vertical,vertical).
%adj(visual,visual).
%adj(violent,violent).


adj(warm,warm).
%adj(weak,weak).
%adj(weird,weird).
adj(weekly,weekly).
adj(welcome,welcome).
adj(west,west).
adj(wet,wet).
adj(white,white).
adj(whole,whole).
adj(wide,wide). %HU:sz�les
adj(wise,wise). %HU:b�lcs
adj(wild,wild).
adj(windy,windy).
adj(wonderful,wonderful).
adj(wooden,wooden).
adj(worried,worried).
adj(wrong,wrong).


adj(yellow,yellow).
adj(young,young).

adjGr(bad,worse,worst).
adjGr(good,better,best).
adjGr(big,bigger,biggest).

adjKind(white,color).
adjKind(black,color).
adjKind(green,color).
adjKind(blue,color).
adjKind(red,color).
adjKind(brown,color).
adjKind(yellow,color).
adjKind(grey,color).

%adv(east,place,east).
%adv(north,place,north).
%adv(south,place,south).
%adv(west,place,west).

advGr(badly,worse,worst).
advGr(far,farther,farthest).
advGr(far,further,furthest).
advGr(little,less,least).
advGr(well,better,best).


adv(about,degree,about).
adv(above,place,above).
adv(abroad,place,abroad).
%adv(absolutely,degree,absolutely).
%adv(accidentally,manner,accidentally).
%adv(accurately,manner,accurately).
adv(across,place,across).
adv(actually,freq,actually).
adv(after,time,after).
adv(afterwards,time,afterwards).
adv(again,time,again).
adv(ago,time,ago).
%adv(all,degree,all).
adv(allDay,time,[all,day]).
adv(allRight,manner,[all,right]).
adv(almost,freq,almost).
adv(almost,degree,almost).
adv(alone,manner,alone).
adv(already,time,already).
adv(allRight,manner,alright).
adv(also,link,also).
%adv(alternatively,manner,alternatively).
adv(always,freq,always).
%adv(amazingly,manner,amazingly).
%adv(angrily,manner,angrily).
adv(anymore,time,anymore).
adv(anyway,focus,anyway).
adv(anywhere,place,anywhere).
%adv(apart,place,apart).
%adv(apparently,manner,apparently).
%adv(approximately,degree,approximately).
adv(around,degree,around).
adv(asWell,link,[as,well]).
%adv(automatically,manner,automatically).
%adv(averagely,manner,averagely).
adv(away,place,away).
%adv(awefully,degree,awefully).
%adv(awesomely,manner,awesomely).
adv(back,place,back).
adv(badly,manner,badly).
%adv(basically,manner,basically).
adv(before,time,before).
adv(behind,place,behind).
adv(below,place,below).
%adv(brightly,manner,brightly).
adv(carefully,manner,carefully).
adv(certainly,prob,certainly).
adv(clearly,manner,clearly).
%adv(clockwise,place,clockwise).
%adv(comfortably,manner,comfortably).
%adv(completely,degree,completely).
%adv(consequently,link,consequently).
%adv(constantly,degree,constantly).
%adv(continuously,manner,continuously).
%adv(correctly,manner,correctly).
%adv(counterclockwise,place,counterclockwise).
%adv(dangerously,manner,dangerously).
adv(daily,time,daily).
%adv(definitely,prob,definitely).
%adv(diagonally,place,diagonally).
%adv(differently,degree,differently).
%adv(dimly,manner,dimly).
%adv(directly,manner,directly).
adv(down,place,down).
adv(downstairs,place,downstairs).
adv(early,time,early).
adv(easily,manner,easily).
adv(east,place,east).
%adv(electrically,manner,electrically).
adv(else,manner,else).
%adv(emptily,manner,emptily).
adv(enough,degree,enough).
%adv(entirely,degree,entirely).
%adv(equally,degree,equally).
adv(especially,focus,especially).
adv(even,focus,even).
adv(ever,freq,ever).
adv(everywhere,place,everywhere).
adv(exactly,manner,exactly).
%adv(extremely,degree,extremely).
%adv(externally,manner,externally).
%adv(fairly,degree,fairly).
adv(far,place,far).
adv(fast,manner,fast).
adv(finally,time,finally).
%adv(finely,manner,finely).
adv(first,time,first).
%adv(forward,place,forward).
%adv(freely,manner,freely).
%adv(fully,manner,fully).
%adv(funnily,manner,funnily).
%adv(generally,freq,generally).
%adv(happily,manner,happily).
adv(hard,manner,hard).
%adv(hardly,freq,hardly).
%adv(heavily,manner,heavily).
adv(here,place,here).
%adv(highly,degree,highly).
adv(home,place,home).
adv(how,manner,how).
adv(however,link,however).
adv(immediately,manner,immediately).
adv(in,place,in).
%adv(importantly,manner,importantly).
%adv(independently,manner,independently).
adv(indoors,place,indoors).
adv(inside,place,inside).
adv(instead,manner,instead).
%adv(initially,manner,initially).
adv(just,focus,just).
%adv(largely,focus,largely).
%adv(lastpast,time,last).
%adv(lastYear,time,[last,year]).
adv(late,time,late).
%adv(lately,time,lately).
adv(later,time,later).
adv(least,super,least).
adv(left,place,left).
adv(less,comp,less).
adv(like,manner,like).
adv(long,time,long).
%adv(loosely,manner,loosely).
%adv(lots,degree,lots).
%adv(mainly,focus,mainly).
%adv(manually,manner,manually).
adv(maybe,prob,maybe).
%adv(moderately,manner,moderately).
%adv(momentarily,time,momentarily).
adv(monthly,time,monthly).
%adv(more,degree,more).
adv(more,comp,more).
adv(most,super,most).
adv(much,degree,much).
adv(near,place,near).
adv(nearly,degree,nearly).
adv(nearly,freq,nearly).
adv(never,freq,never).
adv(next,place,next).
%adv(newly,manner,newly).
%adv(nicely,manner,nicely).
adv(no,neg,no).
adv(north,place,north).
adv(not,neg,not).
adv(now,time,now).
%adv(occasionally,freq,occasionally).
adv(off,manner,off).
adv(offline,manner,offline).
adv(ofcourse,focus,[of,course]).
adv(often,freq,often).
%adv(oldly,manner,oldly).
adv(on,manner,on).
adv(once,freq,once).
adv(online,manner,online).
adv(only,focus,only).
adv(open,manner,open).
adv(out,place,out).
adv(outdoors,place,outdoors).
adv(outside,place,outside).
adv(over,time,over).
adv(over,degree,over).
%adv(overnight,time,overnight).
%adv(particularly,focus,particularly).
%adv(past,time,past).
%adv(perfectly,degree,perfectly).
%adv(permanently,manner,permanently).
adv(perhaps,prob,perhaps).
%adv(positively,manner,positively).
adv(possibly,prob,possibly).
%adv(pretty,degree,pretty).
adv(probably,prob,probably).
adv(quickly,manner,quickly).
adv(quite,degree,quite).
%adv(rarely,freq,rarely).
%adv(rather,degree,rather).
adv(really,degree,really).
%adv(rearwards,place,rearwards).
%adv(recently,time,recently).
%adv(regularly,manner,regularly).
%adv(remarkably,degree,remarkably).
adv(right,manner,right).
%adv(sadly,manner,sadly).
%adv(satisfactorily,manner,satisfactorily).
%adv(secret,manner,secretly).
%adv(seldom,freq,seldom).
%adv(seriously,manner,seriously).
%adv(sickly,manner,sickly).
%adv(simply,focus,simply).
%adv(since,time,since).
%adv(slightly,degree,slightly).
adv(slowly,manner,slowly).
adv(so,degree,so).
adv(sometimes,freq,sometimes).
%adv(somewhat,degree,somewhat).
adv(somewhere,place,somewhere).
adv(soon,time,soon).
adv(sooner,time,sooner).
adv(south,place,south).
%adv(specially,manner,specially).
adv(still,focus,still).
adv(straigth,place,straight).
%adv(structurally,manner,structurally).
%adv(subsequently,manner,subsequently).
adv(suddenly,manner,suddenly).
%adv(sure,manner,sure).
%adv(symmetrically,manner,symmetrically).
%adv(systematically,manner,systematically).
%adv(temporarily,time,temporarily).
%adv(terribly,degree,terribly).
adv(then,time,then).
adv(then,link,then).
adv(there,place,there).
%adv(tightly,manner,tightly).
%adv(tiredly,manner,tiredly).
adv(today,time,today).
adv(together,manner,together).
adv(tomorrow,time,tomorrow).
adv(tonight,time,tonight).
adv(too,degree,too).
%adv(totally,degree,totally).
adv(twice,freq,twice).
adv(up,place,up).
adv(upstairs,place,upstairs).
adv(unfortunately,unfortunately).
%adv(unsatisfactorily,manner,unsatisfactorily).
%adv(unusually,freq,unusually).
adv(usually,freq,usually).
adv(weekly,time,weekly).
adv(well,manner,well).
%adv(vertically,place,vertically).
adv(very,degree,very).
adv(west,place,west).
%adv(visually,manner,visually).
%adv(wrongly,manner,badly).
adv(yesterday,time,yesterday).
adv(yet,degree,yet).
%adv(youngly,manner,youngly).


%Per comma connecting adverbs may come in the front of sentences
%following by a comma. Eg: "However, we were sleeping."
adv(firstOfAll,connecting(comma),[first,of,all]).
adv(accordingly,connecting(comma),accordingly).
adv(besides,connecting(comma),besides).
adv(byTheWay,connecting(comma),[by,the,way]).
adv(consequently,connecting(comma),consequently).
adv(furthermore,connecting(comma),furthermore).
adv(hence,connecting(comma),hence).
adv(however,connecting(comma),however).
adv(likewise,connecting(comma),likewise).
adv(moreover,connecting(comma),moreover).
adv(nevertheless,connecting(comma),nevertheless).
adv(nonetheless,connecting(comma),nonetheless).
adv(otherwise,connecting(comma),otherwise).
adv(quickly,connecting(comma),quickly).
adv(sorry,connecting(comma),sorry).
adv(still,connecting(comma),still).
adv(therefore,connecting(comma),therefore).
adv(thus,connecting(comma),thus).
adv(so,connecting(comma),so).


adv(after,part,after).
adv(at,part,at).
adv(down,part,down).
adv(for,part,for).
adv(off,part,off).
adv(on,part,on).
adv(out,part,out).
adv(up,part,up).



%these are rather relative pronouns than adverbs
adv(when,time,when).
adv(where,place,where).
adv(why,reason,why).







