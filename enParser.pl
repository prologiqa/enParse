﻿:- module(enParser,
	  [parseSentence/2
	   ,parseSentence/3
	   ,collectDictErrors/2
           ,dict/2
           ]).

/** <module> English Parser module
 *
 * The module parses English sentences,
 * and creates parse trees, as Prolog structures
 * The module is based on Contralog DCG
 *
 * @author Kilián Imre 17-Mar-18
 *
 * @tbd each of---each postdeterminer---every... etc.
 * @tbd a,an check of indefinite articles
 * @tbd ordinals only with definite article!

 */

:- use_module(pack(pclog/prolog/clog)).
:- use_module(library(apply)).
%%%:-use_module(pclog/prolog/ctrace).

:- use_module(library(lists)).

:- use_module(enMorph).

%:- use_module(enDict).
%dynamic import - module should be explicitely loaded by boot
%and here only the module name is used (instead of file name)
:- add_import_module(parser,enDict,end).


%:- use_module(semantics).

switchOut:- fail.

%Parse tree format
%
%Sentence formats:
%compound(CONN,CLAUSE1,CLAUSE2) CONN: connective, CLAUSE: clause
%CLAUSE a pure clause
%?(CLAUSE) interrogative clause - without interrogative word
%?(WH,CLAUSE) - interrogative clause with interrogative wordSDEM
%
%Clause formats:
% clause(nominal/KIND,FREE,NPS,VP,POL) - nominal clause wo verbal
% clause(verbal/KIND,FREE,NPS,VP,POL) - verbal clause w free args
% with/out free arguments
%
%
%ARGUMENTS - a list of NounPhrase arguments or adverbs
%FREEARGS - a list of free arguments (NP-s or adverbs)
%
%NounPhrases(NPS)
%... is a Prolog list of possibly cumulated noun phrases
%
%NounPhrase(NP) formats
%poss(NP,OWNER)	- possessive noun phrase
% - NP and OWNER are (Numeral)NounPhrases
%numeral(KIND,NR,NP) - numeral noun phrase KIND: ord;card;indef
% - NR the value of numeral, if any (otherwise the numeral itself)
% - NP: (Adjective)NounPhrase
%adj(ADJ,NP)
% - ADJ: a single or a list of ADJectives,
% - NP:  an pure noun
%
%noun(N,NUM,POL) - a pure noun... NUM: sg;pl;VARIABLE
%                - POL: polarity (+;-)
%pronoun(NUMPERS,GENDER,CASE) - an personal pronoun NUMPERS=NUM/PERS
%
%VerbalPhrase formats
% vp(VERB,ADVS,MODE,TENSE,ARGUMENTS,FREE) - simple verbal phrase with
% verb and
%
%adverb(ADV,KIND,GRADE)
% KIND (time;loc(ation);manner)
% GRADE (base;comp;super)



%
% feature geometry for English Parser
% (if a language independent common formalism is found,
% then it should go to parser.pl)
%
dict(pronoun,[numpers,gender,case]).
dict(common,[noun,numpers,case]).
dict(art,[art,numpers,kind,np]).
dict(adj,[adj,grade]).
dict(adj,[adjp,grade,np]).
dict(adv,[adv,kind,grade]).
dict(proper,[noun,class,numpers,case]).
dict(numeral,[kind,nr,numpers,np]).
dict(poss,[np,numpers,owner]).
dict(poss,[np,numpers,owner,gender]).
dict(post,[np,post]).
dict(coordinating,[conn,np1,np2]).
dict(coordinating,[conn,nps]).
dict(verb,[verb,tense,numpers]).
dict(verbal,[prep,vp]).
dict(vp,[verb,adv,mode,tense,args,free]).
dict(clause,[kind,dir,free,subj,vp,numpers,pol]).
dict( ? ,[interr,clause]).
dict( ? ,[clause]).
dict(interr,[wh,kind]).
dict(interr,[wh,kind,adv]).
dict(interr,[wh,kind,np,numpers]).
dict(interj,[interj]).
dict(prep,[prep,np]).
dict(actpart,[np,vp]).
dict(compound,[interj,clause1,clause2]).



value2Num(1,sg):- !.
value2Num(N,pl):- N>1.

%Fails, if any element isnt present in
%either the English dictionary
%or in the semantic thesaurus
%checkDictionary(SENTENCE):-
%  forall(member(W,SENTENCE),
%    (functor(W,WF,_), toSemantics(WF,S), isClass(S))).

%! collectDictErrors(+SENTENCE:list,-ERRORS:list) is det.
%The predicate collects dictionary errors.
%Collects unknown English semantic items.
%
% @arg SENTENCE list of English tokens
% @arg ERRORS list of error messages
%
collectDictErrors(SENTENCE, ERRORS):-
%    checkDictionary(SENTENCE)-> true;
    findall('*** Unknown English':W,
	     (member(W,SENTENCE),
	     \+ word(_SEM,W)), ERRORS).
%    findall('*** Unknown concept':SEM,
%            (member(W,SENTENCE),
%	     functor(W,WF,_), toSemantics(WF,SEM),
%	     atom(SEM), \+ isClass(SEM)), ERR2),
%    append(ERR1,ERR2,ERRORS).


cleanAll:- clean, fail; true.

goalAll:- goal, fail; true.

list2AtNr([],[]):- !.
list2AtNr([NRATOM|ATLIST],[NR|NRLIST]):-
	atom_number(NRATOM,NR), !, list2AtNr(ATLIST,NRLIST).
list2AtNr([ATOM|ATLIST],[ATOM|NRLIST]):-
	list2AtNr(ATLIST,NRLIST).


fireWord(measure(MEAS,QUANT,KIND))-->
    fire_measure(MEAS,QUANT,KIND).


%verb: NUM _ ; sg/3... if sg/3==>FORM=inf
%verb:FORM inf;past;past(participle);present(participle)
fireWord(common(STEM,NUM,CASE))-->
    fire_noun(STEM,NUM,CASE).
fireWord(common(STEM,NUM,CASE,N,LIST))-->
    fire_noun(STEM,NUM,CASE,N,LIST).

fireWord(proper(STEM,CLASS,NUM,CASE))-->
    fire_proper(STEM,CLASS,NUM,CASE).
fireWord(proper(STEM,CLASS,NUM,CASE,N,LIST))-->
    fire_proper(STEM,CLASS,NUM,CASE,N,LIST).

fireWord(pronoun(NUMPERS,GENDER,CASE))-->
    fire_pronoun(NUMPERS,GENDER,CASE).

fireWord(refl(NUMPERS,GENDER))-->
    fire_reflexive(NUMPERS,GENDER).

fireWord(indefPronoun(PRONOUN,NUM,SQ))-->
    fire_indefPronoun(PRONOUN,NUM,SQ).
fireWord(indefPronoun(PRONOUN,NUM,NR,LIST))-->
    fire_indefPronoun(PRONOUN,NUM,NR,LIST).

%verbs, auxiliaries, modals
fireWord(verb(STEM,KIND,NUMPERS))-->
    {aux(STEM)}, fire_aux(STEM,KIND,NUMPERS,false).
fireWord(verb(STEM,KIND,NUMPERS))-->
    fire_verb(STEM,KIND,NUMPERS,false).
fireWord(modal(MODAL,MODE,TENSE,FRAME))-->
    fire_modal(MODAL,MODE,TENSE,FRAME).

%adjectives
fireWord(adj(ADJ,GRAD))-->
    fire_adject(ADJ,GRAD).
fireWord(adj(ADJ,GRAD,N,LIST))-->
    fire_adject(ADJ,GRAD,N,LIST).

%adverbs
fireWord(adv(ADV,KIND,GRADE))-->
    fire_adverb(ADV,KIND,GRADE).
fireWord(adv(ADV,KIND,GRADE,N,LIST))-->
    fire_adverb(ADV,KIND,GRADE,N,LIST).

%articles
fireWord(art(ART,NUM,KIND))-->
    fire_art(ART,NUM,KIND).

%determiners
fireWord(det(DET,NUM,KIND))-->
    fire_determ(DET,NUM,KIND).
fireWord(det(DET,NUM,NUM0,KIND,N,LIST))-->
    fire_determ(DET,NUM,NUM0,KIND,N,LIST).

%interrogatives
fireWord(interr(INTERR,KIND))-->
    fire_interr(INTERR,KIND).
fireWord(interr(INTERR,KIND,N,LIST))-->
    fire_interr(INTERR,KIND,N,LIST).

%interjections
fireWord(interj(INTERJ,IJ))-->
    fire_interj(INTERJ,IJ).

fireWord(prep(PREP))-->
    fire_prep(PREP).
fireWord(postp(POSTP))-->
    fire_postp(POSTP).

fireWord(conj(CONJ,KIND))-->
    fire_connect(CONJ,KIND).
fireWord(card(CARD,INT))--> {value2Num(INT,NUM)},
    fire_numeral(CARD,card,NUM,INT).
fireWord(ord(NR))-->
    fire_numeral(NR,ord,sg,NR).
fireWord(indef(INDEF,NUM,COUNT,KIND))-->
    fire_numeral(INDEF,indef,NUM,COUNT,KIND).
fireWord(tag(TAG))-->
    fire_tag(TAG).
fireWord(inf(INFPART))-->
    fire_inf(INFPART).

fireWord(token(T))-->
    fire_token(T).

fireQuoted(STRING)--> {atom_string(WORD,STRING),
  proper_(WORD,NUM,CASE,WORD,CLASS,_)}, !,
  fire_proper(WORD,CLASS,NUM,CASE).
fireQuoted(STRING)--> fire_proper(STRING,_CLASS,sg,_CASE).


%! parseSentence(+EN:list,-FOREST:exprOrList) is semidet.
%
% Converts an English sentence to parse tree (perhaps a forest)
% It is a shortcut to en2ParseTree/3 with strict semantics
% builds sentence environment
%
%@arg EN a list of English words (delimiter: a lexical issue)
%@arg FOREST List of English parse trees

parseSentence(SENTENCE,FOREST):-
  parseSentence(SENTENCE,true,FOREST).


% ! parseSentence(+EN:list,?OPTIONS,-FOREST:exprOrList) is semidet.
% Converts an English sentence to parse tree (perhaps a forest)
% and thereby builds sentence environment...
%
%@arg EN list of English notation words
%@arg OPTIONS boolOrOptlist - list of parsing options
%@arg FOREST exprOrList of English parse trees

parseSentence(SENTENCE,STRICT,FOREST):-
  cleanAll, %clean blackboard
  goalAll, %it is necessary only in case of Contralog facts
%%   Contralog should generate an empty 'goal' rule!!!!!!!!
  nb_setval(options,STRICT),
  length(SENTENCE,Y), nb_setval(nrofwords,Y), nb_setval(parsetree,[]),
  (nth0(NTH,SENTENCE,X), NEXT is NTH+1,
     (NTH=0, atom(X), \+ punct(X), downcase_atom(X,LOX), LOX\=X,
        morphAnal(LOX,LEXP), fireWord(LEXP,NTH,NEXT), fail;
     string(X)->fireQuoted(X,NTH,NEXT),fail;
     punct(X), \+ punctWordTag(X)->fire_punct(X,NTH,NEXT), fail;
     punctWordTag(X),fire_punct(X,NTH,NEXT), fail;
     morphAnal(X,XEXP),fireWord(XEXP,NTH,NEXT),fail)
  ;nb_getval(parsetree,FOREST)).

%
%Hook procedures for Contralog exports
%They are mainly used for debugging purposes
%
exp_clause(CL,KIND,DIR,TYPE,SUBJ,X,Y):-
	debug(enParser,'Clause:~w/~w~w~w~w@~d-~d',[CL,KIND,DIR,TYPE,SUBJ,X,Y]).

exp_sentence(TREE,0,Y):- nb_getval(nrofwords,Y), !,
	nb_getval(parsetree,FOREST), nb_setval(parsetree,[TREE|FOREST]),
	debug(enParser,'Sentence:~w@~d-~d',[TREE,0,Y]).
exp_sentence(TREE,X,Y):- X>0,
	debug(enParser,'IncompleteSentence:~w@~d-~d',[TREE,X,Y]).
exp_sentence(TREE,X,Y ):- nb_getval(nrofwords,Z), Y<Z,
	debug(enParser,'IncompleteSentence:~w@~d-~d',[TREE,X,Y]).

exp_nounPhrase(NP,NUM/PERS,GENDER,CASE,SQ,X,Y):- NP\=ref(_,_), !,
	debug(enParser,'NounPhrase:~w/~w(~w)-~w-~w-~w@~d-~d',
              [NP,NUM,PERS,GENDER,CASE,SQ,X,Y]).

exp_verbalPhrase(VP,TENSE,NUM/PERS,POL,SUB,PDEM,SQ,X,Y):-
	debug(enParser,'VerbalPhrase:~w/~w-~w(~w)-~w-~w-~w-~w@~d-~d',
              [VP,TENSE,NUM,PERS,POL,SUB,PDEM,SQ,X,Y]).

exp_interrPhrase(IP,KIND,NUM,X,Y):-
	debug(enParser,'InterrogativePhrase:~w/~w-~w-~w@~d-~d',
	      [IP,KIND,NUM,X,Y]).


%but1DemSat(+DEMANDS,+CASES,+ARGS0,?ARGS,?DEM,?X).
% succeeds, if syntactical demands are all but one satisfiable
%@arg DEMANDS list of syntactical demands
%@arg CASES list of cases of ARGS0
%@arg ARGS0 list of recognized arguments
%     CONSTRAINT length(CASES,LN)=length(ARGS0,LN)
%@arg ARGS list of arguments, extended by X matching DEM
%@arg DEM a single, not satisifed demand
%@arg X new argument, matching DEM, inserted into ARG
% Missing demand is returned in parameter DEM
but1DemSat([],[],[],[],_,_):- !.
%a ?(DEM) demand means as if there were two demandlists
%one with DEM and one without
but1DemSat([?(DEM)|DEMS],CASES,ARGS0,ARGS,DEMX,X):-
  but1DemSat([DEM|DEMS],CASES,ARGS0,ARGS,DEMX,X).
but1DemSat([?(_)|DEMS],CASES,ARGS0,ARGS,DEMX,X):-
  but1DemSat(DEMS,CASES,ARGS0,ARGS,DEMX,X).
%demand is satisfiable
but1DemSat([DEM|DEMS],[CASE|CASES],[ARG|ARGS0],[ARG|ARGS],DEMX,X):-
  DEM \= ?(_),
  once(demSat(DEM,CASE)), !, but1DemSat(DEMS,CASES,ARGS0,ARGS,DEMX,X).
but1DemSat([DEM|DEMS],CASES,ARGS0,[X|ARGS],DEM,X):- !,
  DEM \= ?(_), allDemsSat(DEMS,CASES,ARGS0,ARGS).

%but1DemSat(+DEMANDS,+CASES,+ARGS0,?ARGS).
allDemsSat([],[],[],[]).
allDemsSat([?(DEM)|DEMS],CASES,ARGS0,ARGS):-
  allDemsSat([DEM|DEMS],CASES,ARGS0,ARGS).
allDemsSat([?(_)|DEMS],CASES,ARGS0,ARGS):-
  allDemsSat(DEMS,CASES,ARGS0,ARGS).
allDemsSat([DEM|DEMS],[CASE|CASES],[ARG|ARGS0],[ARG|ARGS]):-
  DEM \= ?(_),
  once(demSat(DEM,CASE)), !, allDemsSat(DEMS,CASES,ARGS0,ARGS).

%demSat(+DEM,+CASE) is semidet
demSat(CASE,CASE):- atom(CASE), !.
demSat(acc,nom). %Demand 'acc' is satisfied by 'nom'..., eg. 'dogs'
demSat(DEM,CASE):- compound(DEM), DEM=..[PREP,DEM1],
  CASE=..[PREP,CAS1], !, demSat(DEM1,CAS1).
demSat(DEM,CASE):- compound(CASE),
  arg(1,CASE,CAS), demSat(DEM,CAS).

emptyOptDem(DEM):- var(DEM), !.
emptyOptDem(?(_)).

optDems([]).
optDems([?(_)|DEMS]):- optDems(DEMS).

interrSatDem(X,X).
interrSatDem(acc,nom).

accusativeDemand(acc).
accusativeDemand(?(acc)).

accusativeCase(CASE):- compound(CASE), !, arg(1,CASE,acc).
accusativeCase(acc).

nominalDem(nom).
nominalDem(?(nom)).
nominalDem(acc).
nominalDem(?(acc)).
%nominalDem(dat).
%nominalDem(?(dat)).
nominalDem(EXPR):- EXPR=..[PREP,DEM], prep(PREP,_), nominalDem(DEM).

%if each but the last obligatory argument demand is nominal
butLastNominalDemands([]):- !.
butLastNominalDemands([verbal]):- !.
butLastNominalDemands([verbal(to)]):- !.
butLastNominalDemands([verbal(wh)]):- !.
butLastNominalDemands([verbal(ing)]):- !.
butLastNominalDemands([clause]):- !.
butLastNominalDemands([clause(wh)]):- !.
butLastNominalDemands([clause(that)]):- !.
butLastNominalDemands([clause(if)]):- !.
butLastNominalDemands([DEM|DEMS]):-
  once(nominalDem(DEM)), !, butLastNominalDemands(DEMS).
butLastNominalDemands([?(_)|DEMS]):- butLastNominalDemands(DEMS).

listConnective(or).
listConnective(and).


:- discontiguous auxTense2Inf/4.

dictTense(inf).
dictTense(present(participle)).
dictTense(past(participle)).
dictTense(present).
dictTense(past).

inf2Tense(inf,present,NUM/PERS):-
    freeze(PERS, NUM/PERS\=sg/3).
inf2Tense(INF,INF,_):- dictTense(INF).

%XXX have driven
auxTense2Inf(have,inf,past(participle),inf(perfect)).
auxTense2Inf(have,present,past(participle),inf(perfect)).
%XXX be driving
auxTense2Inf(be,inf,present(participle),inf(continuous)).
%XXX been driving
auxTense2Inf(be,past(participle),present(participle),
             inf(perfect,continuous)).
%XXX have been driving
auxTense2Inf(have,inf,inf(perfect,continuous),
             inf(inf(perfect,continuous))).

:- discontiguous tense2Conj/2, auxTense2Conj/3, auxTense2Conj/4.

%tense2Conj(VERBTENSE,VERBCONSTRUCT)
%tense2Conj(AUXTENSE,VERBTENSE,VERBCONSTRUCT)
%tense2Conj(AUX,AUXTENSE,VERBTENSE,VERBCONSTRUCT)
%
tense2Conj(present(participle),present(participle)).
tense2Conj(past(participle),past(participle)).

%
% ACTIVE TENSES
%

%drive
tense2Conj(present,present).
%tense2Conj(inf,present). %%%inf should already be given
tense2Conj(inf,inf).
%do drive/don't drive
auxTense2Conj(do,inf,present,present).
%auxTense2Conj(do,present,present,present).
auxTense2Conj(do,present,inf,present).
auxTense2Conj(do,inf,inf,inf).
%have driven
auxTense2Conj(have,present,past(participle),present(perfect)).
auxTense2Conj(inf,past(participle),present(perfect)).
auxTense2Conj(have,inf,past(participle),present(perfect)).

%can drive
auxTense2Conj(present,inf,present).
%can have driven
auxTense2Conj(present,inf(perfect),present(perfect)).

%drove
tense2Conj(past,past).
%did drive
auxTense2Conj(do,past,inf,past).
%had driven
auxTense2Conj(have,past,past(participle),past(perfect)).
auxTense2Conj(past,past(participle),past(perfect)).

%could drive
auxTense2Conj(past,inf,past).
%could have driven
auxTense2Conj(past,inf(perfect),past(perfect)).

%will drive
auxTense2Conj(be,future,inf,future).
%will have driven
auxTense2Conj(be,future,inf(perfect),future(perfect)).

%
% continuous tenses
%
%am driving
auxTense2Conj(be,present,present(participle),present(continuous)).
%have been driving
auxTense2Conj(have,present,inf(perfect,continuous),
              present(perfect,continuous)).
auxTense2Conj(have,inf,inf(perfect,continuous),
              present(perfect,continuous)).

%can be driving
auxTense2Conj(present,inf(continuous),present(continuous)).
%can have been driving
auxTense2Conj(present,inf(inf(perfect,continuous)),
              present(perfect,continuous)).

%was driving
auxTense2Conj(be,past,present(participle),past(continuous)).
%had been driving
auxTense2Conj(have,past,inf(perfect,continuous),
              past(perfect,continuous)).

%could be driving
auxTense2Conj(past,inf(continuous),past(continuous)).
%could have been driving
auxTense2Conj(past,inf(inf(perfect,continuous)),
              past(perfect,continuous)).

%will be driving
auxTense2Conj(be,future,inf(continuous),future(continuous)).
%will have been driving
auxTense2Conj(be,future,inf(inf(perfect,continuous)),
              future(perfect,continuous)).

%
% PASSIVE TENSES
%
%be driven
auxTense2Inf(be,inf,past(participle),inf(passive)).
auxTense2Inf(be,present,past(participle),inf(passive)).
%been driven
auxTense2Inf(be,past(participle),past(participle),inf(passive(perfect))).
%have been driven
auxTense2Inf(have,inf,inf(passive(perfect)),inf(inf(passive(perfect)))).
%being driven
auxTense2Inf(be,present(participle),past(participle),
             inf(passive(continuous))).
%been being driven
auxTense2Inf(be,past(participle),inf(passive(continuous)),
             inf(passive(perfect,continuous))).
%be being driven
auxTense2Inf(be,inf,inf(passive(continuous)),
                        inf(inf(passive(perfect,continuous)))).

%am driven
auxTense2Conj(be,present,past(participle),passive(present)).
%have been driven
auxTense2Conj(have,inf,inf(passive(perfect)),
              passive(present(perfect))).
auxTense2Conj(have,present,inf(passive(perfect)),
              passive(present(perfect))).

%can be driven
auxTense2Conj(present,inf(passive),passive(present)).
%can have been driven
auxTense2Conj(present,inf(inf(passive(perfect))),
              passive(perfect)).

%was driven
auxTense2Conj(be,past,past(participle),passive(past)).
%had been driven
auxTense2Conj(have,past,inf(passive(perfect)),
              passive(past(perfect))).

%could be driven
auxTense2Conj(past,inf(passive),passive(past)).
%could have been driven
auxTense2Conj(past,inf(inf(passive(perfect))),
              passive(perfect)).

%will be driven
auxTense2Conj(be,future,inf(passive),passive(future)).
%will have been driven
auxTense2Conj(be,future,inf(inf(passive(perfect))),
              passive(future(perfect))).

%am being driven
auxTense2Conj(be,present,inf(passive(continuous)),passive(present(continuous))).
auxTense2Conj(be,inf,inf(passive(continuous)),passive(present(continuous))).
%have been being driven
auxTense2Conj(have,present,inf(passive(perfect,continuous)),
              passive(present(perfect,continuous))).
auxTense2Conj(have,inf,inf(passive(perfect,continuous)),
              passive(present(perfect,continuous))).

%will be being driven
auxTense2Conj(be,future,inf(inf(passive(perfect,continuous))),
                            passive(future(perfect,continuous))).

%was being driven
auxTense2Conj(be,past,inf(passive(continuous)),passive(past(continuous))).
%had been being driven
auxTense2Conj(have,past,inf(passive(perfect,continuous)),
              passive(past(perfect,continuous))).


auxModTense2Conj(AUX,AT,VT,TENSE):-
  isModal(AUX)->once(auxTense2Conj(AT,VT,TENSE));
  once(auxTense2Conj(AUX,AT,VT,TENSE)).


midAdverbKind(neg).
midAdverbKind(freq).
midAdverbKind(degree).
midAdverbKind(focus).
midAdverbKind(link).
midAdverbKind(prob).

advNumeralKind(neg,_).
advNumeralKind(focus,_).
advNumeralKind(degree,NUM):- NUM\=indef.

backPrepInterr(nom).
backPrepInterr(acc).

insertInterrPrep(interr(WH,KIND),PREP,interr(WH,PKIND)):-
  PKIND=..[PREP,KIND].

whBeforeNounPhrase(adj).
whBeforeNounPhrase(adv(manner)).

verbclauseDem(VERBCL):- VERBCL=..[verbal|_].
verbclauseDem(VERBCL):- VERBCL=..[clause|_].

freeMatchFrame(?(FRAME),prep(PRE,_)):- functor(FRAME,PRE,1).
freeMatchFrame(?(VERBCL),_):- verbclauseDem(VERBCL).
freeMatchFrame(VERBCL,_):- verbclauseDem(VERBCL).

freeMatchesFrame([FRAME|_],[FREE|_]):- freeMatchFrame(FRAME,FREE), !.
freeMatchesFrame([_,FRAME|_],[FREE|_]):- freeMatchFrame(FRAME,FREE), !.

matchingLongShortVP(vp(verb([_VERB],TENSE,_),_,_,_,_,_),
                    vp(verb([do,''],TENSE,_),_,_,_,_,_)):- !.
matchingLongShortVP(vp(verb([AUX|_],_,_),_,_,_,_,_),
                    vp(verb([AUX,''],_,_),_,_,_,_,_)):- !.

mainVerb(vp(verb(VERB,_,_),_MADV,_MODE,_TENSE,_ARGS,_FREE),VERB):- atom(VERB), !.
mainVerb(vp(verb(VERBS,_,_),_MADV,_MODE,_TENSE,_ARGS,_FREE),VERB):-
    is_list(VERBS), last(VERBS,VERB).

sentenceKind(declarative).
sentenceKind(interrogative(yn)).
sentenceKind(interrogative(wh)).
sentenceKind(interrogative(subj)).
sentenceKind(interrogative(pred)).
sentenceKind(interrogative(about)).
sentenceKind(exclamatory).
sentenceKind(imperative(strong)).
sentenceKind(imperative(weak)).


matchingPunct(_SUBJ,declarative,'.').
matchingPunct(_SUBJ,interrogative(_),'?').
matchingPunct(SUBJ,exclamatory,'!'):- SUBJ\=''.
matchingPunct(SUBJ,imperative(weak),'.'):- SUBJ==''.
matchingPunct(SUBJ,imperative(strong),'!'):- SUBJ==''.

matchingPunct(declarative,'.').
matchingPunct(interrogative(_),'?').
matchingPunct(exclamatory,'!').
matchingPunct(imperative(weak),'.').
matchingPunct(imperative(strong),'!').


quoting(CASN,nom,true):-
  CASN== ?, !.
quoting(CASE,CASE,false).

:- discontiguous(clean/0). %%% curing of sick Contralog pretranslator

:- contra.


%In export and import declarations NOT ContraDCG,
%but pure Contralog predicates must be specified!
:- export([clause/5,sentence/3,nounPhrase/7,interrPhrase/5
          ,verbalPhrase/9]).

:- import([art/5]).
:- import([aux/6]).
:- import([noun/5]).
:- import([noun/7]).
:- import([proper/6]).
:- import([proper/8]).
:- import([modal/6]).
:- import([verb/6]).
:- import([connect/4]).
:- import([prep/3]).
:- import([postp/3]).
:- import([interr/4]).
:- import([interr/6]).
:- import([interj/4]).
:- import([inf/3]).
:- import([measure/5]).
:- import([numeral/6]).
:- import([numeral/7]).
:- import([adject/4]).
:- import([adject/6]).
:- import([adverb/5]).
:- import([adverb/7]).
:- import([determ/5]).
:- import([determ/6]).
:- import([determ/8]).
:- import([pronoun/5]).
:- import([indefPronoun/5]).
:- import([indefPronoun/6]).
:- import([punct/3]).
:- import([reflexive/4]).
:- import([tag/3]).
:- import([token/3]).


%constructing ordinals from cardinal and 'st'/'nd'/'th'
numeral(NUM,ord,sg,NR)-->
  numeral(NUM,card,_,NR), punct('-'), tag(TAG),
  {once(ordTag(NR,TAG))}.
%constructing ordinals from cardinal and '.'
numeral(NUM,ord,sg,NR)-->
  numeral(NUM,card,_,NR), punct('.').


adverb([],_KIND,_GRADE,_N,_LIST,X,X):- true. %Contralog(!) fact
adverb(LIST,KIND,GRADE)-->
  adverb(LIST,KIND,GRADE,1,LIST), {LIST \= [], LIST\=[_]}.
adverb([AM|AN],KIND,GRADE,M,LIST)-->
  adverb(AM,KIND,GRADE,M,LIST), {atom(AM)},
  adverb(AN,KIND,GRADE,N,LIST), {is_list(AN)}, {N is M+1, M>0}.


adject([],_GRADE,_N,_LIST,X,X):- true. %Contralog(!) fact
adject(LIST,GRADE)-->
  adject(LIST,GRADE,1,LIST), {LIST \= [], LIST\=[_]}.
adject([AM|AN],GRADE,M,LIST)-->
  adject(AM,GRADE,M,LIST), {atom(AM)},
  adject(AN,GRADE,N,LIST), {is_list(AN)}, {N is M+1, M>0}.
adject(W1-W2,GRADE)-->
  adject(W1,GRADE,1,W1-W2), {atom(W1)}, punct('-'),
  adject(W2,GRADE,2,W1-W2), {atom(W2)}.

%constructing proper nouns from several tags (eg. New York)
proper([],_CLASS,_NUM,_CASE,_N,_LIST,X,X):- true. %Contralog(!) fact
proper(LIST,CLASS,NUM,CASE)-->
  proper(LIST,CLASS,NUM,CASE,1,LIST), {LIST \= [], LIST\=[_]}.
proper([PM|PN],CLASS,NUM,CASE,M,LIST)-->
  proper(PM,CLASS,NUM,CASE,M,LIST), {PM \= [], atom(PM)},
  proper(PN,CLASS,NUM,CASE,N,LIST), {N is M+1, M>0}.


%constructing common nouns from several tags (eg. ice cream)
noun([],_NUM,_CASE,_N,_LIST,X,X):- true. %Contralog(!) fact
noun(LIST,NUM,CASE)-->
  noun(LIST,NUM,CASE,1,LIST), {LIST \= [], LIST\=[_]}.
noun([PM|PN],NUM,CASE,M,LIST)-->
  noun(PM,NUM,CASE,M,LIST), {atom(PM)},
  noun(PN,NUM,CASE,N,LIST), {is_list(PN), N is M+1, M>0}.
noun(W1-W2,NUM,CASE)-->
  noun(W1,NUM,CASE,1,W1-W2), {atom(W1)}, punct('-'),
  noun(W2,NUM,CASE,2,W1-W2), {atom(W2)}.


%singular genitive is constructed from two tokens
proper(PROPER,CL,sg,gen)--> proper(PROPER,CL,sg,?), token(s).
%singular genitive is constructed from two tokens
noun(NOUN,sg,gen)--> noun(NOUN,sg,?), token(s).
%Two tokens make also (NOUN IS) and/ or (NOUN HAS)
%?????

%constructing determiners from several tags (eg. 'a sort of')
determ([],_,_,_KIND,_N,_LIST,X,X):- true. %Contralog(!) fact
determ(LIST,NUM,NUM0,KIND)-->
  determ(LIST,NUM,NUM0,KIND,1,LIST), {LIST \= [], LIST\=[_]}.
determ([PM|PN],NUM,NUM0,KIND,M,LIST)-->
  determ(PM,NUM,NUM0,KIND,M,LIST), {atom(PM)},
  determ(PN,NUM,NUM0,KIND,N,LIST), {is_list(PN), N is M+1, M>0}.


%constructing interrogatives from several tags (eg. 'how many')
%interr([],_KIND,_N,_LIST,X,X):- true. %Contralog(!) fact
%interr(LIST,KIND)-->
%  interr(LIST,KIND,1,LIST), {LIST \= [], LIST\=[_]}.
%interr([IM|IN],KIND,M,LIST)-->
%  interr(IM,KIND,M,LIST), {atom(IM)},
%  interr(IN,KIND,N,LIST), {is_list(IN)}, {N is M+1, M>0}.
interr([W1,W2],KIND)-->
  interr(W1,KIND,1,[W1,W2]), {atom(W1)},
  interr(W2,KIND,2,[W1,W2]), {atom(W2)}.

%constructing indefinite pronouns from several tags (eg. 'a few')
indefPronoun([],_NUM,_N,_LIST,X,X):- true. %Contralog(!) fact
indefPronoun(LIST,NUM,false)-->
  indefPronoun(LIST,NUM,1,LIST), {LIST \= [], LIST\=[_]}.
indefPronoun([IM|IN],NUM,M,LIST)-->
  indefPronoun(IM,NUM,M,LIST), {atom(IM)},
  indefPronoun(IN,NUM,N,LIST), {is_list(IN)}, {N is M+1, M>0}.


verb(be,present,sg/3,true)-->token(s).
aux(be,present,sg/3,true)-->token(s).
aux(have,present,sg/3,true)-->token(s).

auxMod(AUX,'',TENSE,NUMPERS,+)-->aux(AUX,T,NUMPERS,false),
    {(T=inf->TENSE=present;TENSE=T)}.
%auxMod(AUX,'',present,NUMPERS,POL)-->
%    auxMod(AUX,'',inf,NUMPERS,POL), {AUX \== be}.
auxMod(AUX,indicative,TENSE,NUMPERS,-)-->aux(AUX,T,NUMPERS,false),
  adverb(_ADV,neg,base), {(T=inf->TENSE=present;TENSE=T)}.
auxMod(MODAL,MODE,TENSE,_NUMPERS,+)-->modal(MODAL,MODE,TENSE,verb).
auxMod(MODAL,MODE,TENSE,_NUMPERS,-)-->modal(MODAL,MODE,TENSE,verb),
  adverb(_ADV,neg,base).


%cold, better, easiest
%adject(A,GRADE)-->adj(A,GRADE).
%(a) changing world
adject(verb(V,present(participle)),base)-->
  verb(V,present(participle),_,false),
  {verbAPartFrame(_,V,[nom|FRAMES]), optDems(FRAMES)}.
%(a) borrowed book
adject(verb(V,past(participle)),base)-->
  verb(V,past(participle),_,false), {V\=have, V\=be},
  {verbAPartFrame(_,V,[nom,FRAME|FRAMES])},
  {nominalDem(FRAME), optDems(FRAMES)}.

%quick
adjPhrase(adj(A,GRADE),GRADE)--> adject(A,GRADE).
% adjPhrase(V,base)--> verb(V,present(participle),_,false),
% {optDems(FRAMES)}. most cold|least cold|more cold|less cold
%(This fruit is) really sweet.
adjPhrase(adjP(adv(ADV,KIND,base),GRADE,ADJ),APGRADE)-->
   adverb(ADV,KIND,base), {preAdjAdvKind(KIND)},
   adject(ADJ,GRADE), {ADJ\=[_|_]},
   {preAdjAdvGrade(GRADE,KIND,APGRADE)},
   {KIND\=comp->true; GRADE\=comp}. %adj
%(This fruit is) not much sweeter.
adjPhrase(adjP([ADV1,ADV2],GRADE,ADJ),APGRADE)-->
  adverb(ADV1,KIND1,base), {preAdjAdvKind(KIND1)},
  adverb(ADV2,KIND2,base),
  {preAdjAdvKind(KIND2)}, adject(ADJ,GRADE),
   {preAdjAdvGrade(GRADE,KIND2,APGRADE)}. %adj
%3 meters long / some meters long
adjPhrase(adjP(KIND,NR,UNIT,ADJ),base)-->
  numeral(_NUMEXP,KIND,NUM,NR),  noun(UNIT,NUM,nom),
  adject(ADJ,base), {once((measure(UNIT,_,_,ADJS),member(ADJ,ADJS)))}.
%2 meter long, awesomely big --- only 2 list members!!
adjPhrase([ADJ,ADJP],GRADE)-->
  adjPhrase(ADJ,GRADE), {\+ is_list(ADJ)}, punct(','),
  adjPhrase(ADJP,GRADE), {\+ is_list(ADJP)}.
%big hot brown
adjPhrase(adj([ADJ,ADJP]),GRADE)-->
  adjPhrase(adj(ADJ),GRADE), {atom(ADJ)}, punct(','),
  adject(ADJP,GRADE), {atom(ADJP)}. %adj
adjPhrase(adj(ADJ|ADJP),GRADE)-->
  adjPhrase(adj(ADJP),GRADE), {is_list(ADJP)},
  adject(ADJ,GRADE), {atom(ADJ)}. %adj

% $ 5 ...
quantity(quantity(NR,UNIT,abbr,NUM/3,CASE),abbr,NUM/3,CASE)-->
    measure(UNIT,money,abbr), numeral(_NUMEXP,card,NUM,NR).
% 1000 km
quantity(quantity(NR,UNIT,abbr,NUM/3,CASE),abbr,NUM/3,CASE)-->
    numeral(_NUMEXP,card,NUM,NR), measure(UNIT,_,abbr).
%the number of the quantity is not agreed with that of measure
quantity(quantity(NR,UNIT,full,NUM/3,CASE),abbr,NUM/3,CASE)-->
    numeral(_NUMEXP,card,_NUM,NR), noun(UNIT,NUM,CASE),
    {once(measure(UNIT,_,_,_))}.

%	NounPhrase
%
%NP:       syntax structure of NP
%NUM/PERS: number and person of NP (if any)
%CASE:     nom;acc (if distinguishable); gen; poss
%SQ        single quote at the end (true;false)
%Noun Phrase with or without some common adjective
%SEM: class of double quoted noun

%'America':a known proper noun
adjectiveNounPhrase(proper(PROPER,CLASS,NUM/3,CASE),NUM/3,CASE,SQ)-->
  proper(PROPER,CLASS,NUM,CAS0), {quoting(CAS0,CASE,SQ)}. %known proper nouns
%car
adjectiveNounPhrase(common(NOUN,NUM/3,CASE),NUM/3,CASE,SQ)-->
  noun(NOUN,NUM,CAS0), {quoting(CAS0,CASE,SQ)}.
  %we dont know if it is plural or not
%sea journey - composed of two nouns
adjectiveNounPhrase(common([ADJN,NOUN],NUM/3,CASE),NUM/3,CASE,SQ)-->
  noun(ADJN,sg,nom), {atom(ADJN)}, noun(NOUN,NUM,CASN), {atom(NOUN)},
  {quoting(CASN,CASE,SQ)}.
%platform 8
adjectiveNounPhrase(#(N,NR),sg/3,CASE,false)-->
  noun(N,sg,CASE), numeral(NR,card,_,_), {integer(NR)}.

%city "budapest" - noun as class, and an unknown proper between quotes
%either the class is the prefix noun, or no alternative parses
adjectiveNounPhrase([common(NOUN,NUM/3,CASE),
    proper(NAME,NOUN,NUM/3,CASE)],NUM/3,CASE,false)-->
  noun(NOUN,NUM,nom), proper(NAME,CLASS,NUM,CASE),
  {once(CLASS=NOUN;
        ( \+ proper_(NAME,_,_,_,NOUN,_),
          \+ (proper(NAME,CL,NUM,CASE,_,_),CL \= CLASS) )
        )}.
%(pretty;left) one
adjectiveNounPhrase(indef(PRONOUN,NUM/3),NUM/3,_CASE,SQ)-->
  indefPronoun(PRONOUN,NUM,SQ). %, {PRONOUN=one}.
%big blue cars (are nice)
%adjectiveNounPhrase(adj(ADJ,NP),NUM,CASE)-->
%  adjPhrase(ADJ,_GRADE), {GRADE \= more(_), GRADE \= less(_)},
%  adjectiveNounPhrase(NP,NUM,CASE), {NP \= adj(_,_)}.
%awesomely big very blue company (adjPhrase feldolgozza)
adjectiveNounPhrase(adj(AP,GRADE,NP),NUMPERS,CASE,SQ)-->
  adjPhrase(AP,GRADE),
  adjectiveNounPhrase(NP,NUMPERS,CASE,SQ), {NP\=adj(_,_)}.


%NounPhrase with or without numeral
%numeralNounPhrase(NP,NUM,CASE)
%NP: resulting noun phrase
%NUM: plurality indicator (pl;sg)
%CASE:nom;gen.
numeralNounPhrase(NP,NUMPERS,CASE,SQ)-->
  adjectiveNounPhrase(NP,NUMPERS,CASE,SQ).
% quantity expressions
numeralNounPhrase(NP,NUMPERS,CASE,false)-->
  quantity(NP,abbr,NUMPERS,CASE).
%3 big apples
numeralNounPhrase(numeral(KIND,NUMR,NUM/PERS,NP),NUM/PERS,CASE,SQ)-->
  numeral(NUMR,KIND,NUM,_NR), adjectiveNounPhrase(NP,NUM/PERS,CASE,SQ),
  {\+ (NP=common(N,_,_), measure(N,_,_,_)) }.
%totally 3 red apples / not 3 many apples
numeralNounPhrase(numeral(KIND,NR,adv(ADV,AKIND,base),NUM/PERS,NP),
                  NUM/PERS,CASE,SQ)-->
  adverb(ADV,AKIND,base), numeral(_,KIND,NUM,NR), {advNumeralKind(AKIND,KIND)},
  adjectiveNounPhrase(NP,NUM/PERS,CASE,SQ).
%one more apple / one less apples
numeralNounPhrase(numeral(card,NUMR,adv(ADV,comp,base),NUM/PERS,NP),
                  NUM/PERS,CASE,SQ)-->
  numeral(NUMR,card,NUM,_NR), adverb(ADV,comp,base),
  adjectiveNounPhrase(NP,NUM/PERS,CASE,SQ).
%we don't use single quotes for such constructions
%numeralNounPhrase(poss(NP,NUM/3,OWNER),NUM/3,CASE,false)-->
%  numeralNounPhrase(NP,NUM/3,CASE,false), prep(of),
%  numeralNounPhrase(OWNER,_,nom,false).


%Noun phrase with or wo determiner
%NUM: plurality indicator (pl;sg)
determinedNounPhrase(NP,NUMPERS,CASE,SQ)-->
  numeralNounPhrase(NP,NUMPERS,CASE,SQ).
%the 2 red apple - a few minutes
determinedNounPhrase(art(DET,NUM/PERS,KIND,NP),NUM/PERS,CASE,SQ)-->
  art(DET,NUM,KIND), numeralNounPhrase(NP,NUM/PERS,CASE,SQ),
  {\+ (KIND=indef, NUM=pl) }.
%that 2 red apple
determinedNounPhrase(det(DET,NUM/PERS,NP),NUM/PERS,CASE,SQ)-->
  determ(DET,NUM,numNP), numeralNounPhrase(NP,NUM/PERS,CASE,SQ).
%all red apples
%determinedNounPhrase(det(QUANT,NUM/PERS,NP),NUM/PERS,CASE,SQ)-->
determinedNounPhrase(numeral(indef,INDEF,NUM/PERS,NP),NUM/PERS,CASE,SQ)-->
  numeral(INDEF,indef,NUM,_COUNT,adjNP),
  adjectiveNounPhrase(NP,NUM/PERS,CASE,SQ).
%not much money
%determinedNounPhrase(det(ADV,NUMERAL,NUM/PERS,NP),NUM/PERS,CASE,SQ)-->
determinedNounPhrase(numeral(indef,INDEF,adv(ADV,AKIND,base),NUM/PERS,NP),
                     NUM/PERS,CASE,SQ)-->
  adverb(ADV,AKIND,base), numeral(INDEF,indef,NUM,_COUNT,adjNP),
  {advNumeralKind(AKIND,indef)},
  adjectiveNounPhrase(NP,NUM/PERS,CASE,SQ).
%someone
%determinedNounPhrase(indef(PRONOUN,NUM/3),
%                     NUM/3,_CASE,SQ)-->
%	indefPronoun(PRONOUN,NUM,SQ).


%Noun phrase with or wo possessive relations
%No more than one possPronoun plus two NPs
%personal pronouns can have neither possessive
%nor plain adjectives, thus they are also possessive NPs
%
%possessiveNounPhrase(POSSNP,NUMPERS,GENDER,CASE,SQ)
%POSSNP: resulting noun phrase
%NUM: number indicator (sg;pl)
%PERS: 1..3
%GENDER: femaleSex;maleSex ...gender of owned object!!
%CASE:nom;gen.
%
%you, me,...
possessiveNounPhrase(pronoun(NUM/PERS,GENDER,CASE),NUM/PERS,
                     GENDER,CASE,false)-->
  pronoun(NUM/PERS,GENDER,CASE), {CASE\==poss, CASE\==gen}.
%yours, mine
possessiveNounPhrase(pronoun(NUM/PERS,GENDER),
                     _NUMPERS,GENDER,_CASE,false)-->
  pronoun(NUM/PERS,GENDER,CASE), {CASE==poss}.
possessiveNounPhrase(NP,NUM/3,_GENDER,CASE,SQ)-->
  determinedNounPhrase(NP,NUM/3,CASE,SQ).
%poss(NP,NUMPERS,OWNER,GENDER)
%NP: the owned object, NUMPERS: the number/3 of owned object
%OWNER: the owner, GENDER: gender of the owner
%my friend
possessiveNounPhrase(poss(NP,NUM/3,OWNER,GENDER),
                     NUM/3,_G,CASE,SQ)-->
  pronoun(OWNER,GENDER,GEN), {GEN==gen},
  numeralNounPhrase(NP,NUM/3,CASE,SQ).
%your one
possessiveNounPhrase(poss(PRONOUN,NUM/3,OWNER,GENDER),
                     NUM/3,_G,_CASE,SQ)-->
  pronoun(OWNER,GENDER,GEN), {GEN==gen}, indefPronoun(PRONOUN,NUM,SQ).
%each,2,friend's,3,brown,dog
possessiveNounPhrase(poss(NP,NUM/3,OWNER),
                     NUM/3,_GENDER,CASE,SQ)-->
  determinedNounPhrase(OWNER,_,GEN,false), {GEN==gen},
  numeralNounPhrase(NP,NUM/3,CASE,SQ).
%3 brown dogs of your rich company,
possessiveNounPhrase(poss(NP,NUM/3,OWNER),
                     NUM/3,_GENDER,CASE,false)-->
  determinedNounPhrase(NP,NUM/3,CASE,false), prep(of),
%  possessiveNounPhrase(OWNER,_,_,_,false).
  possessiveNounPhrase(OWNER,_,_,acc,false).
%your,old,friend's,red,car
possessiveNounPhrase(poss(NP,NPNUM/3,
                     poss(OWNER1,OWNUMPERS,OWNER2,GENDER)),
                     NPNUM/3,_GENDER,CASE,SQ)-->
  pronoun(OWNER2,GENDER,GEN2), {GEN2==gen},
  numeralNounPhrase(OWNER1,OWNUMPERS,GEN1,false), {GEN1==gen},
  numeralNounPhrase(NP,NPNUM/3,CASE,SQ).
%Determiner + Noun phrase with or w/o pre/postposition
%both my parents
%possessiveNounPhrase(det(QUANT,NUM/PERS,NP),NUM/PERS,GENDER,CASE,SQ)-->
possessiveNounPhrase(numeral(indef,INDEF,NUM/PERS,NP),
                     NUM/PERS,GENDER,CASE,SQ)-->
  numeral(INDEF,indef,NUM,_COUNT,possNP),
  possessiveNounPhrase(NP,NUM/PERS,GENDER,CASE,SQ).


%myself - we cant prefix neither with numeral nor with det
prepositionalNounPhrase(refl(NUMPERS,GENDER),NUMPERS,GENDER,acc,false)-->
    reflexive(NUMPERS,GENDER).
%to myself
prepositionalNounPhrase(prep(PREP,refl(NUMPERS,GENDER)),
                        NUMPERS,GENDER,PRECASE,false)-->
    prep(PREP), {PREP \= of},
    reflexive(NUMPERS,GENDER), {PRECASE=..[PREP,acc]} .
%each,your,old,friend's,red,car
prepositionalNounPhrase(det(DET,NUM/PERS,NP),NUM/PERS,GENDER,CASE,SQ)-->
    determ(DET,NUM,NUM0,possNP),
    possessiveNounPhrase(NP,NUM0/PERS,GENDER,CASE,SQ), {NP\=refl(_,_)}.
prepositionalNounPhrase(NP,NUMPERS,GENDER,CASE,SQ)-->
    possessiveNounPhrase(NP,NUMPERS,GENDER,CASE,SQ).
%in,my,old,friend's,red,car / (I looked out) of the window
prepositionalNounPhrase(prep(PREP,NP),NUMPERS,GENDER,PRECASE,false)-->
    prep(PREP),
    possessiveNounPhrase(NP,NUMPERS,GENDER,acc,false),
    {PRECASE=..[PREP,acc]} .
%two hard years ago / miles away - NUMPERS is irrelevant!!
prepositionalNounPhrase(postp(POSTP,NP),_,GENDER,POSTCASE,false)-->
    possessiveNounPhrase(NP,_NUMPERS,GENDER,acc,false),  postp(POSTP),
    {POSTCASE=..[POSTP,acc]} .
% Noun phrase with indefinite numerals (quantification)
prepositionalNounPhrase(numeral(indef,NR,NUM/PERS,NP),
                        NUM/PERS,GENDER,CASE,SQ)-->
    numeral(_,indef,NUM,NR), possessiveNounPhrase(NP,NUM/PERS,GENDER,CASE,SQ),
    {\+ functor(NP,pronoun,_), NP\=refl(_,_)}.


%...see the previous definition
%I, you, me, her etc...
modifiedNounPhrase(NP,NUMPERS,GENDER,CASE,SQ)-->
    prepositionalNounPhrase(NP,NUMPERS,GENDER,CASE,SQ).
%%mine, yours etc... can be only nominal!!!
%modifiedNounPhrase(NP,_NUM/_PERS,GENDER,nom)-->
%	prepositionalNounPhrase(NP,_,GENDER,poss).
%(I see) the girl with glasses
modifiedNounPhrase(post(NP,NP1),NUMPERS,GENDER,CASE,false)-->
    prepositionalNounPhrase(NP,NUMPERS,GENDER,CASE,false),
    prepositionalNounPhrase(NP1,_NUM_PERS,_GENDER,PREPCASE,false),
    {compound(PREPCASE)}, {NP1 \=prep(of,_), \+ functor(NP,proper,4)},
    {NP-NP1 \= prep(as,_)-prep(as,_) }.
%(I see) the girl that sings nicely.
%The car that was seen (in the street is nice.)
modifiedNounPhrase(that(subject,NP,VP),NUMPERS,GENDER,CASE,false)-->
    prepositionalNounPhrase(NP,NUMPERS,GENDER,CASE,false),
    {\+ functor(NP,proper,4)},
    connect(_THAT,subclause(that)),
    verbalPhrase(VP,TENSE,NUMPERS,_POL,_REF,PDEM,false),
    {emptyOptDem(PDEM)},
    {TENSE \= inf, TENSE \= past(participle),
     TENSE \= present(participle)}.
%The car that you drove yesterday is nice.)
modifiedNounPhrase(that(pendent,NP,CL),NUMPERS,GENDER,CASE,false)-->
    prepositionalNounPhrase(NP,NUMPERS,GENDER,CASE,false),
    {\+ functor(NP,proper,4)},
    connect(_THAT,subclause(that)),
    verbalClause(CL,_NUMP,declarative,TENSE,_SUBJ,REF,PDEM),
    {\+ emptyOptDem(PDEM), REF=''},
    {\+ \+ demSat(PDEM,CASE)}, %doesn't bind CASE
    {TENSE \= inf, TENSE \= past(participle),
     TENSE \= present(participle)}.
%The woman coming (is busy). / (I saw) you coming in the street.
modifiedNounPhrase(actpart(NP,VP),NUMPERS,GENDER,CASE,false)-->
    prepositionalNounPhrase(NP,NUMPERS,GENDER,CASE,false),
    {\+ functor(NP,proper,4)},
    verbalPhrase(VP,present(participle),NUMPERS,_POL,_X,PDEM,false),
    {emptyOptDem(PDEM)}.
%The woman seen by the teacher (is busy).
modifiedNounPhrase(passpart(NP,VP),NUMPERS,GENDER,CASE,false)-->
    prepositionalNounPhrase(NP,NUMPERS,GENDER,CASE,false),
    {\+ functor(NP,proper,4)},
    {NP \= pronoun(_,_,_)},
    verbalPhrase(VP,past(participle),NUMPERS,_POL,_X,PDEM,false),
    {mainVerb(VP,VERB), VERB\= have},
    {emptyOptDem(PDEM)}.
%The book to read (is on the desk).
modifiedNounPhrase(infinite(NP,VP),NUMPERS,GENDER,CASE,false)-->
    prepositionalNounPhrase(NP,NUMPERS,GENDER,CASE,false), inf(to),
    {\+ functor(NP,proper,4)},
    verbalPhrase(VP,inf,_NUM_PERS,_POL,_X,PDEM,false),
    {emptyOptDem(PDEM)}.
%for my parents both
modifiedNounPhrase(both(NP),pl/3,GENDER,CASE,false)-->
    prepositionalNounPhrase(NP,pl/3,GENDER,CASE,false),
    numeral(both,indef,pl,both).
%my 3 small daughters themselves - agreement!
modifiedNounPhrase(refl(NUMPERS,GENDER,CASE,NP),NUMPERS,GENDER,CASE,false)-->
    prepositionalNounPhrase(NP,NUMPERS,GENDER,CASE,false),
    reflexive(NUMPERS,GENDER).


nounPhrase(NP,NUMPERS,GENDER,CASE,SQ)-->
    modifiedNounPhrase(NP,NUMPERS,GENDER,CASE,SQ).
%in my house and in your car
nounPhrase(coordinating(CONN,NP1,NP2),NUMP,GENDER,CASE,false)-->
    modifiedNounPhrase(NP1,_NUM1P1,GENDER,CASE,false),
    connect(CONN,coordinating), {once(listConnective(CONN))},
    modifiedNounPhrase(NP2,NUM2P2,_GENDER,CASE,false),
    {\+ functor(NP1,prep,2), \+ functor(NP2,prep,2), CONN=and -> NUMP=pl/3;
    NUMP=NUM2P2}.
%either in my house or in your car
nounPhrase(correlative(CONN,NP1,NP2),NUMPERS,GENDER,CASE,false)-->
    connect(C1,corr),
    {correlative(CONN,C1,C2,nounPhrase)},
    modifiedNounPhrase(NP1,_NUM1_P1,_GENDER,_CAS,false),
    %%%% CASE agreement!!! is missing!!! *****
    connect(C2,corr), modifiedNounPhrase(NP2,NUMPERS,GENDER,CASE,false).

%how ... what ... when
interrPhrase(interr(WH,KIND),_,KIND)--> interr(WH,KIND),
    {KIND\==num}.

%(what name? how long? which red car? why? how? when? where?)
interrPhrase(interr(what,CASE,NP,NUM/3),NUM/3,CASE)-->
    interr(what,nom), possessiveNounPhrase(NP,NUM/3,_,CASE,false).

% which nice girl - how quick car
interrPhrase(interr(WH,CASE,NP,NUM/3),NUM/3,CASE)-->
    interr(WH,KIND), {whBeforeNounPhrase(WKIND), WKIND==KIND},
    adjectiveNounPhrase(NP,NUM/3,CASE,false).

%how quick (as an adjective) - how extremely quick
interrPhrase(interr(how,ADJP,adj),_NUM,adj)-->
    interr(how,adv(manner)), adjPhrase(ADJP,base).

%how long (an adverb)
interrPhrase(interr(how,adv(ADV,KIND,base),adv(KIND)),_NUM,adv(KIND))-->
    interr(how,adv(manner)),   adverb(ADV,KIND,base),
    {KIND \== part, KIND \== neg, KIND \== super, KIND \== link,
     KIND \== comp, KIND \== degree}.

%how many red apples
interrPhrase(interr(WH,num,NP,NUMPERS),NUMPERS,CASE)-->
    interr(WH,KIND), {KIND==num}, adjectiveNounPhrase(NP,NUMPERS,CASE,false).

%to whom (did your write a letter)
%under what (did he live)
interrPhrase(interr(WH,NUMPERS,CASE),NUMPERS,CASE)-->
    prep(PREP), interr(WH,ICASE),
    {once((ICASE=acc;ICASE=nom)), CASE=..[PREP,acc]}.

%to which girl (did your write a letter)
%under what name (did he live)
interrPhrase(interr(WH,CASE,NP,NUM/3),NUM/3,CASE)-->
    prep(PREP),
    interrPhrase(interr(WH,_,NP,NUM/3),NUM/3,WCASE),
    {once((WCASE=acc;WCASE=nom)), CASE=..[PREP,WCASE]}.


%Recognition of free arguments/adverbs...
%List of free arguments is limited to 3!!!!!
optFreeArgs([],X,X):- true.  %Contralog!! fact!!
%noun phrase
optFreeArgs([NP|FREES])-->nounPhrase(NP,_NUMPERS,_GENDER,CASE,false),
    {once((NP=prep(_,_), NP\=prep(of,_);
    NP=correlative(_,_,_);NP=det(_,_,_);
    NP=refl(_,_))), CASE \== gen},
      optFreeArgs(FREES), {FREES\=[_,_,_|_]}.
%(I had enough money) to buy a car - (a subclause of purpose)
%no other arguments after a subclause
optFreeArgs([verbal(to,VP)])-->
    prep(to), verbalPhrase(VP,inf,_NUMPERS,_POL,_X,DEM,false),
    {emptyOptDem(DEM)}.
%(I run) quickly
optFreeArgs([adv(ADV,KIND,base)|FREES])-->
    adverb(ADV,KIND,base),
    {KIND\=super, KIND\=comp, KIND \= part, KIND \= connecting(_)},
    optFreeArgs(FREES), {FREES\=[_,_,_|_]}.
%(I run) from here
optFreeArgs([prep(PREP,adv(ADV,KIND,base))|FREES])-->
    prep(PREP), adverb(ADV,KIND,base),
    {KIND\=super, KIND\=comp, KIND \= part, KIND \= connecting(_)},
    optFreeArgs(FREES), {FREES\=[_,_,_|_]}.
%(That sounds) nice
optFreeArgs([ADJP|FREES])-->
    adjPhrase(ADJP,base), {ADJP \= adj(verb(_,present(participle)),_)},
    optFreeArgs(FREES), {FREES\=[_,_,_|_]}.
%(I danced) last night
%optFreeArgs(adj(ADJ,base,ADV))-->
%    adject(ADJ,base), adverb(ADV,time,base),
%    optFreeArgs(FREES), {FREES\=[_,_,_|_]}.
%
% comparisons
optFreeArgs([COMP|FREES])-->
    comparison(COMP),
    optFreeArgs(FREES), {FREES\=[_,_,_|_]}.

reference(NP)-->
    nounPhrase(NP,_NUMPERS,_GENDER,acc,false).
reference(VCL)-->
    verbalClause(VCL, _NUM,declarative,_TENSE,_SUBJ,_X,DEM),
    {emptyOptDem(DEM)}.
reference(SVCL)-->
    shortVerbalClause(SVCL, _NUMP,declarative,_TENSE).
reference(adj(ADJ,base))--> adject(ADJ,base).

%(I run) quicker than my brother
comparison(comp(ADJP,>,REF))-->
    adjPhrase(ADJP,comp),  connect(than,comp), reference(REF).

%AP-lessthan-REF
comparison(comp(ADJ,<,REF))-->
    adjPhrase(ADJ,comp), {ADJ = adjP(adv(less,comp,base),_,_)},
    connect(than,comp), reference(REF).
%   nounPhrase(NP,_NUMPERS,_GENDER,acc,false).
%(I run) as quick as my brother
comparison(comp(ADJ,=,REF))-->
    connect(as,comp), adject(ADJ,base),
    connect(as,comp), reference(REF).
%As quickly as I could I ate my apple.
%NP V as-AP-as-SVCL (short verbal clause, as far as I could)
comparison(comp(ADV,=,REF))-->
    connect(as,comp), adverb(ADV,KIND,base),
    {KIND\=super, KIND\=comp, KIND \= part},
    connect(as,comp), reference(REF).

%at the moment only 2 adverbs are allowed
midAdverb(ADV,+)-->
    adverb(ADV,KIND,base), {midAdverbKind(KIND), KIND\=neg}.
midAdverb([ADV1,ADV2],+)-->
    adverb(ADV1,KIND1,base), {midAdverbKind(KIND1), KIND1\=neg},
    adverb(ADV2,KIND2,base), {midAdverbKind(KIND2), KIND2\=neg}.
midAdverb(ADV,-)-->
    adverb(ADV,neg,base).
midAdverb(ADV,-)-->
    adverb(_NOT,neg,base), adverb(ADV,KIND,base),
    {midAdverbKind(KIND), KIND\=neg}.


%drink - drinking - drunk
verbInfinite([V],TENSE,NUMPERS,SQ)-->
  verb(V,INF,NUMPERS,SQ), {inf2Tense(INF,TENSE,NUMPERS)}.
%...look forward - verb with verb-particle
verbInfinite([V-ADV],TENSE,NUMPERS,false)-->
  verb(V,INF,NUMPERS,false), {inf2Tense(INF,TENSE,NUMPERS)},
  adverb(ADV,part,base), {verbPart_(_,V,ADV,_)}.
%...(I may)...have drunk
verbInfinite([AUX1|VERB],TENSE,NUMPERS,SQ)-->
  aux(AUX1,A1T,NUMPERS,SQ),
  verbInfinite(VERB,VT,_,false),
  {once(auxTense2Inf(AUX1,A1T,VT,TENSE))}.
%...(I may) have never drunk ...
%Special case: adverb after the aux-verb (negation is forbidden)
verbInfinite([AUX1|VERB],TENSE,NUMPERS,ADV,SQ)-->
  aux(AUX1,A1T,NUMPERS,SQ), midAdverb(ADV,+), verbInfinite(VERB,VT,_,false),
  {once(auxTense2Inf(AUX1,A1T,VT,TENSE))}.

%for single verb tenses we generate a single atom
%for compound verb tenses we generate a list of atoms
verbConj(verb(V,TENSE,NUMPERS),'','',TENSE,NUMPERS,+,SQ)-->
  verbInfinite(V,TENS,NUMPERS,SQ),
  {tense2Conj(TENS,TENSE)}.
verbConj(verb(V,TENSE,NUMPERS),'','',TENSE,NUMPERS,-,SQ)-->
  verbInfinite(V,TENS,NUMPERS,SQ),
%  midAdverb(ADV,POL),
  adverb(ADV,neg,base),
  {\+ adj_(ADV,base,_,_), \+ det(_,ADV,_,_)},
  {tense2Conj(TENS,TENSE)}.
%you 'hardly drink' much wine
verbConj(verb(V,TENSE,NUMPERS),ADV,'',TENSE,NUMPERS,POL,false)-->
   midAdverb(ADV,POL), verbInfinite(V,TENS,NUMPERS,false),
   {tense2Conj(TENS,TENSE)}.
verbConj(verb([MODAL|VERB],TENSE,NUMPERS),'',MODE,TENSE,NUMPERS,+,false)-->
  modal(MODAL,MODE,MT,verb),
  verbInfinite(VERB,VT,_,false), {once(auxTense2Conj(MT,VT,TENSE))}.
%adverb after aux-verb (she may have always been nice).
verbConj(verb([MODAL|VERB],TENSE,NUMPERS),ADV,MODE,TENSE,NUMPERS,+,false)-->
  modal(MODAL,MODE,MT,verb),
  verbInfinite(VERB,VT,_,ADV,false), {once(auxTense2Conj(MT,VT,TENSE))}.
verbConj(verb([MODAL|VERB],TENSE,NUMPERS),ADV,MODE,TENSE,NUMPERS,POL,false)-->
  modal(MODAL,MODE,MT,verb), midAdverb(ADV,POL),
  verbInfinite(VERB,VT,_,false), {once(auxTense2Conj(MT,VT,TENSE))}.
verbConj(verb([AUX|VERB],TENSE,NUMPERS),'','',TENSE,NUMPERS,+,SQ)-->
  aux(AUX,AT,NUMPERS,SQ),
  verbInfinite(VERB,VT,_,false), {once(auxTense2Conj(AUX,AT,VT,TENSE))}.
verbConj(verb([AUX|VERB],TENSE,NUMPERS),ADV,'',TENSE,NUMPERS,POL,SQ)-->
  aux(AUX,AT,NUMPERS,SQ), midAdverb(ADV,POL),
  verbInfinite(VERB,VT,_,false), {once(auxTense2Conj(AUX,AT,VT,TENSE))}.


%verbalArgs(NPS)
%NPS:   recognized verbal arguments
%CASES: recognized cases (for noun phrases!)
%verbal (infinitive and participle) arguments and
%subclausal arguments must be the last!
verbalArgs([],[],X,X):- true.  %Contralog!! fact!!
verbalArgs([CLAUSE],[clause])-->
  clause(CLAUSE,declarative,straight,full,_).
verbalArgs([connect(that,CLAUSE)],[clause(that)])-->
  connect(that,subclause(that)), clause(CLAUSE,declarative,straight,full,_).
verbalArgs([connect(CONN,CLAUSE)],[clause(if)])-->
  connect(CONN,subclause(if)), clause(CLAUSE,declarative,straight,full,_).
verbalArgs([connect(IP,CLAUSE)],[clause(wh)])-->
  subClause(?(IP,CLAUSE),interrogative(SUB),straight,_COMPLETION),
  {once(SUB=wh;SUB=subj)}.
%let us 'go home'
verbalArgs([verbal(VP)],[verbal])-->
  verbalPhrase(VP,inf,_NUMPERS,_POL,_X,DEM,false),
  {emptyOptDem(DEM)}.
%where to find a restaurant
verbalArgs([verbal(IP,VP)],[verbal(wh)])-->
  interrPhrase(IP,_,_CASE), prep(to),
  verbalPhrase(VP,inf,_NUMPERS,_POL,_X,DEM,false), {emptyOptDem(DEM)}.
%i want 'to drink a beer'
verbalArgs([verbal(to,VP)],[verbal(to)])-->
  prep(to), verbalPhrase(VP,inf,_NUMPERS,_POL,_X,DEM,false),
  {emptyOptDem(DEM)}.
%i want 'to be a doctor'
verbalArgs([verbal(to,NPP)],[verbal(to)])-->
  prep(to), nominalPhrase(NPP,inf,_NUMPERS,_POL,false).
%we stopped 'talking'
verbalArgs([verbal(ing,VP)],[verbal(ing)])-->
  verbalPhrase(VP,present(participle),_NUMPERS,_POL,_X,DEM,false),
  {emptyOptDem(DEM)}.
verbalArgs([NP|NPS],[CASE|CASES])-->
	nounPhrase(NP,_NUMPERS,_GENDER,CASE,false),
        {accusativeCase(CASE)},
        verbalArgs(NPS,CASES).



%verbalPhrase(VP,TENSE,NUMPERS,POL,PEND,PDEM,SQ).
%VP: Verbal phrase structure
%TENSE: tense of the verb (indicative;???????)
%NUM: number of verb (pl/sg)
%POL: polarity: affirmative;negative
%PEND: Prolog variable to hold a pendant argument
%      (that is missing from arguments)
%PDEM: syntactic demand of evt pendant verbal argument or 'none'
%SQ:   binary flag: true if 's' was taken as 'is' or as 'has'
%
%eg. Which apple do you like?
%eg. In which town do you want to live?
%
%V-ARGS
verbalPhrase(vp(VERB,MADV,MODE,TENSE,ARGS,FREE),TENSE,NUMPERS,POL,X,PDEM,SQ)-->
%an active verb with an argument structure
%eg. '(I) see my friend'
%! !! is driven by my mother --> will not recognized as verbal phrase
    verbConj(VERB,MADV,MODE,TENSE,NUMPERS,POL,SQ),
    {VERB\=verb([be],_,_)},
    {TENSE\=past(participle), \+ functor(TENSE,passive,1)},
    {verbAPartFrame(_,VERB,[nom|FRAMES])},
    {butLastNominalDemands(FRAMES)},
    verbalArgs(ARGS0,CASES),
    {but1DemSat(FRAMES,CASES,ARGS0,ARGS,PDEM,X)},
    optFreeArgs(FREE), {\+ (VERB=verb([do],_,_), ARGS=[], FREE=[])},
    {\+ freeMatchesFrame(FRAMES,FREE)}.

%V-ARGS-PART
verbalPhrase(vp(VERB,MADV,MODE,TENSE,ARGS,FREE),TENSE,NUMPERS,POL,X,PDEM,SQ)-->
    verbConj(VERB0,MADV,MODE,TENSE,NUMPERS,POL,SQ),
    {VERB0=verb(VS0,TENSE,NP), append(VAUX,[VL],VS0), VL\=be},
    {TENSE\=past(participle), \+ functor(TENSE,passive,1)},
    verbalArgs(ARGS0,CASES), {ARGS0=[_|_]},
    adverb(PART,part,base),
    {verbAPartFrame(_,VL-PART,[nom|FRAMES])},
    {butLastNominalDemands(FRAMES)},
    {but1DemSat(FRAMES,CASES,ARGS0,ARGS,PDEM,X)},
    optFreeArgs(FREE), {\+ (VL=do, ARGS=[], FREE=[])},
    {\+ freeMatchesFrame(FRAMES,FREE),
    append(VAUX,[VL^PART],VS), VERB=verb(VS,TENSE,NP)}.
verbalPhrase(vp(VERB,MADV,MODE,TENSE,ARGS,FREE),TENSE,NUMPERS,POL,X,PDEM,SQ)-->
%a verb with a passive argument structure
%for transitive verbs, i.e. the second arg is acc
%eg. '(A ship) seen by my friend (is nice). ' <-- TENSE=past(participle)
%eg: 'The book was written by my friend.' <-- functor(TENSE,passive,1)
    verbConj(VERB,MADV,MODE,TENSE,NUMPERS,POL,SQ),
    {VERB\=verb([be],_,_)},
    {once(TENSE=past(participle);
      functor(TENSE,passive,1))},
    verbalArgs(ARGS0,CASES),
    {verbAPartFrame(_,VERB,[nom|ALLFRAMES]),
    (ALLFRAMES= [ACC|FRAMES], ACC=acc,
      butLastNominalDemands(FRAMES)->
      but1DemSat([?(by(acc))|FRAMES],CASES,ARGS0,ARGS,PDEM,X))},
      optFreeArgs(FREE), {FREE \= [prep(by,_)|_] }.

%predicate is nominal, etc.'(my car is) RED/a vehicle' ????
%nominal predicate may refer to the subject
%nominalPredicate(NPP,NUM).
%?NUM: sg/pl
%-NPP: noun phrase structure
%
%NP: you are 'a doctor' -- NUM agreement!
%NP: you are 'in a car' -- no NUM agreement!
nominalPredicate(NP,NUM)-->
    nounPhrase(NP,NUMNP/_PERS,_GENDER,CASE,false), {NP \= refl(_,_)},
    {accusativeCase(CASE)},
    {NP \= prep(_,_)->NUM=NUMNP;true}.
nominalPredicate(adv(ADV,KIND,base),_NUM)-->
    adverb(ADV,KIND,base), {nomPredAdvKind(KIND)}.
%ADV-NP: you are 'really a doctor'
nominalPredicate(adv(ADV,NP),NUM)-->
    adverb(ADV,KIND,base), {preAdjAdvKind(KIND)},
    nounPhrase(NP,NUM/_PERS,_GENDER,CASE,false),
    {accusativeCase(CASE)}, {NP \= refl(_,_)}.
%AP "really sweeter"
nominalPredicate(ADJP,_NUM)--> adjPhrase(ADJP,_GRADE),
    {ADJP \= adjP(adv(_,comp,base),_,_)}.
%ORD - 3-rd
nominalPredicate(ord(NR),NUM)-->numeral(_,ord,NUM,NR).
%less strong than you brother
nominalPredicate(COMP,_NUM)-->comparison(COMP).
%(The temperature is) 30 degrees
nominalPredicate(QUANT,_NUMPERS)-->quantity(QUANT,_KIND,_,_CASE).
%My car is like yours but quicker.
nominalPredicate(connected(CONN,NPP1,NPP2),NUMP)-->
    nominalPredicate(NPP1,NUMP), connect(CONN,coordinating),
    nominalPredicate(NPP2,NUMP).
%(That is) where I keep all my stuff
nominalPredicate(SUBCL,_NUMPERS)-->
    clause(SUBCL,declarative,straight,full,_),
    {SUBCL=clause(_,_,[FREE],_,_,_,_),
    FREE=adv(where,place,base)}.

%(I) am your automatic lover
nominalPhrase(vp(verb(V,TENSE,NUM/PERS),ADV,MODE,TENSE,[NPP],FREE),
                  TENSE,NUM/PERS,POL,SQ)-->
    verbConj(verb(V,TENSE,NUM/PERS),ADV,MODE,TENSE,NUM/PERS,POL,SQ),
    {last(V,be)},
    {TENSE \= futurePast},
    nominalPredicate(NPP,NUM), {NPP \= adj(verb(_,_),_)},
    optFreeArgs(FREE).

%who 'are you'/'am I' - this is predicate interrogative
%'you' is subject
%predInterrogativeNominalClause(clause(nominal/KIND,inverted,[],NPS,
%       vp(verb(V,TENSE,NUM/PERS),ADV,MODE,TENSE,[NPP],FREE),NUM/PERS,+),
%                                    NPS,KIND)-->
predInterrogativeNominalClause(clause(nominal/KIND,inverted,[],NPS,
       vp(verb(V,TENSE,NUM/PERS),ADV,MODE,TENSE,[''],FREE),NUM/PERS,POL),
                                    NPS,KIND)-->
    verbConj(verb(V,TENSE,NUM/PERS),ADV,MODE,TENSE,NUM/PERS,POL,false),
    {last(V,be)}, {TENSE \= futurePast},
    nounPhrase(NPS,NUM/PERS,_GENDER,nom,false),
    optFreeArgs(FREE),
    {NPS \= common(_,_,_), NPS \= art(_,_,_,_) },
    {NPS \= poss(_,_,_), NPS \=actpart(_,_)},
    {KIND=interrogative(pred)}.

% what about 'a cold drink' / predicate 'about' interrogative
% 'cold drink' is subject
aboutInterrogativeNominalClause(clause(nominal/KIND,inverted,[],NPS,
            '',NUM/PERS,+),NPS,KIND)-->
   nounPhrase(NPS,NUM/PERS,_GENDER,acc,false), {KIND=interrogative(about)}.

%FREE1-NPSUBJ-NOMPRED-FREE2
%"I am hungry now"
nominalClause(clause(nominal/KIND,straight,FREE,NPS,NPP,NUMPERS,POL),
              TENSE,KIND,NPS)-->
   optFreeArgs(FREE),
   nounPhrase(NPS,NUMPERS,_GENDER,nom,SQ), %{KIND=declarative},
   nominalPhrase(NPP,TENSE,NUMPERS,POL,SQ).

%VERB.BE-NPSUBJ-FREE (VSO)
%There "are animals in the house".
oppositeNominalClause(clause(nominal/KIND,opposite,[],NPS,
            vp(verb(be,TENSE,NUM/PERS),'',MODE,TENSE,[],FREE),
                             NUM/PERS,POL),KIND)-->
   auxMod(be,MODE,TENSE,NUM/PERS,POL), {TENSE \= futurePast, TENSE \= future},
   nounPhrase(NPS,NUM/PERS,_GENDER,nom,false),
   optFreeArgs(FREE).

%AUX-VERB.BE-NPSUBJ-FREE (VSO)
%There "could have been animals in the house".
oppositeNominalClause(clause(nominal/KIND,opposite,[],NPS,
            vp(verb(VERB,TENSE,NUM/PERS),'',MODE,TENSE,[],FREE),
                             NUM/PERS,POL),KIND)-->
   auxMod(AUX,MODE,AT,NUM/PERS,POL),
   verbInfinite(VERB,VT,NUM/PERS,false), {last(VERB,be)},
   {once(auxModTense2Conj(AUX,AT,VT,TENSE))},
   nounPhrase(NPS,NUM/PERS,_GENDER,nom,false),
   optFreeArgs(FREE).

%VERB.BE-NPSUBJ-NOMPRED-FREE2
%"Are you hungry now?"
invertedNominalClause(clause(nominal/KIND,inverted,[],NPS,
            vp(verb([be],TENSE,NUM/PERS),'',MODE,TENSE,[NPP],FREE),
                             NUM/PERS,POL),KIND,NPS)-->
   auxMod(be,MODE,TENSE,NUM/PERS,POL), {TENSE \= futurePast, TENSE \= future},
   nounPhrase(NPS,NUM/PERS,_GENDER,nom,false),
   nominalPredicate(NPP,NUM), {NPP \= adj(verb(_,_),_)},
   optFreeArgs(FREE).

%VERB.BE-NPSUBJ-FREE2
%(Who) am I?" - empty predicate - interrogative word
invertedNominalClause(clause(nominal/KIND,inverted,[],NPS,
            vp(verb([be],TENSE,NUM/PERS),'',MODE,TENSE,[''],FREE),
                             NUM/PERS,POL),KIND,NPS)-->
   auxMod(be,MODE,TENSE,NUM/PERS,POL), {TENSE \= futurePast, TENSE \= future},
   nounPhrase(NPS,NUM/PERS,_GENDER,nom,false),
   optFreeArgs(FREE).

%AUXMOD-NPSUBJ-VERBINFINITE-NOMPRED-FREE2
%"Have you been hungry?"  "Can you be hungry?"
invertedNominalClause(clause(nominal/KIND,inverted,[],NPS,
            vp(verb([AUX|VERB],TENSE,NUM/PERS),'',MODE,TENSE,[NPP],FREE),
                             NUM/PERS,POL),KIND,NPS)-->
   auxMod(AUX,MODE,AT,NUM/PERS,POL), nounPhrase(NPS,NUM/PERS,_GENDER,nom,false),
   verbInfinite(VERB,VT,NUM/PERS,false), {last(VERB,be)},
   {once(auxModTense2Conj(AUX,AT,VT,TENSE))},
   nominalPredicate(NPP,NUM), {NPP \= adj(verb(_,_),_)},
   optFreeArgs(FREE).

%AUXMOD-NPSUBJ-ADV-VERBINFINITE-NOMPRED-FREE2
%"Have you ever been to London?" "Can she always be hungry?"
invertedNominalClause(clause(nominal/KIND,inverted,[],SUBJ,
            vp(verb([AUX|VERB],TENSE,NUM/PERS),ADV,MODE,TENSE,[NPP],FREE),
                             NUM/PERS,POL),KIND,SUBJ)-->
   auxMod(AUX,MODE,AT,NUM/PERS,POL),
   nounPhrase(SUBJ,NUM/PERS,_GENDER,nom,false),
   midAdverb(ADV,+), verbInfinite(VERB,VT,NUM/PERS,false), {last(VERB,be)},
   {once(auxModTense2Conj(AUX,AT,VT,TENSE))},
   nominalPredicate(NPP,NUM), {SUBJ \= adj(verb(_,_),_)},
   optFreeArgs(FREE).


addressing(proper(PROP,human,NUM,nom),NUM)-->
    proper(PROP,human,NUM,nom).


%invertedVerbalClause(CLAUSE,NUMPERS,KIND,TENSE,SUBJ,REF,DEM)
%The predicate handles pendent subject
%and pendent/missing piece of a noun phrase in argument list
%This is used for topicalization
%Eg: complete pendent arg:
%    - which apple do you want
% @arg CLAUSE clause structure
% @arg NUMPERS sg/1...pl/3 - number and person of missing argument
% @arg KIND ...
% @arg TENSE ... tense expression ...
% @arg SUBJ  subject of the inverted clause
% @arg REF Referred NP structure
%      (eg. a topic, usually a var) embedded in/referred by CLAUSE
% @arg DEM: syntactic demand for missing argument/piece of arg


%'does my sister like ice cream'
%subject+verbal phrase+free args before and/or after
%AUX/MOD-NPSUBJ-(NOT)-VP
%interrogated/inverted verbal clause
%a y/n question with aux and infinitive verb (Have I eaten an apple?)
invertedVerbalClause(clause(verbal/KIND,inverted,[],SUBJ,
        vp(verb([AUX|VERB],TENSE,NUMPERS),
           '',MODE,TENSE,ARGS,FREE),NUMPERS,POL),
                     NUMPERS,KIND,TENSE,SUBJ,REF,DEM)-->
   auxMod(AUX,MODE,AT,NUMPERS,POL),
   nounPhrase(SUBJ,NUMPERS,_GENDER,nom,false),
   verbInfinite(VERB,VT,NUMPERS,false), {last(VERB,V),V\=be},
   {verbAPartFrame(_,V,[nom|FRAMES])},
   {once(auxModTense2Conj(AUX,AT,VT,TENSE)), \+ functor(TENSE,passive,_)},
   verbalArgs(ARGS0,CASES),
   {but1DemSat(FRAMES,CASES,ARGS0,ARGS,DEM,REF)},
   optFreeArgs(FREE).
%the same with verb particle thrown to the end
invertedVerbalClause(clause(verbal/KIND,inverted,[],SUBJ,
        vp(verb([AUX|VERB],TENSE,NUMPERS),
           '',MODE,TENSE,ARGS,FREE),NUMPERS,POL),
                     NUMPERS,KIND,TENSE,SUBJ,REF,DEM)-->
   auxMod(AUX,MODE,AT,NUMPERS,POL),
   nounPhrase(SUBJ,NUMPERS,_GENDER,nom,false),
   verbInfinite(VERB0,VT,NUMPERS,false),
   {append(VAUX,[VL],VERB0), VL\=be,
   VT\=past(participle)},
   verbalArgs(ARGS0,CASES),
   adverb(PART,part,base),
   {verbAPartFrame(_,VL-PART,[nom|FRAMES])},
   {once(auxModTense2Conj(AUX,AT,VT,TENSE)), \+ functor(TENSE,passive,_)},
   {but1DemSat(FRAMES,CASES,ARGS0,ARGS,DEM,REF)},
   optFreeArgs(FREE), {\+ freeMatchesFrame(FRAMES,FREE)},
   {append(VAUX,[VL^PART],VERB)}.
%interrogated/inverted negated passive verbal clause
% a y/n question with aux and infinitive verb
%AUXNOT-NPSUBJ-VERBINF-ARGS-FREE
%(Hasn't the apple been eaten?)
invertedVerbalClause(clause(verbal/KIND,inverted,[],SUBJ,
        vp(verb([AUX|VERB],TENSE,NUMPERS),
          '',MODE,TENSE,ARGS,FREE),NUMPERS,POL),
                     NUMPERS,KIND,TENSE,SUBJ,REF,DEM)-->
   auxMod(AUX,MODE,AT,NUMPERS,POL),
   nounPhrase(SUBJ,NUMPERS,_GENDER,nom,false),
   verbInfinite(VERB,VT,NUMPERS,false),
   {last(VERB,V),V \= be},
   {verbAPartFrame(_,V,[nom,ACC|FRAMES])},
   {once(accusativeDemand(ACC))},
   {once(auxModTense2Conj(AUX,AT,VT,TENSE)), functor(TENSE,passive,_)},
   verbalArgs(ARGS0,CASES),
   {but1DemSat([?(by(acc))|FRAMES],CASES,ARGS0,ARGS,DEM,REF)},
   optFreeArgs(FREE), {FREE \= [prep(by,_)|_] }.

%interrogated/inverted verbal clause with mid adverb
% a y/n question with aux and infinitive verb
%AUX-NPSUBJ-ADV-VERB-ARGS-FREE
%(Have you ever been there?)
invertedVerbalClause(clause(verbal/KIND,inverted,[],SUBJ,
        vp(verb([AUX|VERB],TENSE,NUMPERS),
           ADV,MODE,TENSE,ARGS,FREE),NUMPERS,POL),
                     NUMPERS,KIND,TENSE,SUBJ,REF,DEM)-->
   auxMod(AUX,MODE,AT,NUMPERS,POL), nounPhrase(SUBJ,NUMPERS,_GENDER,nom,false),
   midAdverb(ADV,+),
   verbInfinite(VERB,VT,NUMPERS,false),
   {last(VERB,V),V \= be},
   {verbAPartFrame(_,V,[nom|FRAMES])},
   {once(auxModTense2Conj(AUX,AT,VT,TENSE)), \+ functor(TENSE,passive,_)},
   verbalArgs(ARGS0,CASES),
   {but1DemSat(FRAMES,CASES,ARGS0,ARGS,DEM,REF)},
   optFreeArgs(FREE).
%the same with a verb particle thrown to the end
invertedVerbalClause(clause(verbal/KIND,inverted,[],SUBJ,
        vp(verb([AUX|VERB],TENSE,NUMPERS),
           ADV,MODE,TENSE,ARGS,FREE),NUMPERS,POL),
                     NUMPERS,KIND,TENSE,SUBJ,REF,DEM)-->
   auxMod(AUX,MODE,AT,NUMPERS,POL),
   nounPhrase(SUBJ,NUMPERS,_GENDER,nom,false),
   midAdverb(ADV,+),
   verbInfinite(VERB0,VT,NUMPERS,false),
   {append(VAUX,[VL],VERB0), VL\=be,
   VT\=past(participle)},
   verbalArgs(ARGS0,CASES),
   adverb(PART,part,base),
   {verbAPartFrame(_,VL-PART,[nom|FRAMES])},
   {once(auxModTense2Conj(AUX,AT,VT,TENSE)), \+ functor(TENSE,passive,_)},
   {but1DemSat(FRAMES,CASES,ARGS0,ARGS,DEM,REF)},
   optFreeArgs(FREE), {\+ freeMatchesFrame(FRAMES,FREE)},
   {append(VAUX,[VL^PART],VERB)}.
%recognizing an adverb-modified PASSIVE yn question
%AUX-NPSUBJ-ADV-VERB-ARGS-FREE
%Has this apple ever been eaten by me?
invertedVerbalClause(clause(verbal/KIND,inverted,[],SUBJ,
        vp(verb([AUX|VERB],TENSE,NUMPERS),
           ADV,MODE,TENSE,ARGS,FREE),NUMPERS,POL),
                     NUMPERS,KIND,TENSE,SUBJ,REF,DEM)-->
   auxMod(AUX,MODE,AT,NUMPERS,POL), nounPhrase(SUBJ,NUMPERS,_GENDER,nom,false),
   midAdverb(ADV,+),
   verbInfinite(VERB,VT,NUMPERS,false),
   {last(VERB,V),V \= be},
   {verbAPartFrame(_,V,[nom,ACC|FRAMES])},
   {once(accusativeDemand(ACC))},
   {once(auxModTense2Conj(AUX,AT,VT,TENSE)), functor(TENSE,passive,_)},
   verbalArgs(ARGS0,CASES),
   {but1DemSat([?(by(acc))|FRAMES],CASES,ARGS0,ARGS,DEM,REF)},
   optFreeArgs(FREE), {FREE \= [prep(by,_)|_] }.


%where to build a house
%WH-to-VInf-VArgs-FREE
% interrogativeVerbalClause(clause(interrogative,inverted,FREE,wh(WH),VP))-->
% verbalPhrase(VP,TENSE,NUM/PERS,POL,PEND,PDEM,false),
% optFreeArgs(FREEARGS), interrogative(WH,KIND), {KIND \= nom}.

%verbalClause(CLAUSE,NUMPERS,SUBJ,REF,DEM)
%The predicate handles pendent subject
%and pendent/missing piece of a nounphrase in argument list
%This is used for topicalization
%Eg: complete pendent arg:
%    - which apple do you want
% @arg CLAUSE clause structure
% @arg NUMPERS sg;pl / 1..3 - number and person of clause
% @arg SUBJ    subject of the clause
% @arg REF Referred NP structure
%      (eg. a topic, usually a var) embedded in/referred by CLAUSE
% @arg DEM: syntactic demand for missing argument/piece of arg


% yesterday my sister had a party
% free before+subject+verbal phrase+free args after
% FREE1-NPSUBJ-VP-FREE2
verbalClause(clause(verbal/KIND,straight,FREE,SUBJ,VP,NUMPERS,POL),
             NUMPERS,KIND,TENSE,SUBJ,REF,PDEM)-->
   optFreeArgs(FREE),
   nounPhrase(SUBJ,NUMPERS,_GENDER,nom,SQ),
   verbalPhrase(VP,TENSE,NUMPERS,POL,REF,PDEM,SQ),
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)} .


shortVerbalClause(clause(verbal/declarative,straight,FREE,NP,
                         vp(verb(AUX,TENSE,NUMPERS),
                            '',MODE,TENSE,[],[]),NUMPERS,POL),
             NUMPERS,declarative,TENSE)-->
   optFreeArgs(FREE),
   nounPhrase(NP,NUMPERS,_GENDER,nom,false),
   {\+ functor(NP,actpart,_), \+ functor(NP,passpart,_)},
   auxMod(AUX,MODE,TENSE,NUMPERS,POL),
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)}.
shortVerbalClause(clause(verbal/declarative,straight,FREE,NP,
                         vp(verb([AUX,''],TENSE,NUMPERS),
                            ADV,MODE,TENSE,[],[]),NUMPERS,POL),
             NUMPERS,declarative,TENSE)-->
   optFreeArgs(FREE),
   nounPhrase(NP,NUMPERS,_GENDER,nom,false),
   {\+ functor(NP,actpart,_), \+ functor(NP,passpart,_)},
   midAdverb(ADV,+),
   auxMod(AUX,MODE,TENSE,NUMPERS,POL),
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)}.

shortInvertedVerbalClause(clause(verbal/declarative,inverted,[],NP,
                         vp(verb([AUX,''],TENSE,NUMPERS),
                            '',MODE,TENSE,[],[]),NUMPERS,POL),
             NUMPERS,declarative,TENSE)-->
   auxMod(AUX,MODE,TENSE,NUMPERS,POL),
   {TENSE \= inf, TENSE \= past(participle), TENSE \= present(participle)},
   nounPhrase(NP,NUMPERS,_GENDER,nom,false),
   {\+ functor(NP,actpart,_), \+ functor(NP,passpart,_)}.


%I do not know 'where she has gone'. INTERR-SUBJ-PRED-ARGS
%I do not know 'why I like my teacher'.
subClause(?(IP,CL),interrogative(wh),straight,full)-->
    {debug(enParser,'interrogative+straight subclause',[])},
    interrPhrase(IP,NUM,KIND),
    verbalClause(CL,NUM,_KIND,_TENSE,_SUBJ,_NP,DEM),
    {emptyOptDem(DEM)->
       (nonvar(KIND), KIND=adv(_)->true;
        compound(KIND), arg(1,KIND,acc)->true);
    nonvar(DEM), interrSatDem(DEM,KIND)->
       (nonvar(KIND),KIND=adv(_)->true;
        compound(KIND), arg(1,KIND,acc)->true)
    }.
%I do not know 'who wrote this book'. INTERR(subj)-PRED-ARGS
%I do not know 'why I like my teacher'.
subClause(?(IP,CL),interrogative(wh),straight,full)-->
    {debug(enParser,'interrogative+straight subclause',[])},
    interrPhrase(IP,NUM,nom),
    verbalPhrase(VP,_TENSE,NUM,POL,_,DEM,false),
    {emptyOptDem(DEM)},
    {CL=clause(verbal/declarative,straight,[],'',VP,NUM,POL)}.
%I do not know 'who it was'. INTERR-SUBJ-SHORTPRED
subClause(?(IP,CL),interrogative(wh),straight,short)-->
   {debug(enParser,'interrogative+straight short subclause',[])},
   interrPhrase(IP,NUM,KIND), {ignore(KIND=nom)},
   shortVerbalClause(CL,NUM/_PERS,_,_TENSE).

%
%clause(CLAUSE,MODE,TYPE,KIND,SUBJ).
% @arg MODE=declarative;interrogative(yn);interrogative(pred)
%           interrogative(wh),interrogative(subj);interj
% @arg TYPE=straight;inverted;infinite;interj
%%%%% @arg KIND=nominal;verbal NOT YET IMPLEMENTED

%'What about a cold drink?'
clause(?(IP,CL),
       interrogative(about),straight,full,SUBJ)-->
    {debug(enParser,'Pred interrogative (what about)',[])},
    interrPhrase(IP,_NUMPERS,KIND), {KIND==pred},
    aboutInterrogativeNominalClause(CL,SUBJ,interrogative(about)).

%'I like my teacher'
clause(CL,KIND,straight,full,SUBJ)-->
    {debug(enParser,'simple verbal clause',[])},
    verbalClause(CL,_NUM,KIND,_TENSE,SUBJ,_NP,DEM), {emptyOptDem(DEM)}.

%'I eat apple and drink beer.'
clause(clause(verbal/KIND,straight,
           FREE,SUBJ,coordinating(CONN,[VP1,VP2]),
           NUMPERS,POL),KIND,straight,full,SUBJ)-->
    {debug(enParser,'simple verbal clause',[])},
    verbalClause(clause(verbal/KIND,straight,FREE,SUBJ,VP1,
                        NUMPERS,POL),
                 NUMPERS,KIND,TENSE1,SUBJ,_X1,DEM), {emptyOptDem(DEM)},
    connect(CONN,coordinating),
    verbalPhrase(VP2,TENSE,NUMPERS,_POL,_X2,PDEM,false),
    {TENSE1 \=inf, TENSE \= inf, emptyOptDem(PDEM)}.

%'Thank you' - subjectless
%other fake parses are also generated!! ??????
clause(clause(verbal/KIND,straight,[],'',VP,
           NUM/1,POL),KIND,straight,subjless,'')-->
    {debug(enParser,'subjectless verbal clause',[])},
    verbalPhrase(VP,present,NUM/1,POL,_X,PDEM,false),
    {emptyOptDem(PDEM), KIND=declarative}.

%'Eat apple' - imperative
clause(clause(verbal/KIND,straight,[],'',VP,
           NUMPERS,POL),KIND,straight,full,'')-->
    {debug(enParser,'verbal imperative clause',[])},
    verbalPhrase(VP,inf,NUMPERS,POL,_X,PDEM,false),
    {emptyOptDem(PDEM), KIND=imperative(_)}.

%'Be quiet' - imperative
%clause(clause(nominal/KIND,straight,[],'',NPP,
%           NUMPERS,POL),KIND,straight,full,'')-->
clause(clause(nominal/imperative(IKIND),straight,[],'',NPP,
           NUMPERS,POL),imperative(IKIND),straight,full,'')-->
    {debug(enParser,'nominal imperative clause',[])},
    nominalPhrase(NPP,inf,NUMPERS,POL,false).

%'Please be quiet' - imperative
clause(clause(CLKIND,straight,[interj(IJ)],'',PRED,
           NUMPERS,POL),KIND,straight,full,SUBJ)-->
    {debug(enParser,'imperative clause with please',[])},
    interj(please,IJ),
    clause(clause(CLKIND,straight,[],'',PRED,
           NUMPERS,POL),KIND,straight,full,SUBJ).

%'Do you like your teacher?'
clause(?(CL),interrogative(yn),inverted,full,SUBJ)-->
    {debug(enParser,'Y/N interrogative verbal clause',[])},
    invertedVerbalClause(CL, _NUMPERS,interrogative(yn),_TENSE,SUBJ,_X,DEM),
    {emptyOptDem(DEM)}.

% Subject interrogative questions with linear word order
% and free args at the end... verb must be sg/pl but always pers3
clause(?(IP,CL),
       interrogative(subj),straight,full,IP)--> %'Who likes your teacher' ?
        %'What is parking outside' ? --- 'Who wrote this book' ?
        %'How many apples stand here' ?
        %'How many apples can be seen here' ?
    {debug(enParser,'Subject interrogative verbal clause',[])},
    interrPhrase(IP,NUM/PERS,nom),
    verbalPhrase(VP,TENSE,NUM/PERS,POL,_,DEM,false),
    {PERS==3, TENSE\=inf}, {emptyOptDem(DEM)},
    {CL=clause(verbal/interrogative(subj),straight,[],IP,VP,NUM/PERS,POL)}.

%'To whom did you write a letter?' - %'Why do I like my teacher?'
clause(?(IP,CL),
       interrogative(wh),inverted,full,SUBJ)-->
    {debug(enParser,'interrogative+verbal question',[])},
    interrPhrase(IP,_NUMPERSNP,KIND), {KIND \== nom},
    invertedVerbalClause(CL,_NUMPERSPV,interrogative(wh),_TENSE,SUBJ,X,DEM),
    {emptyOptDem(DEM)-> nonvar(KIND),
      once(KIND=adv(_); functor(KIND,_,1));
    atom(DEM)->X=IP,demSat(DEM,KIND);
    compound(KIND)->X=IP,demSat(DEM,KIND)}.

%'Whom did you send a letter to?' - verb arg
%'Whom did you travel in Europe with?' - free arg ???????????
clause(?(IPP,CL),
   interrogative(wh),inverted,full,SUBJ)-->
    {debug(enParser,'interrogative+verbal question',[])},
    interrPhrase(IP,_NUMPERSNP,KIND), {once(backPrepInterr(KIND))},
    invertedVerbalClause(CL,_NUMPERSVP,interrogative(wh),_TENSE,SUBJ,X,DEM),
    prep(PREP),
    {(\+ emptyOptDem(DEM), IKIND=..[PREP,KIND],
       interrSatDem(DEM,IKIND),
       insertInterrPrep(IP,PREP,IPP), X=IPP)}.

%'my father is a doctor'
clause(CL,KIND,straight,full,SUBJ)-->
    {debug(enParser,'simple nominal clause',[])},
    nominalClause(CL,TENSE,KIND,SUBJ), {TENSE \= inf}.

%'is my mother a teacher' ?
clause(?(CL),interrogative(yn),inverted,full,SUBJ)-->
    {debug(enParser,'yn nominal question',[])},
    invertedNominalClause(CL,interrogative(yn),SUBJ).

%'There are some apples on the table'.
clause(clause(nominal/declarative,DIR,[adv(ADV,place,base)],
                                        SUBJ,VP,NUMPERS,POL),
       declarative,inverted,full,SUBJ)-->
    {debug(enParser,'there/here are/is nominals',[])},
    adverb(ADV,place,base), {once(ADV=here;ADV=there)},
    oppositeNominalClause(clause(nominal/declarative,
                          DIR,[],SUBJ,VP,NUMPERS,POL),
                          declarative).

%I can / Could I
clause(clause(verbal/declarative,DIR,FREE,SUBJ,VP,NUMPERS,POL),
             declarative,DIR,short,SUBJ)-->
    {debug(enParser,'short declaration/question',[])},
    shortVerbalClause(clause(verbal/declarative,DIR,FREE,SUBJ,
                         VP,NUMPERS,POL),
             NUMPERS,declarative,_TENSE).

%'could I'?
clause(clause(verbal/declarative,DIR,[],
              SUBJ,VP,NUMPERS,POL),declarative,inverted,short,SUBJ)-->
    {debug(enParser,'short inverted question tag',[])},
    shortInvertedVerbalClause(clause(verbal/declarative,
             DIR,[],SUBJ,VP,NUMPERS,POL),NUMPERS,declarative,_TENSE).

%'nor could I'
clause(clause(verbal/declarative,DIR,[connect(nor,coordinating)],
              SUBJ,VP,NUMPERS,+),declarative,inverted,short,SUBJ)-->
    {debug(enParser,'short inverted negative declaration',[])},
    connect(nor,coordinating),
    shortInvertedVerbalClause(clause(verbal/declarative,
             DIR,[],SUBJ,VP,NUMPERS,+),NUMPERS,declarative,_TENSE).

% 'so could I'
clause(clause(verbal/declarative,DIR,[connect(so,subordinating)],
              SUBJ,VP,NUMPERS,+),declarative,inverted,short,SUBJ)-->
    {debug(enParser,'short inverted positive declaration',[])},
    connect(so,subordinating),
    shortInvertedVerbalClause(clause(verbal/declarative,
             DIR,[],SUBJ,VP,NUMPERS,+),NUMPERS,declarative,_TENSE).

%'why is your mother a teacher'? - 'where is the bath'?
%'Who am I'? 'Who are you'? - this is a predicative question
clause(?(IP,CL),interrogative(wh),inverted,full,SUBJ)-->
    {debug(enParser,'wh interrogative',[])},
    interrPhrase(IP,_NUMPERS,KIND),
    {nonvar(KIND), once(KIND=adv(_);KIND\=nom)},
    invertedNominalClause(CL,interrogative(wh),SUBJ),
    {CL= clause(_,_,[],SUBJ,vp(_V,_,_MODE,_TENSE,_NPP,_FREE),_,_)}.

%'who is doctor'? - subject interrogative with straight word order
clause(?(IP,clause(nominal/interrogative(wh),straight,[],IP,VP,NUM/3,+)),
         interrogative(wh),straight,full,IP)-->
    {debug(enParser,'wh subject interrogative',[])},
    interrPhrase(IP,NUM/3,nom),
    nominalPhrase(VP,_TENSE,NUM/3,+,false),
    {VP=vp(_V,_,_MODE,_TENSE,[NPP],_FREE), NPP\=proper(_,_,_,_),
    NPP\=coordinating(_,proper(_,_,_,_),proper(_,_,_,_))}.

%'who was your father'? (eg. president)
%predicate interrogative, inverted (subject:your father)
clause(?(IP,CL),KIND,inverted,full,IP)-->
    {debug(enParser,'wh pred-interrogative',[])},
    interrPhrase(IP,_,IKIND),
    {nonvar(IKIND), once(IKIND=adv(_);IKIND=nom)},
    predInterrogativeNominalClause(CL,_,KIND).

%'wow' | 'yes' | 'no'
clause(interj(IJ),interj,interj,interj,'')-->
    {debug(enParser,'interjection as a clause',[])},
    interj(_,IJ).

:-if(switchOut).
%clause(CL,declarative,_,_)--> %'Hungry'-->I am hungry
%	{debug(enParser,'subjectless nominal clause',[])},
%	subjlessNominalClause(CL).
%clause(?(CL),interrogative(yn),_,_)--> %'hungry?'-->Are you hungry?
%	{debug(enParser,'subjectless nominal clause',[])},
%	subjlessNominalClause(CL).
%clause(CL,declarative,_,_)--> %'My father'
%	{debug(enParser,'predicateless nominal clause',[])},
%	predlessNominalClause(CL).
%clause(?(IP,CL),interrogative(wh),_,_)-->%'What is your name?'
%	{debug(enParser,'predicateless nominal,?',[])},
%	interrPhrase(IP,_,KIND), predlessNominalClause(CL).
%	%, {KIND\=nom}.
:- endif.


:- op(1050,yfx,<-).


%consequently, I took a bus
commaPreclause(connected(adv(ADV,connecting(comma),base),CL),CL)-->
  adverb(ADV,connecting(comma),_).
%Sam, why did you come here?
commaPreclause(preclause(ADDR,CL),CL)-->
  addressing(ADDR,_NUM).
%Wow, she is also my best friend.
commaPreclause(compound(',',CL1,CL2),CL2)-->
  clause(CL1,interj,interj,interj,_).

%Why did you come here, Sam?
commaPostclause(postclause(CL,ADDR),CL,KIND)-->
  addressing(ADDR,_NUM),
  punct(P), {matchingPunct(KIND,P)}.
%I am a good writer, and she can sing well.
commaPostclause(compound(connect(CONN,coordinating),CL1,CL2),
                CL1,declarative)-->
  connect(CONN,coordinating),
  clause(CL2,declarative,_,_,_), punct('.').
%question tags: 'She is a nice girl, isn't she?'
commaPostclause(questag(',',CL,QT),CL,declarative)-->
  clause(QT,declarative,inverted,short,_),
  punct('?').
%Could you be quiet, please?
commaPostclause(postclause(CL,interj(IJ)),CL,KIND)-->
  interj(please,IJ),
  punct(P), {matchingPunct(KIND,P)}.
%I am fine, thank you.
commaPostclause(postclause(CL,SLESS),CL,_)-->
  clause(SLESS,declarative,_,_,_), punct('.').


sentence(incomplete(IP,KIND))-->interrPhrase(IP,_NUM,_), punct(PUNCT),
    {matchingPunct(KIND,PUNCT)}.
sentence(CL)--> clause(CL,KIND,_,_,SUBJ), punct(PUNCT),
    {matchingPunct(SUBJ,KIND,PUNCT)}.
sentence(EXPR)-->
    commaPreclause(EXPR,CL), punct(','), clause(CL,KIND,_,_,SUBJ),
    punct(P), {matchingPunct(SUBJ,KIND,P)}.
sentence(EXPR)-->
     clause(CL,KIND,_,_,_SUBJ), {KIND \= interj}, punct(','),
     commaPostclause(EXPR,CL,KIND),
     {EXPR=questag(',',CL,QT)->
          CL=clause(_,_,_,S1,VP1,_,P1),
          QT = clause(_,_,_,S2,VP2,_,P2),
          S1=S2, P1\=P2, matchingLongShortVP(VP1,VP2);
     true}.
sentence(EX2PR)-->
    commaPreclause(EX2PR,EXPR), punct(','),
    commaPreclause(EXPR,CL), punct(','),
    clause(CL,KIND,_,_,SUBJ), punct(P), {matchingPunct(SUBJ,KIND,P)}.
%She is my wife; she is also my best friend.
sentence(compound(';',CL1,CL2))-->
  clause(CL1,declarative,_,_,_), punct(';'),
  clause(CL2,declarative,_,_,_), punct('.').
%Look out - she is going to fall.
sentence(compound('-',CL1,CL2))-->
  clause(CL1,imperative(_),_,_,''), punct('-'),
  clause(CL2,KIND,_,_,_),
      {once(KIND=exclamatory;KIND=imperative(strong))}, punct('!').
%no sooner...than...
sentence(correlated(CONN,CL1,CL2))-->
  adverb(C1,_,base), adverb(C2,_,base),
  {correlative(CONN,C1,C2,inverse,C3,straight)},
  invertedVerbalClause(CL1,_NUMPERS,declarative,_TENSE,_SUBJ,_REF,DEM),
  {emptyOptDem(DEM)},
  connect(C3,comp), clause(CL2,declarative,_,_,_), punct('.').
sentence(correlated(CONN,CL1,CL2))-->
  connect(C1,corr),
  {correlative(CONN,C1,inverse,C2,straight)},
  invertedVerbalClause(CL1,_NUMPERS,declarative,_TENSE,_SUBJ,_REF,DEM),
  {emptyOptDem(DEM)},
  punct(','), connect(C2,corr), clause(CL2,declarative,_,_,_), punct('.').
%either...or...
sentence(correlated(CONN,CL1,CL2))-->
  connect(C1,corr),
  {correlative(CONN,C1,straight,C2,straight)}, clause(CL1,declarative,_,_,_),
  connect(C2,corr), clause(CL2,declarative,_,_,_), punct('.').
%first...the...
sentence(correlated(CONN,CL1,CL2))-->
  adverb(C1,_,base),
  {correlative(CONN,C1,straight,C2,straight)}, clause(CL1,declarative,_,_,_),
  connect(C2,corr), clause(CL2,declarative,_,_,_), punct('.').
sentence(complex(connect(CONN,subordinating),MAIN->SUB))-->
  clause(MAIN,KIND,_,_,SUBJ), connect(CONN,subordinating),
  {once(CONN\=if;(MAIN=clause(_,_,[],_,_,_,_),
   SUB=clause(_,_,_,_,VP,_,_), VP=vp(_,_,_,_,_,[])))},
  clause(SUB,declarative,_,_,_SUBJ),
  punct(P), {matchingPunct(SUBJ,KIND,P)}.
sentence(complex(connect(CONN,subordinating),(SUB<-MAIN)))-->
  connect(CONN,subordinating), clause(SUB,declarative,_,_,_),
  clause(MAIN,KIND,_,_,SUBJ),
  {once(CONN\=if;(MAIN=clause(_,_,[],_,_,_,_),
   SUB=clause(_,_,_,_,VP,_,_), VP=vp(_,_,_,_,_,[])))},
  punct(P), {matchingPunct(SUBJ,KIND,P)}.

:- pro.




















