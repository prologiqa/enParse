%
% intermediate level (B2( vocabulary
%

:- include('../enDict'). %including the Basic English dictionary

:- include(enNouns). %including B2 only nouns

:- include(enVerbs). %including B2 only verbs

:- include(enAdjAdvEtc). %including B2 only adj, adv, etc.

