:- module(matchSyn,[matches/3]).

:- use_module('../parser').

:- use_module(prolog(dictUtils/prolog/pl2dict)).
:- use_module(prolog(dictUtils/prolog/dictMan)).

%asymmetric: matches(TREE,PATTERN)/semidet
%matches(ATOM,ATOM,SENTENCE):- atom(ATOM), !.
matches(PATTERN,SENTENCE,TREE):-
    copy_term(TREE,TREEBOUND), numbervars(TREEBOUND,0,_),
    ignore(once(\+ \+ contains_term(PATTERN,TREEBOUND))->writeln(SENTENCE)).


pattern(clause{subject:sg(1)}).

replace(clause{subject:sg(1)},clause{subject:sg(2)}).

%match(SENTENCE,PATTERN)
match(LINE,PATTERN):-
  parseThisSentence(LINE,[TREE|_]),
  pl2dict(=,matchSyn:dict,TREE,DICT), dictMatch(PATTERN, DICT) .

:- if(fail).
test3("I am a doctor.",
     [clause{subj:pronoun{numpers:sg/1}}-->clause{subj:pronoun{numpers:sg/2}},
     clause{vp:vp{verb:verb{numpers:sg/1}}}-->
           clause{vp:vp{verb:verb{numpers:sg/2}}},
     clause{numpers:sg/1}-->clause{numpers:sg/2}]
    ).
:-endif.


test(1,inverse,"I am my doctor.", [sg/1-->sg/2] ).
test(2,inverse,"Why do I want to love you?",
     [sg/1-->sg2,sg/2-->sg/1,sg2-->sg/2] ).
test(3,ynInt,"I am so happy today.",
 [clause{kind:nominal(declarative)}-->clause{kind:nominal(interrogative(yn))}]).
test(4,whyInt,"I am so happy today.",
 [clause{kind:nominal(declarative)}
  ->CL^ ?{interr:interr{wh:why,kind:adv(reason)},clause:CL}]).
test(5,passivize,"I read a book.",
     [vp{tense:T,verb:verb{tense:T,verb:[V]}}
      -->vp{tense:passive(T),
            verb:verb{tense:passive(T),verb:[be,V]}},
     clause{subj:S1{case:nom}}-->clause{subj:S1{case:acc}},
     clause{subj:SUBJ,vp:vp{args:[OBJ]}}
      -->clause{subj:OBJ,vp:vp{args:[prep{prep:by,np:SUBJ}]}},
     clause{subj:S2{case:_}}-->clause{subj:S2{case:nom}},
     clause{numpers:_,subj:S{numpers:NUMPS},vp:vp{verb:verb{numpers:_}}}
      -->clause{subj:S{numpers:NUMPS},
                numpers:NUMPS,
                vp:vp{verb:verb{numpers:NUMPS}}}
     ]).
test(6,doYouThink,"I am so happy today.",
 [clause{kind:nominal(declarative)}
  ->CL^clause{kind:verbal(interrogative(yn)),
              free:[], subj:pronoun(sg/2,_,nom),
              numpers:sg/2,
              vp:vp{verb:[do,think],
                    advs:adverb(really,base,degree),
                    mode:indicative,
                    tense:present,
                    args:[CL]}}]).
test(7,iReallyDoubt,"you are so happy today.",
 [clause{kind:nominal(declarative)}
  ->CL^clause{kind:verbal(declarative),
              free:[], subj:pronoun(sg/1,_,nom),pol: +,
              numpers:sg/1,
              vp:vp{verb:[doubt],
                    advs:adverb(really,base,degree),
                    mode:indicative,
                    tense:present,
                    args:[CL]}}]).
test(8,whyDoYouThink,"do you love me?",
 [ ?{clause:clause{kind:verbal(interrogative(yn))}}
  ->CL^clause{kind:verbal(interrogative(wh)),
              free:[], subj:pronoun(sg/2,_,nom),pol: +,
              numpers:sg/2,
              vp:vp{verb:[think],
                    advs:'',
                    mode:indicative,
                    tense:present,
                    args:[CL]}}]).


transformAll(RULES,S0,S):- is_list(RULES),
  parseThisSentence(S0,[T0|_]),
  pl2dict(matchSyn:dict,T0,D0),
  foldl(transform(),RULES,D0,D),
  dict2pl(matchSyn:dict,D,T),
  toSentence(T,S).

transform(PATTERN-->REPLACE,DICT0,DICT):-
  dictMan:dictReplaceAll(DICT0,PATTERN,REPLACE,DICT).
transform(PRE->DICT0^DICT,DICT0,DICT):-
  dictMan:dictMatch(DICT0,PRE).


%X=sg/1... X=orange)    subterm
%X=
%a (form�lis) alany �N (sg/1)
%olvas�s az �ll�tm�ny, sg/1  (�n olvasok)
%van benne szabad hat�roz�
%els� t�rgyb�l egyn�l t�bb van
%szabad hat�roz�sz�k k�z�tt hat�roz�sz� van
%
t1:- start('r.txt',matches(sg/1)).
t1(X):- start('r.txt',matches(X)).

t2:- start('regression.txt',matches(sg/1)).
t2(X):- start('regression.txt',matches(X)).




test(N):- test(N,_,S,RULES), transformAll(RULES,S,TX), writeln(TX).









