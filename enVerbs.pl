
aux(do).
aux(have).
aux(be).


aux(need,needed,to).
aux(want,wanted,to).
aux(may,might,'').
aux(can,could,'').
aux(shall,should,'').
aux(will,would,'').

modal(must,'',necessity,verb).
modal(can,could,ability,verb).
modal(may,might,possibility,verb).
modal(ought,'',proposal,verbal(to)).

modal(should,proposal,verb).
modal(would,conditional,verb).

%verb(accept,accept,[nom,acc]).
%verb(access,access,[nom,acc]).
%verb(accompany,accompany,[nom,acc]).
verb(achieve,achieve,[nom,verbal(to)]).
verb(act,act,[nom]).
%verb(adapt,adapt,[nom,acc]).
verb(add,add,[nom,acc]).
%verb(adjust,adjust,[nom,acc]).
verb(admire,admire,[nom,acc]).
verb(admit,admit,[nom,clause(that)]).
%verb(advance,advance,[nom,acc]).
verb(advertise,advertise,[nom,acc]).
verb(advise,advise,[nom,verbal(to)]).
verb(afford,afford,([nom,acc];[nom,verbal(to)])).
verb(agree,agree,([nom,clause(that)];
                  [nom,verbal(to)];
                  [nom,?(with(acc))])).
verb(aim,aim,([nom,acc];[nom,verbal(to)])).
%verb(alarm,alarm,[nom,acc]).
%verb(align,align,[nom,acc]).
verb(allow,allow,[nom,acc,verbal(to)]).
%verb(announce,announce,[nom,acc]).
verb(annoy,annoy,[nom,acc]).
verb(answer,answer,([nom,acc,?(acc)];
                    [nom,for(_)];
                    [nom,to(acc)])).
verb(apologise,apologise,[nom,acc]).
verb(appear,appear,[nom,verbal(to)]).
%verb(apply,apply,[nom,acc]).
verb(approach,approach,[nom,acc]).
%verb(appreciate,appreciate,[nom,acc]).
%verb(approve,approve,[nom,acc]).
verb(argue,argue,[nom,(acc)]).
verb(arrange,arrange,[nom,acc]).
%verb(arrest,arrest,[nom,acc]).
verb(arrive,arrive,[nom,?(to(acc))]).
verb(ask,ask,[nom,acc]).
%verb(assemble,assemble,[nom,acc]).
%verb(attach,attach,[nom,acc]).
%verb(attack,attack,[nom,acc]).
%verb(attempt,attempt,[nom,?(verbal(to))]).
verb(attend,attend,[nom,acc]).
verb(attract,attract,[nom,acc]).
%verb(average,average,[nom,acc]).
%verb(avoid,avoid,[nom,acc]).
%verb6(awake,awake,awakes,awoke,awaken,[nom,acc]).
%
% BBBBB
%
verb(babysit,babysit,[nom,?(acc)]).
verb(bake,bake,[nom,acc]).
verb(barbecue,barbecue,[nom]).
%verb(base,base,[nom,acc]). %HU:alapoz, alap�t
%verb(bath,bath,[nom,?(acc)]).
verb(be,be,was,were,been,being,will,[nom,?(acc)];[nom,clause]).
verb(beat,beat,[nom,acc]).
verb6(become,become,becomes,became,become,[nom,acc]).
verb(begin,begin,[nom,acc]).
verb(behave,behave,[nom,?(acc)]).
verb(believe,believe,[nom,in(acc)];[nom,clause]).
verb(belong,belong,[nom,to(acc)]).
%verb(bend,bend,bent,[nom]). %HU:hajol
%verb6(bite,bite,bites,bit,bitten,[nom,acc]). %HU: harap
verb(blame,blame,[nom,acc]).
%verb(bleed,bleed,[nom]).
%verb6(blow,blow,blows,blew,blown,[nom,?(acc)]). %HU: f�j
verb(board,board,[nom,?(acc)]).
verb(boil,boil,[nom,?(acc)]). %HU: f�z/f�
verb(bomb,bomb,[nom,acc]).
verb(bond,bond,[nom,acc]). %HU:k�t, ragaszt
verb(book,book,[nom,acc]).
%verb6(bear,bear,bears,bore,born,[nom,acc]).
verb(borrow,borrow,[nom,acc]).
verb(bother,bother,[nom,acc]).
verb(brake,brake,[nom,?(acc)]).
verb6(break,break,breaks,broke,broken,[nom,acc]).
verbPart_(breakDown,break,down,[nom]).
verbPart_(breakIn,break,in,[nom,?(acc)]).
verbPart_(breakUp,break,up,[nom]).
verb(breathe,breathe,[nom]).
verb(bring,bring,brought,[nom,acc]). %bring+back
verbPart_(bringUp,bring,up,[nom,acc]).
verb(brush,brush,[nom,acc]).
verb(build,build,built,[nom,acc]).
%verb(burn,burn,[nom,acc]).
%verb(burn,burn,burnt,[nom,acc]).
%verb(burst,burst,[nom,acc]).
verb(bury,bury,[nom,acc]).
verb(buy,buy,bought,[nom,acc,acc];[nom,acc,?(to(acc))]).
%
% CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
%
verb(call,call,[nom,?(acc)]).
verbPart_(callFor,call,for,[nom,?(acc)]).
verbPart_(callIn,call,in,[nom,?(acc)]).
verb(camp,camp,[nom]).
verb(calculate,calculate,[nom,acc]).
%verb(cancel,cancel,[nom,acc]).
%verb(care,care,[nom,?(for(acc))]).
verb(carry,carry,[nom,acc]).
verbPart_(carryOn,carry,on,[nom,verbal(ing)]).
verbPart_(carryOut,carry,out,[nom,acc]).
verb(cash,cash,[nom,acc]).
verb(catch,catch,caught,[nom,acc]).
%verb(cause,cause,[nom,acc]). %HU:okoz
verb(celebrate,celebrate,[nom,acc]).
%verb(challenge,challenge,[nom,acc]).
verb(change,change,[nom,?(acc)]).
%verb(charge,charge,[nom,?(acc)]).
%verb(chase,chase,[nom,acc]).
verb(cheat,cheat,[nom,?(acc)]).
verb(check,check,[nom,acc]).
verbPart_(checkIn,check,in,[nom,?(to(acc))]).
verbPart_(checkOut,check,out,[nom,?(of(acc))]).
verb6(choose,choose,chooses,chose,chosen,[nom,acc]).
verb(clap,clap,[nom,?(acc)]).
verb(clean,clean,[nom,acc]).
verb(clear,clear,[nom,acc]).
verb(click,click,[nom,?(acc)]).
verb(climb,climb,[nom,acc]).
verb(close,close,[nom,acc]).
verb(collect,collect,[nom,acc]).
verb(colour,colour,[nom,acc]).
verb(comb,comb,[nom,acc]).
verb6(come,come,comes,came,come,[nom]). %come.back
verbPart_(comeOn,come,on,[nom]).
%verb(command,command,[nom]).
%verb(comment,comment,[nom,?(acc)]).
%verb(communicate,communicate,[nom,with(acc)]).
%verb(compare,compare,([nom,acc,with(acc)];
%                      [nom,acc,to(acc)])).
verb(compete,compete,[nom,?(with(acc))]).
verb(complain,complain,[nom,?(of(acc))]).
verb(complete,complete,[nom,acc]).
%verb(compress,compress,[nom,acc]).
verb(concentrate,concentrate,[nom,?(acc),?(on(acc))]).
verb(confirm,confirm,[nom,acc];[nom,clause(that)]).
%verb(connect,connect,[nom,acc]).
verb(consider,consider,[nom,acc];[nom,verbal(to)];[nom,clause(that)]).
verb(consist,consist,[nom,of(acc)]).
verb(contact,contact,[nom,acc]).
%verb(contain,contain,[nom,acc]).
verb(continue,continue,[nom,acc]).
%verb(control,control,[nom,acc]).
verb(convince,convince,[nom,acc,?(verbal(to))];[nom,acc,clause(that)]).
verb(cook,cook,[nom,acc]).
verb(copy,copy,[nom,acc]).
%verb(correct,correct,[nom,acc]).
verb(cost,cost,[nom,acc]).
verb(cough,cough,[nom]).
%verb(count,count,[nom,acc]).
verb(cover,cover,[nom,acc]).
verb(crash,crash,[nom]).
%verb(crack,crack,[nom,acc]).
verb(create,create,[nom,acc]).
verb(cross,cross,[nom,acc]).
verbPart_(crossOut,cross,out,[nom,acc]).
%verb(crush,crush,[nom,acc]). %HU: �sszenyom, -pr�sel
verb(cry,cry,[nom]).
verb(cure,cure,[nom,acc]).
verb(cut,cut,cut,[nom,acc]).
verbPart_(cutUp,cut,up,[nom,acc]).
verb(cycle,cycle,[nom]).
%
% DDDDDDDDDDDDDDDDDDDDDDDDD
%
%verb(damage,damage,[nom,acc]).
verb(dance,dance,[nom,?(acc)]).
%verb(date,date,[nom,acc]).
verb(deal,deal,[nom,with(acc)]).
verb(decide,decide,[nom,?(verbal(to))]).
verb(declare,declare,[nom,acc,?(acc)];
     [nom,?(verbal(to))];[nom,?(clause(that))]).
verb(decorate,decorate,[nom,?(acc)]).
%verb(decrease,decrease,[nom,acc]).
verb(defeat,defeat,[nom,?(acc)]).
verb(defend,defend,[nom,acc]).
%verb(deflate,deflate,[nom,acc]).
verb(delay,delay,[nom,acc]).
verb(delete,delete,[nom,acc]).
verb(deliver,deliver,[nom,acc]).
verb(demand,demand,[nom,acc];[nom,?(verbal(to))]).
verb(depart,depart,[nom,?(from(acc))]).
verb(depend,depend,[nom,on(acc)]).
verbPart_(dependOn,depend,on,[nom,clause(wh)]).
%verb(desire,desire,[nom,?(verbal(to))]).
verb(describe,describe,[nom,acc]).
verb(deserve,deserve,[nom,acc]).
verb(design,design,[nom,acc]).
verb(destroy,destroy,[nom,acc]).
%verb(develop,develop,[nom,acc]).
verb(dial,dial,[nom,acc]).
verb(die,die,[nom]).
verb(dig,dig,[nom,?(acc)]).
%verb(digest,digest,[nom,acc]).
verb(direct,direct,[nom,acc]).
verb(disagree,disagree,([nom,clause(that)];
                  [nom,verbal(to)];
                  [nom,?(with(acc))])).
%verb(disappear,disappear,[nom]).
verb(disappoint,disappoint,[nom,acc]).
%verb(discard,discard,[nom,acc]).
verb(discover,discover,[nom,acc]).
verb(discuss,discuss,[nom,acc]).
verb(dislike,dislike,[nom,acc]).
%verb(dispatch,dispatch,[nom,acc]).
%verb(display,display,[nom,acc]).
verb(disturb,disturb,[nom,acc]).
verb(dive,dive,[nom]).
%verb(divide,divide,[nom,acc]).
%verb(divorce,divorce,[nom,from(acc)]).
verb6(do,do,does,did,done,[nom,?(acc),?(to(acc))]).
verb(doubt,doubt,([nom,clause];
                  [nom,clause(that)];
                  [nom,clause(if)])).
verb(download,download,[nom,acc]).
verb(drag,drag,[nom,acc]).
%verb(drain,drain,[nom,acc]).
verb6(draw,draw,draws,drew,drawn,[nom,acc]).
verb(dream,dream,[nom,acc]).
verb(dress,dress,[nom,acc]).
verb6(drink,drink,drinks,drank,drunk,[nom,acc]).
verb6(drive,drive,drives,drove,driven,[nom,?(acc)]).
%verb(drop,drop,[nom,acc]).
verb(dry,dry,[nom,acc]).
%
% EEEEEEEEEEEEEEEEEEEEEEEEEEE
%
verb(earn,earn,[nom,acc]).
verb6(eat,eat,eats,ate,eaten,[nom,?(acc)]).
verb(educate,educate,[nom,acc]).
verb(email,email,[nom,acc]).
verb(employ,employ,[nom,acc]).
%verb(effect,effect,[nom,acc]).
%verb(elaborate,elaborate,[nom,acc]).
verb(encourage,encourage,[nom,acc]).
verb(end,end,[nom,?(acc)]).
verbPart_(endUp,end,up,[nom,verbal(ing)]).
verb(enjoy,enjoy,([nom,acc];
                 [nom,verbal(ing)])).
verb(enter,enter,[nom,?(acc)]).
verb(entertain,entertain,[nom,acc]).
%verb(erase,erase,[nom,acc]).
verb(escape,escape,[nom,?(acc),?(from(acc))]).
%verb(estimate,estimate,[nom,acc]).
%verb(exaggerate,exaggerate,[nom,acc]).
%verb(examine,examine,[nom,acc]).
verb(exchange,exchange,[nom,acc]).
%verb(excite,excite,[nom,acc]).
verb(excuse,excuse,[nom,acc]).
verb(exercise,exercise,[nom,acc]).
verb(exist,exist,[nom]).
verb(expect,expect,[nom,?(acc),?(verbal(to))]).
verb(explain,explain,[nom,acc,?(acc)]).
verb(explode,explode,[nom]).
verb(explore,explore,[nom,acc]).
%
% FFFFFFFFFFFFFF
%
verb(fail,fail,[nom]).
%verb(faint,faint,[nom]).
verb6(fall,fall,falls,fell,fallen,[nom]).
verb(fancy,fancy,[nom]).
%verb(farm,farm,[nom,acc]).
verb(fasten,fasten,[nom]).
verb(fax,fax,[nom,acc,?(acc)]).
verb(feed,feed,[nom,acc]).
verb(feel,feel,felt,[nom,?(acc)]).
verb(fetch,fetch,[nom,acc]).
verb(fight,fight,fought,[nom,acc]).
verb(fill,fill,[nom,acc]). %fill.in
verbPart_(fillIn,fill,in,[nom,acc]).
verbPart_(fillUp,fill,up,[nom,acc,?(with(acc))]).
verb(film,film,[nom,acc]).
verb(find,find,found,[nom,acc]). %find.out
verbPart_(findOut,find,out,[nom,clause(wh)]).
verb(finish,finish,([nom,?(acc)];
                    [nom,verbal(ing)])).
verb(fire,fire,[nom,acc]).
verb(fish,fish,[nom]).
verb(fit,fit,[nom,?(acc)]).
%verb(fix,fix,[nom,acc]).
verb(float,float,[nom]).
verb(flood,flood,[nom,acc]).
%verb(flow,flow,[nom,acc]).
verb6(fly,fly,flies,flew,flown,[nom]).
%verb(fold,fold,[nom,acc]).
verb(follow,follow,[nom,acc]).
verb(forget,forget,forgot,[nom,acc]).
verb(forgive,forgive,[nom,acc]).
%verb(forward,forward,[nom,acc]). %HU:tov�bb�t
%verb6(freeze,freeze,freezes,froze,frozen,[nom]).
verb(frighten,frighten,[nom,acc]).
verb(fry,fry,[nom,acc]).
%
% GGGGGGGGGGGGGGGGGGGGGGGGGGGGG
%
%verb(gain,gain,[nom,acc]).
%verb(gather,gather,[nom,acc]).
verb(get,get,got,([nom,?(acc)];[nom,verbal(ing)];
                           [nom,?(acc),verbal(to)])).
verbPart_(getAlong,get,along,[nom,with(acc)]).
verbPart_(getBack,get,back,[nom,?(to(acc))]).
verbPart_(getDown,get,down,[nom,?(acc)]).
verbPart_(getIn,get,in,[nom]).
verbPart_(getOff,get,off,[nom,?(acc)]).
verbPart_(getOn,get,on,[nom,?(with(acc))]).
verbPart_(getRidOf,get,[rid,of],[nom,?(acc)]).
verbPart_(getUp,get,up,[nom]).
verb6(give,give,gives,gave,given,[nom,acc,?(acc)];
                                 [nom,acc,to(acc)]).
verbPart_(giveBack,give,back,[nom,acc,?(to(acc))]).
verbPart_(giveIn,give,in,[nom,acc]).
verbPart_(giveOut,give,out,[nom,acc]).
verbPart_(giveUp,give,up,[nom,verbal(ing)]).
verbPart_(giveWay,give,way,[nom,to(acc)]).
verb(glance,glance,[nom,acc]).
verb6(go,go,goes,went,gone,[nom,?(acc)];[nom,to(acc)];
                            [nom,verbal(to)]).
verbPart_(goFor,go,for,[nom,acc]).
verbPart_(goOff,go,off,[nom]).
verbPart_(goOn,go,on,[nom]).
verbPart_(goOut,go,out,[nom,?(acc)]).
verbPart_(goWith,go,with,[nom,acc]).
verbPart_(goTogether,go,together,[nom]).
verb(grab,grab,[nom,acc]).
%verb(grip,grip,[nom,acc]). %HU:megragad
verb(greet,greet,[nom,acc]).
verb(grill,grill,[nom,acc]).
verb6(grow,grow,grows,grew,grown,[nom,?(acc)]).
verbPart_(growUp,grow,up,[nom]).
verb(guess,guess,[nom,acc]).
verb(guide,guide,[nom,acc]).
%
% HHHHHHHHHHHHHHHHHHHHHHH
%
verb(hand,hand,[nom,acc]).
verbPart_(handIn,hand,in,[nom,acc]).
verbPart_(handOut,hand,out,[nom,acc]).
%verb(hang,hang,hung,[nom,?(acc)]). %HU:f�gg, l�g
verbPart_(hangOut,hang,out,[nom]).
verbPart_(hangUp,hang,up,[nom,?(acc)]).
verb(happen,happen,[nom,?(acc)]).
verb(hate,hate,[nom,acc];[nom,verbal(to)]).
verb6(have,have,has,had,had,[nom,acc];[nom,verbal(to)]). %have.got.to, have.to
verb(hear,hear,heard,[nom,acc]).
verb(help,help,[nom,?(acc),?(verbal(to))]).
verb(hide,hide,[nom,acc]).
verb(hike,hike,[nom]).
verb(hire,hire,[nom,?(acc)]).
verb(hit,hit,hit,[nom,acc]).
verb(hitchhike,hitchhike,[nom]).
verb(hold,hold,held,[nom,acc]).
verbPart_(holdUp,hold,up,[nom,?(acc)]).
verb(hope,hope,([nom,?(verbal(to))];
                [nom,clause];[nom,clause(that)])).
verb(hug,hug,[nom,acc]).
verb(hunt,hunt,[nom,acc]).
verb(hurry,hurry,[nom]).
verb(hurt,hurt,[nom,?(acc)]).
%
% IIIIIIIIIIIIIIIIIIIII
%
%verb(identify,identify,[nom,acc]).
%verb(ignore,ignore,[nom,acc]).
verb(imagine,imagine,[nom,?(acc),?(clause(that))]).
%verb(impact,impact,[nom]).
%verb(imply,imply,[nom,acc]).
%verb(imprison,imprison,[nom,acc]).
verb(improve,improve,[nom,acc]).
verb(include,include,[nom,acc]).
%verb(increase,increase,[nom,?(acc),?(by(acc))]). %HU:n�vekszik, n�vel
%verb(inform,inform,[nom,acc]).
verb(injure,injure,[nom,?(acc)]).
%verb(inquire,inquire,[nom,about(acc)]).
verb(insist,insist,[nom,on(acc)];[nom,clause];[nom,clause(that)]).
%verb(install,install,[nom,acc]).
verb(intend,intend,[nom,verbal(to)]).
verb(interest,interest,[nom,acc]).
%verb(interpret,interpret,[nom,acc]).
verb(interrupt,interrupt,[nom,acc]).
verb(interview,interview,[nom,acc]).
%verb(introduce,introduce,[nom,acc]).
verb(invent,invent,[nom,acc]).
verb(invite,invite,[nom,acc]).
verb(involve,involve,[nom,acc]).
verb(iron,iron,[nom,acc]).
%
% JJJJJJJJJJJJJJJJJJJJJJJJ
%
verb(jog,jog,[nom]).
verb(join,join,[nom,?(acc)]).
verb(joke,joke,[nom]).
%verb(judge,judge,[nom,acc]). %HU:�t�l
verb(jump,jump,[nom]).
%
% KKKKKKKKKKKKKKKKKKKKKKKKKKKKK
%
verb(keep,keep,kept,[nom,?(acc)]).
verbPart_(keepIn,keep,in,[nom,?(acc)]).
verbPart_(keepOn,keep,on,[nom,verbal(ing)]).
verbPart_(keepUp,keep,up,[nom,acc]).
verb(kick,kick,[nom,acc]).
%verb(kill,kill,[nom,acc]).
verb(kiss,kiss,[nom,acc]).
verb(knit,knit,[nom,acc]).
verb(knock,knock,[nom,acc]).
verbPart_(knockDown,knock,down,[nom,acc]).
verb6(know,know,knows,knew,known,([nom,?(acc)];
                [nom,clause];[nom,clause(wh)]
                ;[nom,clause(if)];[nom,clause(that)];
                                 [nom,verbal(wh)])).
%
% LLLLLLLLLLLLLLLLLLL
%
verb(land,land,[nom]).
verb(last,last,[nom]).
verb(laugh,laugh,[nom,?(at(acc))]).
verb(lay,lay,laid,[nom,?(acc)]). %HU:fekszik,fektet
%verb(lead,lead,[nom,acc]).
verb6(learn,learn,learns,learned,learnt,([nom,acc];
                                         [nom,verbal(to)];
                                         [nom,clause(that)];
                                         [nom,verbal(wh)])).
verb(leave,leave,left,[nom,?(acc)]).
verb(lend,lend,lent,[nom,acc,?(acc)];[nom,acc,to(acc)];
     [nom,acc,verbal]).
verb(let,let,let,[nom,acc,?(verbal)]).
verb(lie,lie,[nom,acc]). %HU:hazudik
verb6(lie,lie,lies,lay,lain,[nom,?(acc)]). %HU:fekszik
verbPart_(lieDown,lie,down,[nom]). %HU:lefekszik
%verb(lift,lift,[nom,acc]).
verb(light,light,[nom,acc]).
verb(like,like,([nom,acc];
                [nom,verbal(ing)];
                [nom,verbal(to)];
                [nom,clause(wh)])).
verb(listen,listen,[nom,to(acc)]).
%verb(light,light,[nom,?(acc)]).
verb(listen,listen,[nom,?(to(acc))]).
verb(live,live,[nom]).
%verb(load,load,[nom,acc]).
%verb(locate,locate,[nom,acc]).
%verb(lock,lock,[nom,acc]).
verb(look,look,[nom,?(acc)]).
verbPart_(lookAfter,look,after,[nom,acc]). %HU:gondoskodik
verbPart_(lookAt,look,at,[nom,acc]). %HU:r�tekint
verbPart_(lookFor,look,for,[nom,acc]). %HU:keres
verbPart_(lookForward,look,forward,[nom,to(acc)]). %HU:keres
verbPart_(lookLike,look,like,[nom,acc]). %HU:keres
verbPart_(lookOut,look,out,[nom,of(acc)]). %HU: keres, kin�z, kir�
verbPart_(lookUp,look,up,[nom,acc]). %HU: keres, kin�z, kir�
verb(lose,lose,lost,[nom,acc]).
verb(love,love,([nom,acc];[nom,verbal(ing)])).
%
% MMMMMMMMMMMMMMMMMMMMMM
%
%verb(mail,mail,[nom,acc]).
verb(make,make,made,[nom,acc,?(clause)] %?????make sure
    ;[nom,acc,of(acc)] ;[nom,acc,from(acc)]).
verbPart_(makeSure,make,sure,[nom,clause(that)]). %HU:biztos�t
verb(manage,manage,[nom,acc];[nom,verbal(to)]).
%verb(map,map,[nom,?(acc)]).
%verb(marry,marry,[nom,?(acc)]).
verb(match,match,[nom,acc]).
verb(matter,matter,[nom,?(acc)]). %it doesn't matter
verb(mean,mean,meant,[nom,acc]).
%verb(measure,measure,[nom,acc]).
verb(meet,meet,met,[nom,?(acc)]).
verb(mend,mend,[nom,?(acc)]). %HU:jav�t
verb(mention,mention,[nom,acc]).
%verb(melt,melt,[nom]).
verb(mind,mind,([nom,?(acc)];[nom,verbal(ing)];
                [nom,clause(if)];
                [nom,clause(wh)])).
verb(miss,miss,[nom,?(acc)]).
verb(mix,mix,[nom,acc]).
%verb(mount,mount,[nom]).
verb(move,move,[nom,?(acc),?(to(acc))]). %HU: mozog, mozgat
verb(multiply,multiply,[nom,?(acc),?(by(acc))]).
%
% NNNNNNNNNNNNNNNNNNNNNN
%
verb(name,name,[nom,acc]).
verb(need,need,([nom,acc];
               [nom,verbal(to)])).
%verb(nod,nod,[nom]).
verb(note,note,[nom,?(acc)]).
%verb(notice,notice,[nom,acc]).
%
% OOOOOOOOOOOOOOOOOOOOOOOOO
%
%verb(obey,obey,[nom,acc]).
%verb(occur,occur,[nom,acc]).
verb(offer,offer,[nom,?(acc),?(verbal(to))]).
verb(open,open,[nom,acc]).
%verb(operate,operate,[nom,acc]).
verb(order,order,[nom,acc]).
verb(organise,organise,[nom,acc]).
%verb(oversee,oversee,[nom,acc]).
verb(owe,owe,[nom,acc]).
%verb(own,own,[nom,acc]).
%
% PPPPPPPPPPPPPPPPPP
%
verb(pack,pack,[nom,acc]).
verb(paint,paint,[nom,?(acc)]).
verb(park,park,[nom,?(acc)]).
verb(pass,pass,[nom,?(acc)]).
verb(pause,pause,[nom]).
verb(pay,pay,[nom,acc]).
verb(peel,peel,[nom,acc]).
verb(perform,perform,[nom]).
%verb(permit,permit,[nom,acc]).
verb(persuade,persude,[nom,acc,?(verbal(to))];
     [nom,acc,?(clause(that))]).
verb(phone,phone,[nom,acc]).
verb(pick,pick,[nom,acc]). %pick.up
verbPart_(pickUp,pick,up,[nom,acc]).
%verb(pin,pin,[nom,acc]). %HU: t�z, szegez
verb(plan,plan,[nom,verbal(to)]).
%verb(plant,plant,[nom,acc]).
verb(play,play,[nom,?(acc)]).
verb(please,please,[nom,acc]).
verb(point,point,[nom,at(acc)]).
verb(post,post,[nom,acc]).
verb(postpone,postpone,[nom,acc]).
verb(pour,pour,[nom,acc]).
%verb(polish,polish,[nom,acc]).
verb(practise,practise,[nom,?(verbal(to))]).
verb(pray,pray,[nom]).
verb(precede,precede,[nom,acc]).
verb(predict,predict,[nom,acc];[nom,verbal(to)]
     ;[nom,clause(that)]).
verb(prefer,prefer,[nom,acc,?(to(acc))];[nom,verbal(to)]).
verb(prepare,prepare,[nom,acc]).
%verb(present,present,[nom,acc,acc]).
%verb(preserve,preserve,[nom,acc]).
verb(press,press,[nom,acc]).
%verb(pretend,pretend,[nom,acc]).
%verb(prevent,prevent,[nom,acc]).
verb(print,print,[nom,acc]).
%verb(produce,produce,[nom,acc]).
verb(promise,promise,[nom,acc];[nom,verbal(to)]
     ;[nom,clause(that)]).
verb(promote,promote,[nom,acc]).
verb(pronounce,pronounce,[nom,acc]).
verb(protect,protect,[nom,acc]).
verb(prove,prove,[nom,acc]).
verb(provide,provide,[nom,acc]).
%verb(protest,protest,[nom,against(acc)]).
%verb(prove,prove,[nom,acc]).
%verb(provide,provide,[nom,acc]).
verb(publish,publish,[nom,acc]).
verb(pull,pull,[nom,acc]).
%verb(pump,pump,[nom,acc]).
%verb(punish,punish,[nom,acc]).
verb(push,push,[nom,acc]).
verb(put,put,put,[nom,acc,?(to(acc))]). %put-on
verbPart_(putAway,put,away,[nom,acc]).
verbPart_(putDown,put,down,[nom,acc]).
verbPart_(putOff,put,off,[nom,acc]).
verbPart_(putOn,put,on,[nom,acc]).
verbPart_(putOut,put,out,[nom,acc]).
verbPart_(putThrough,put,out,[nom,acc]).
verbPart_(putUp,put,up,[nom,acc]).
%
% QQQQQQQQQQQQQQQ
%
%verb(queue,queue,[nom]).
verb(quit,quit,[nom]).
%
% RRRRRRRRRRRRRRRRRR
%
verb(race,race,[nom,?(acc)]).
verb(rain,rain,[nom]).
verb(raise,raise,[nom,acc]).
verb(reach,reach,[nom,acc]).
verb(read,read,read,[nom,?(acc)]).
verb(realise,realise,[nom,?(acc)];[nom,verbal]).
verb(rebuild,rebuild,[nom,acc]).
verb(receive,receive,[nom,acc]).
verb(recognise,recognise,[nom,acc]).
%verb(recommend,recommend,[nom,acc]).
verb(record,record,[nom,acc]).
verb(recover,recover,[nom]).
verb(recycle,recycle,[nom,acc]).
verb(reduce,reduce,[nom,acc]).
%verb(refer,refer,[nom,to(acc)]).
%verb(refuel,refuel,[nom,acc]).
%verb(refuse,refuse,[nom,acc]).
verb(register,register,[nom,acc]).
verb(regret,regret,[nom,acc];[nom,clause(that)]).
%verb(reject,reject,[nom,acc]).
%verb(regret,regret,[nom,acc]).
%verb(relate,relate,[nom,to(nom)]).
%verb(release,release,[nom,acc]).
%verb(relax,relax,[nom]).
verb(remain,remain,[nom]).
verb(remember,remember,[nom,acc];[nom,clause(that)]).
verb(remind,remind,[nom,acc];[nom,verbal(to)];
     [nom,clause(that)]).
%verb(remove,remove,[nom,acc]).
verb(rent,rent,[nom,acc]).
verb(repair,repair,[nom,acc]).
verb(repeat,repeat,[nom,acc]).
%verb(replace,replace,[nom,acc]).
verb(reply,reply,[nom,acc]).
verb(report,report,[nom,acc]).
verb(request,request,[nom,acc]).
%verb(require,require,[nom,acc]).
%verb(rescue,rescue,[nom,acc]).
%verb(research,research,[nom,acc]).
%verb(reserve,reserve,[nom,acc]).
verb(respect,respect,[nom,acc];[nom,clause(that)]).
verb(rest,rest,[nom]).
verb(retire,retire,[nom]).
verb(retract,retract,[nom,acc]).
verb(return,return,[nom,?(from(acc))];[nom,acc]).
verb(revise,revise,[nom,acc]).
%verb(reward,reward,[nom,acc]).
verb6(ride,ride,rides,rode,ridden,[nom,acc]).
verb6(ring,ring,rings,rang,rung,[nom]).
verbPart_(ringBack,ring,back,[nom,acc]).
verbPart_(ringUp,ring,up,[nom,acc]).
verb(rise,rise,[nom]).
%verb(risk,risk,[nom,acc]).
verb(roast,roast,[nom,acc]).
verb(rob,rob,[nom,acc]).
%verb(roll,roll,[nom,acc]). %�sszeg�ngy�l�t
%verb(rub,rub,[nom,acc]).
verb(rule,rule,[nom,acc]).
verb(run,run,ran,[nom,?(acc)]).
verbPart_(runOut,run,out,[nom,of(acc)]).
%
% SSSSSSSSSSSSSSSSSSSSS
%
verb(sail,sail,[nom,?(acc)]).
verb(save,save,[nom,acc]).
verb(say,say,[nom,?(acc),?(to(acc))]).
%verb(scare,scare,[nom,acc]). %HU:f�leml�t
%verb(schedule,schedule,[nom,acc]).
verb(score,score,[nom,acc]).
verb(scream,scream,[nom]).
%verb(seal,seal,[nom,?(acc)]).
%verb(search,search,[nom,acc]).
verb6(see,see,sees,saw,seen,[nom,acc];
                [nom,clause(if)];
                [nom,clause(that)];
                [nom,clause(wh)]).
verb(seem,seem,[nom,?(verbal(to))]).
verb(select,select,[nom,acc]).
verb(sell,sell,sold,[nom,?(acc),?(to(acc))]).
verb(send,send,sent,[nom,?(acc),?(to(acc))]).
verb(serve,serve,[nom,?(acc)]).
%verb(set,set,set,[nom,?(acc),?(to(acc))]).
verbPart_(setOff,set,off,[nom]).
verbPart_(setOut,set,out,[nom]).
verbPart_(setUp,set,up,[nom]).
%verb(separate,separate,[nom,acc]).
verb6(sew,sew,sews,sewed,sewn,[nom,?(acc),?(to(acc))]).
%verb6(shake,shake,shakes,shook,shaken,[nom,acc]).
verb(share,share,[nom,acc]).
verb(shave,shave,[nom,acc]).
verb(shine,shine,[nom]).
%verb(ship,ship,[nom,acc]).
verb(shoot,shoot,[nom,?(at(acc))]).
verb(shop,shop,[nom,acc]).
verb(shout,shout,[nom,?(at(acc))];[nom,to(acc)]).
verb(show,show,shown,[nom,acc,?(acc)];[nom,acc,to(acc)];
     [nom,clause(that)]).
%verb(shower,shower,[nom,?(acc)]).
verb(shut,shut,shut,[nom,acc]).
%verb(sign,sign,[nom,acc]).
verb(signal,signal,[nom,acc]).
verb6(sing,sing,sings,sang,sung,[nom,?(acc)]).
verb(sink,sink,sinks,sank,sunk,[nom,acc]).
verb6(sit,sit,sits,sat,sat,[nom]).
verbPart_(sitDown,sit,down,[nom]).
verb(skate,skate,[nom]).
verb(ski,ski,[nom]).
verb(sleep,slept,slept,[nom,?(acc)]).
%verb(slip,slip,[nom]).
%verb(smash,smash,[nom]).
%verb(smell,smell,[nom,acc]).
%verb(smile,smile,[nom,?(at(acc))]).
verb(smoke,smoke,[nom,?(acc)]).
%verb(sneeze,sneeze,snoze,[nom]). %HU:t�sszent
verb(sneeze,sneeze,[nom]). %HU:t�sszent
verb(snow,snow,[nom]).
verb(snowboard,snowboard,[nom]).
verb(sound,sound,[nom,?(acc)]).
verb(spare,spare,[nom,?(acc)]).
verb(speak,speak,[nom,?(to(acc))]).
verb(spell,spell,[nom,acc]).
verb(spend,spend,spent,[nom,acc]).
verb6(spill,spills,spill,spillt,spilled,[nom,?(acc)]). %HU:ki�nt/�mlik
verb(split,split,[nom,acc]).
verbPart_(splitUp,split,up,[nom,?(with(acc))]). %HU:szak�t p�rj�val
verb(spoil,spoil,[nom,acc]). %HU:elk�nyeztet
%verb(spot,spot,[nom,acc]).
%verb(sprain,sprain,[nom,acc]).
verb(stand,stand,stood,[nom]).
verb(star,star,[nom,acc]).
%verb(stare,stare,[nom,acc]).
verb(start,start,[nom,?(acc)]).
%verb(station,station,[nom,?(acc)]).
verb(stay,stay,[nom,?(verbal(ing))]).
verbPart_(stayBehind,stay,behind,[nom]). %HU:h�tra/ottmarad
verb6(steal,steal,steals,stole,stolen,[nom,acc]).
%verb(step,step,[nom,to(acc)]).
verb(stick,stick,[nom,acc]).
verb(stir,stir,[nom,acc]).
%verb(stitch,stitch,[nom,acc,to(acc)]). %r�varr, r�t�z
verb(stop,stop,stopped,([nom,acc];[nom,verbal(to)];[nom,verbal(ing)])).
%verb(stretch,stretch,[nom,acc]). %HU:fesz�t, ny�jt
verb(strike,strike,struck,[nom,acc]). %HU:csap
%verb(struggle,struggle,[nom,?(verbal(to))]).
verb(study,study,[nom,acc]).
%verb(subordinate,subordinate,[nom,acc]).
%verb(subtract,subtract,[nom,acc,?(from(acc))]).
verb(succeed,succeed,[nom,acc,?(to(acc))]). %+ing
verb(suffer,suffer,[nom,?(from(acc))]).
verb(suggest,suggest,[nom,acc,?(to(acc))];[nom,verbal(to)];
     [nom,verbal(ing)]).
verb(sunbathe,sunbathe,[nom]).
%verb(supply,supply,[nom,acc,?(with(acc))]).
%verb(support,support,[nom,acc]).
verb(suppose,suppose,[nom,?(acc),?(verbal(to))];[nom,clause]).
verb(surf,surf,[nom,acc]).
%verb(surprise,surprise,[nom,acc]).
verb(surround,surround,[nom,acc]).
verb(swim,swim,swam,[nom,?(acc)]).
verb(switch,switch,[nom,?(acc)]).
%
% TTTTTTTTTTTTTTTTTTTTTTTT
%
%verb(tag,tag,[nom,acc]).
verb6(take,take,takes,took,taken,([nom,acc,?(acc)]; %take.off %take.part
                                 [nom,from(acc)];
                                 [nom,up(acc)])).
verbPart_(takeAway,take,away,[nom,acc]).
verbPart_(takeCare,take,care,[nom,of(acc)]).
verbPart_(takeOff,take,off,[nom,?(acc)]).
verbPart_(takePart,take,part,[nom,?(acc)]).
verbPart_(takePlace,take,place,[nom]).
verbPart_(takeUp,take,up,[nom,acc]).
verb(talk,talk,([nom,?(acc)];
               [nom,about(acc)])).
%verb(taste,taste,[nom,acc]).
verb(teach,teach,taught,[nom,acc,?(verbal(to))]).
verb(tear,tear,[nom,acc]).
verb(telephone,telephone,[nom,acc]).
verb(tell,tell,[nom,acc,?(acc)]).
%verb(tend,tend,[nom,verbal(to)]).
verb(test,test,[nom,acc]).
verb(text,text,[nom,acc,?(acc)]).
verb(thank,thank,[nom,acc,?(acc)]).
verb(think,think,thought,[nom,acc];[nom,clause(that)];
                             [nom,clause]).
%verb(threaten,threaten,[nom,acc]).
verb6(throw,throw,throws,threw,thrown,[nom,acc]).
verbPart_(throwAway,throw,away,[nom,acc]).
verb(tick,tick,[nom,?(acc)]).
verb(tidy,tidy,[nom,acc]).
verbPart_(tidyUp,tidy,up,[nom,?(acc)]).
verb(tie,tie,[nom,acc]).
%verb(tighten,tighten,[nom,acc]).
verb(time,time,[nom,acc]).
%verb(touch,touch,[nom,acc]).
verb(tour,tour,[nom]).
%verb(tow,tow,[nom,acc]).
%verb(train,train,[nom,acc]).
verb(transfer,transfer,[nom,acc]).
verb(translate,translate,[nom,acc]).
%verb(transmit,transmit,[nom,acc]).
verb(travel,travel,[nom,?(acc)]).
verb(trust,trust,[nom,in(acc)]).
verb(try,try,([nom,?(acc)]; %try.on
              [nom,verbal(to)])).
verbPart_(tryOn,try,on,[nom,acc]). %HU: felpr�b�l
%verb(tune,tune,[nom,acc]).
verb(turn,turn,[nom,?(to(acc))]). %turn.off, turn.on
verbPart_(turnDown,turn,down,[nom,acc]). %HU: lecsavar/kapcsol
verbPart_(turnInto,turn,into,[nom,acc]). %HU: �talakul
verbPart_(turnOff,turn,off,[nom,acc]). %HU: �talakul
verbPart_(turnOn,turn,on,[nom,acc]). %HU: bekapcsol
verbPart_(turnUp,turn,up,[nom,acc]). %HU: felkapcsol
%verb(twist,twist,[nom]).
%verb(type,type,[nom,acc]).
%
% UUUUUUUUUUUUUUUUUUUU
%
verb(underline,underline,[nom,acc]).
verb(understand,understand,understood,([nom,acc];
                                      [nom,clause(wh)];
                                      [nom,clause(if)];
                                      [nom,verbal(to)])).
verb(undress,undress,[nom]).
verb(unpack,unpack,[nom,?(acc)]).
verb(update,update,[nom,acc]).
verb(upload,upload,[nom,acc,to(acc)]).
verb(use,use,[nom,?(acc)];[nom,verbal(to)]).
%
% VVVVVVVVVVVVVVVVVVVVVV
%
verb(video,video,[nom,acc]).
verb(visit,visit,[nom,acc]).
verb(vote,vote,[nom,?(for(acc))]).
%
% WWWWWWWWWWWWWWWWWWWWWW
%
verb(wait,wait,[nom,?(for(acc))]).
verb6(wake,wake,wakes,waked,woken,[nom]).
verbPart_(wakeUp,wake,up,[nom,?(acc)]).
verb(walk,walk,[nom]).
verb(want,want,([nom,acc];[nom,verbal(to)])).
verb(warn,warn,[nom,?(acc)]).
verb(wash,wash,[nom,acc]).
verbPart_(washUp,wash,up,[nom,?(acc)]).
verb(waste,waste,[nom,acc]).
verb(watch,watch,[nom,acc];[nom,clause]).
verb(water,water,[nom,acc]).
verb(wave,wave,[nom]).
verb6(wear,wear,wears,wore,worn,[nom,acc]).
verbPart_(wearOut,wear,out,[nom,?(acc)]).
%verb(weigh,weigh,[nom,acc]).
%verb(welcome,welcome,[nom,acc]).
%verb(weld,weld,[nom,acc]).
%verb(whip,whip,[nom,acc]).
verb(win,win,won,[nom,acc]).
verb(wish,wish,([nom,acc,?(acc)];
               [nom,clause])).
%verb(whistle,whistle,[nom,?(acc)]).
verb(wonder,wonder,[nom,?(clause(if))]).
verb(work,work,[nom]).
verbPart_(workOut,work,out,[nom]).
verb(worry,worry,[nom,?(about(acc))]).
verb(wrap,wrap,[nom,?(acc)]). %HU:becsomagol/teker
verbPart_(wrapUp,wrap,up,[nom,acc]).
verb6(write,write,writes,wrote,written,[nom,acc]).
verbPart_(writeDown,write,down,[nom,acc]).



verbConj(be,present,pl/_,are).
verbConj(be,present,sg/1,am).
verbConj(be,present,sg/2,are).
verbConj(be,present,sg/3,is).
verbConj(be,past,pl/_,were).
verbConj(be,past,sg/1,was).
verbConj(be,past,sg/2,were).
verbConj(be,past,sg/3,was).
verbConj(be,future,_NUM/1,shall).
verbConj(be,future,_NUM/PERS,will):-
  PERS=2;PERS=3.
verbConj(be,futurePast,_,would).












