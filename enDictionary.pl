persPron(pl(1),_,we,us,our,ours).
persPron(pl(2),_,you,you,your,yours).
persPron(pl(3),_,they,them,their,theirs).
persPron(sg(1),_,'I',me,my,mine).
persPron(sg(2),_,you,you,your,yours).
persPron(sg(3),femaleSex,she,her,her,hers).
persPron(sg(3),maleSex,he,him,his,his).
persPron(sg(3),neutral,it,it,its,its).

reflPron(pl(1),_,ourselves).
reflPron(pl(2),_,yourselves).
reflPron(pl(3),_,themelves).
reflPron(sg(1),_,myself).
reflPron(sg(2),_,yourself).
reflPron(sg(3),femaleSex,herself).
reflPron(sg(3),maleSex,himself).
reflPron(sg(3),neutral,itself).

prep(about,about).
prep(across,across).
prep(after,after).
prep(against,against).
prep(ago,ago).
prep(among,among).
prep(around,around).
prep(at,at).
prep(behind,behind).
prep(between,between).
prep(before,before).
prep(by,by).
prep(during,during).
prep(for,for).
prep(from,from).
prep(in,in).
prep(into,into).
prep(like,like).
prep(near,near).
prep(of,of).
prep(off,off).
prep(over,over).
prep(on,on).
prep(since,since).
prep(through,through).
prep(till,till).
prep(to,to).
prep(under,under).
prep(until,until).
prep(with,with).
prep(without,without).

art(def,the,_).
art(indef,a,sg).
art(indef,an,sg).

%independent/coordinating conjunctions
conj(and,and,coordinating). %the combination of two similar thoughts
conj(but,but,coordinating). %contrasting between two thoughts
conj(yet,yet,coordinating). %contrasting between two thoughts
conj(or,or,coordinating).   %indicating alternative thoughts
conj(nor,nor,coordinating). %when neither thought is true


% both...and...
% either...or... sentence either you will eat your dinner of you will go
% to bed
% hardly...when... He had hardly begun to work when he was interrupted
% just as...so...
% neither...nor... INVERTED
% no sooner...INVERTED than...
% No sooner had I received the corner than the bus came
% Scarcely... INVERTED... than...
%
% not only...INVERTED... but also...
% Not only do I love this band, but I have also seen the concert twice.
% rather...or... whether...or...

%if...then...  wo 'then' is applicable too...
%If that is true, then what happened is not surprising.


%correlative conjunctions
correlative(noSoonerThan,no,sooner,inverse,than,straight).

correlative(eitherOr,either,straight,or,straight).
correlative(neitherNor,neither,straight,nor,inverse).
correlative(ifThen,if,straight,then,straight).
correlative(scarcelyWhen,scarcely,inverse,when,straight).

correlative(eitherOr,either,or,nounPhrase).
correlative(neitherNor,neither,nor,nounPhrase).
correlative(bothAnd,both,and,nounPhrase).

%correlative(ifThen,if,straight,then,straight).
%correlative(scarcelyWhen,scarcely,inverse,when,straight).

conj(bothAnd,both,and,coordinating,phrase).




%subordinating conjunction
conj(so,so,subordinating).   %the second follows from the first
conj(because,because,subordinating). %the first follows from the second
conj(for,for,subordinating). %the first follows from the second
conj(if,if,subordinating).
conj(when,when,subordinating).
conj(while,while,subordinating).
conj(although,although,subordinating).
conj(as,as,subordinating).



%comparison conjunction
conj(than,than,comp).
conj(as,as,comp).


conj(or,or,corr).   %indicating alternative thoughts
conj(nor,nor,corr). %when neither thought is true
conj(either,either,corr).
conj(neither,neither,corr).
conj(if,if,corr).   %indicating alternative thoughts
conj(then,then,corr).   %indicating alternative thoughts
conj(scarcely,scarcely,corr).
conj(when,when,corr).
conj(both,both,corr).
conj(and,and,corr).


%conj(please,please).


%det(all,all,pl,_).
%det(any,any,sg,_).
%det(anyOf,[any,of],sg,_).
%det(every,every,pl,_).
%det(some,some,pl,_).
%demonstratives
det(this,this,sg,numNP).
det(that,that,sg,numNP).
det(these,these,pl,numNP).
det(those,those,pl,numNP).
det(enough,enough,sg,numNP).
det(certain,certain,pl,numNP).
det(other,other,pl,numNP).
det(another,another,sg,numNP).


det(allOf,pl,[all,of],_,possNP).
det(fewOf,pl,[few,of],pl,possNP).
det(alotOf,pl,[a,lot,of],_,possNP).
det(lotsOf,pl,[lots,of],pl,possNP).
det(neitherOf,pl,[neither,of],pl,possNP).
det(noneOf,pl,[none,of],pl,possNP).
det(eitherOf,sg,[either,of],pl,possNP).
det(anyOf,sg,[any,of],pl,possNP).
det(coupleOf,pl,[a,couple,of],pl,possNP).
det(tonsOf,pl,[tons,of],pl,possNP).
det(plentyOf,pl,[plenty,of],sg,possNP).
det(bitOf,sg,[a,bit,of],sg,possNP).
det(numberOf,pl,[a,number,of],pl,possNP).
det(pairOf,pl,[a,pair,of],pl,possNP).
det(greatDealOf,sg,[a,great,deal,of],sg,possNP).
det(suchA,sg,[such,a],sg,possNP).
det(whatA,sg,[what,a],sg,possNP).
det(quiteA,sg,[quite,a],sg,possNP).
det(ratherA,sg,[rather,a],sg,possNP).


indef(all,all,pl,count,adjNP).
indef(each,each,_,count,adjNP).
indef(some,some,_,count,adjNP).
indef(many,many,pl,count,adjNP).
indef(much,much,sg,uncount,adjNP).
indef(more,more,sg,_,adjNP).
indef(most,most,pl,_,adjNP).
indef(any,any,_,_,adjNP).
indef(few,few,_,_,adjNP).
indef(fewer,fewer,_,_,adjNP).
indef(fewest,fewest,sg,_,adjNP).
indef(little,little,sg,uncount,adjNP).
indef(less,less,_,_,adjNP).
indef(least,least,sg,_,adjNP).
indef(every,every,sg,count,adjNP).
indef(several,several,pl,count,adjNP).
indef(no,no,_,_,adjNP).

indef(both,both,pl,count,possNP).
indef(half,half,sg,count,possNP).


indefPronoun(someone,someone,sg).
indefPronoun(somebody,somebody,sg).
indefPronoun(something,something,sg).
indefPronoun(anyone,anyone,sg).
indefPronoun(anybody,anybody,sg).
indefPronoun(anything,anything,sg).
indefPronoun(everyone,everyone,pl).
indefPronoun(everybody,everybody,pl).
indefPronoun(everything,everything,pl).
indefPronoun(nobody,nobody,sg).
indefPronoun(nothing,nothing,sg).
indefPronoun(many,many,pl).
indefPronoun(some,some,pl).
indefPronoun(each,each,pl).
indefPronoun(any,any,pl).
indefPronoun(one,one,sg).
indefPronoun(this,this,sg).
indefPronoun(that,that,sg).


interj(hello,hello).
interj(no,no).
interj(ok,ok).
interj(whoa,whoa).
interj(wow,wow).
interj(yes,yes).

interr(how,how,adv(manner)).
interr(what,what,nom).
interr(when,when,adv(time)).
interr(where,where,adv(place)).
interr(which,which,adj). %which shoes
interr(whom,whom,acc).
interr(who,who,nom).
interr(why,why,adv(reason)).

interr(howmany,[how,many],num).
interr(howmuch,[how,much],num).


ord(first,1).
ord(second,2).
ord(third,3).
ord(fourth,4).
ord(fifth,5).

card(one,1).
card(two,2).
card(three,3).
card(four,4).
card(five,5).
card(six,6).
card(seven,7).
card(eight,8).
card(nine,9).
card(ten,10).
card(eleven,11).
card(twelve,12).
card(twenty,20).
card(thirty,30).
card(fourty,40).
card(fifty,50).
card(sixty,60).
card(seventy,70).
card(eighty,80).
card(ninety,90).
card(houndred,100).
card(thousand,1000).
card(million,1000000).
card(billion,1000000000).

unitAdj(meter,long).
unitAdj(meter,high).
unitAdj(hour,long).
unitAdj(year,old).
unitAdj(year,long).


adj(able,able).
adj(academic,academic).
adj(alone,alone).
adj(amazing,amazing).
adj(american,american).
adj(angry,angry).
adj(arealocal,local).
adj(asia,asia).
adj(automatic,automatic).
adj(average,average).
adj(awake,awake).
adj(awesome,awesome).
adj(awful,awful).
adj(bad,bad).
adj(basic,basic).
adj(beautiful,beautiful).
adj(big,big).
adj(bitter,bitter).
adj(black,black).
adj(blue,blue).
adj(bored,bored).
adj(bright,bright).
adj(broken,broken).
adj(brown,brown).
adj(busy,busy).
adj(careful,careful).
adj(certain,certain).
adj(cheap,cheap).
adj(chemical,chemical).
adj(clear,clear).
adj(closed,closed).
adj(clueless,clueless).
adj(cold,cold).
adj(colorful,colorful).
adj(comfortable,comfortable).
adj(common,common).
adj(comparable,comparable).
adj(complete,complete).
adj(complex,complex).
adj(conscious,conscious).
adj(cool,cool).
adj(correct,correct).
%adj(cracked,cracked).  ----it can be derived from verb/crack
adj(cruel,cruel).
adj(curious,curious).
adj(current,current).
adj(dangerous,dangerous).
adj(dark,dark).
adj(dead,dead).
adj(deaf,deaf).
adj(dear,dear).
adj(deep,deep).
adj(delicate,delicate).
adj(delicious,delicious).
adj(dependent,dependent).
adj(different,different).
adj(difficult,difficult).
adj(dirty,dirty).
adj(divorced,divorced).
adj(dizzy,dizzy).
adj(dry,dry).
adj(early,early).
adj(easy,easy).
adj(elastic,elastic).
adj(electric,electric).
adj(electronic,electronic).
%adj(enough,enough).
adj(empty,empty).
adj(english,'English').
adj(equal,equal).
adj(european,'European').
adj(excellent,excellent).
adj(expensive,expensive).
adj(experienced,experienced).
adj(expert,expert).
adj(fair,fair).
adj(false,false).
adj(familiar,familiar).
adj(fancy,fancy).
adj(far,far).
adj(fast,fast).
adj(fat,fat).
adj(favorite,favorite).
adj(feeble,feeble). %HU: gyenge
adj(fertile,fertile).
adj(fine,fine).
adj(finish,finish).
adj(fixed,fixed).
adj(flat,flat).
adj(fond,[fond,of]).
adj(free,free).
adj(frequent,frequent).
adj(front,front).
adj(frozen,frozen).
adj(full,full).
adj(fullfood,sated).
adj(funny,funny).
adj(general,general).
adj(gold,gold).
adj(goneabsent,absent).
adj(good,good).
adj(great,great).
adj(green,green).
adj(grey,grey).
adj(gross,gross).
adj(happy,happy).
adj(hard,hard).
adj(healthy,healthy).
adj(hearing,hearing).
adj(heavy,heavy).
adj(high,high).
adj(hollow,hollow). %HU:�reges
adj(honest,honest).
adj(hot,hot).
adj(hungry,hungry).
adj(hurt,hurt).
adj(ill,ill).
adj(important,important).
adj(impossible,impossible).
adj(included,included).
adj(known,known).
adj(last,last).
adj(late,late).
adj(lazy,lazy).
adj(leather,leather).
adj(left,left).
adj(light,light).
adj(liquid,liquid).
adj(little,little).
adj(local,local).
adj(lonely,lonely).
adj(long,long).
adj(lose,lose).
adj(loose,loose).
adj(loud,loud).
adj(lousy,lousy).
adj(low,low).
adj(married,married).
adj(material,material).
adj(medical,medical).
adj(metal,metal).
adj(middle,middle).
adj(mixed,mixed).
adj(narrow,narrow).
adj(natural,natural).
adj(net,net).
adj(near,near).
adj(nearby,nearby).
adj(necessary,necessary).
adj(negative,negative).
adj(new,new).
adj(next,next).
adj(nice,nice).
adj(normal,normal).
adj(odd,odd).
adj(offline,offline).
adj(old,old).
adj(online,online).
adj(open,open).
adj(opposite,opposite).
adj(orange,orange).
adj(orangecolor,orange).
adj(other,other).
adj(over,over).
adj(overall,overall).
adj(parallel,parallel).
adj(perfect,perfect).
adj(personal,personal).
adj(physical,physical).
adj(pink,pink).
adj(plantDry,dry).
adj(political,political).
adj(possible,possible).
adj(poor,poor).
adj(positive,positive).
adj(pregnant,pregnant).
adj(preserved,preserved).
adj(pretty,pretty).
adj(private,private).
adj(probable,probable).
adj(professional,professional).
adj(public,public).
adj(purple,purple).
adj(quiet,quiet).
adj(quick,quick).
adj(rare,rare).
adj(ready,ready).
adj(real,real).
adj(recent,recent).
adj(red,red).
adj(regular,regular).
adj(rescued,rescued).
adj(responsible,responsible).
adj(rich,rich).
adj(rightcorrect,correct).
adj(rough,rough). %HU:durva
adj(round,round). %HU:kerek
adj(rusty,rusty).
adj(sad,sad).
adj(safe,safe).
adj(samealso,same).
adj(saved,saved).
adj(secret,secret).
adj(senior,senior).
adj(separate,separate).
adj(serious,serious).
%adj(several,several).
adj(short,short).
adj(sharp,sharp).
adj(sick,sick).
adj(silver,silver).
adj(simple,simple).
adj(single,single).
adj(slow,slow).
adj(small,small).
adj(smart,smart).
adj(smooth,smooth).
adj(social,social).
adj(soft,soft).
adj(solid,solid).
adj(sorry,sorry).
adj(sour,sour).
adj(special,special).
adj(specific,specific).
adj(sticky,sticky). %HU:ragad�s
adj(straight,straight). %HU: egyenes
adj(strange,strange). %HU furcsa, k�l�n�s
adj(strong,strong).
adj(stupid,stupid).
adj(such,such).
adj(sudden,sudden).
adj(sure,sure).
adj(sweet,sweet).
adj(tall,tall).
adj(thick,thick). %HU: vastag
adj(thin,thin).
adj(thirsty,thirsty).
adj(tight,tight).
adj(tired,tired).
adj(true,true).
adj(ugly,ugly).
adj(useful,useful).
adj(usual,usual).
adj(violent,violent).
adj(warm,warm).
adj(weak,weak).
adj(weird,weird).
adj(wet,wet).
adj(white,white).
adj(wide,wide). %HU:sz�les
adj(wise,wise). %HU:b�lcs
adj(wrong,wrong).
adj(yellow,yellow).
adj(young,young).

adjGr(bad,worse,worst).
adjGr(good,better,best).

adjKind(white,color).
adjKind(black,color).
adjKind(green,color).
adjKind(blue,color).
adjKind(red,color).
adjKind(brown,color).
adjKind(yellow,color).
adjKind(grey,color).

%adv(east,place,east).
%adv(north,place,north).
%adv(south,place,south).
%adv(west,place,west).

advGr(badly,worse,worst).
advGr(far,farther,farthest).
advGr(far,further,furthest).
advGr(little,less,least).
advGr(well,better,best).


adv(absolutely,degree,absolutely).
adv(again,time,again).
adv(ago,time,ago).
adv(allDay,time,[all,day]).
adv(almost,freq,almost).
adv(almost,degree,almost).
adv(alone,manner,alone).
adv(also,link,also).
adv(always,freq,always).
adv(amazingly,manner,amazingly).
adv(angrily,manner,angrily).
adv(automatically,manner,automatically).
adv(average,manner,averagely).
adv(averagely,manner,averagely).
adv(away,place,away).
adv(awefully,degree,awefully).
adv(awesomely,manner,awesomely).
adv(back,place,back).
adv(badly,manner,badly).
adv(basically,manner,basically).
adv(before,time,before).
adv(carefully,manner,carefully).
adv(certainly,prob,certainly).
adv(comfortably,manner,comfortably).
adv(completely,degree,completely).
adv(consequently,link,consequently).
adv(correctly,manner,correctly).
adv(daily,time,daily).
adv(definitely,prob,definitely).
adv(down,place,down).
adv(earlier,time,earlier).
adv(easily,manner,easily).
adv(electrically,manner,electrically).
adv(especially,focus,especially).
adv(emptily,manner,emptily).
adv(enough,degree,enough).
adv(entirely,degree,entirely).
adv(even,focus,even).
adv(ever,freq,ever).
adv(exactly,manner,exactly).
adv(extremely,degree,extremely).
adv(fairly,degree,fairly).
adv(far,place,far).
adv(fast,manner,fast).
adv(finally,time,finally).
adv(finely,manner,finely).
adv(freely,manner,freely).
adv(fully,manner,fully).
adv(funnily,manner,funnily).
adv(generally,freq,generally).
adv(happily,manner,happily).
adv(hardAdv,manner,hard).
adv(hardly,freq,hardly).
adv(here,place,here).
adv(highly,degree,highly).
adv(home,place,home).
adv(however,link,however).
adv(importantly,manner,importantly).
adv(just,focus,just).
adv(largely,focus,largely).
adv(lastpast,time,last).
adv(lastYear,time,[last,year]).
adv(lately,time,lately).
adv(later,time,later).
adv(left,place,left).
adv(least,super,least).
adv(less,comp,less).
adv(lots,degree,lots).
adv(mainly,focus,mainly).
adv(maybe,prob,maybe).
adv(more,degree,more).
adv(more,comp,more).
adv(most,super,most).
adv(much,degree,much).
adv(nearly,degree,nearly).
adv(nearly,freq,nearly).
adv(never,freq,never).
adv(newly,manner,newly).
adv(nicely,manner,nicely).
adv(not,neg,not).
adv(now,time,now).
adv(offline,manner,offline).
adv(occasionally,freq,occasionally).
adv(often,freq,often).
adv(oldly,manner,oldly).
adv(online,manner,online).
adv(only,focus,only).
adv(open,manner,open).
adv(out,place,out).
adv(outside,place,outside).
adv(over,place,over).
adv(overnight,time,overnight).
adv(particularly,focus,particularly).
adv(past,time,past).
adv(perfectly,degree,perfectly).
adv(perhaps,prob,perhaps).
adv(possibly,prob,possibly).
adv(pretty,degree,pretty).
adv(probably,prob,probably).
adv(quite,degree,quite).
adv(rarely,freq,rarely).
adv(rather,degree,rather).
adv(really,degree,really).
adv(recently,time,recently).
adv(regularly,manner,regularly).
adv(remarkably,degree,remarkably).
adv(rightdirection,place,right).
adv(sadly,manner,sadly).
adv(secret,manner,secretly).
adv(seldom,freq,seldom).
adv(seriously,manner,seriously).
adv(sickly,manner,sickly).
adv(simply,focus,simply).
adv(since,time,since).
adv(slightly,degree,slightly).
adv(slowly,manner,slowly).
adv(so,degree,so).
adv(sometimes,freq,sometimes).
adv(somewhat,degree,somewhat).
adv(soon,time,soon).
adv(sooner,time,sooner).
adv(still,focus,still).
adv(sure,manner,sure).
adv(terribly,degree,terribly).
adv(then,time,then).
adv(then,link,then).
adv(there,place,there).
adv(tiredly,manner,tiredly).
adv(today,time,today).
adv(together,manner,together).
adv(tomorrow,time,tomorrow).
adv(too,degree,too).
adv(totally,degree,totally).
adv(twice,freq,twice).
adv(up,place,up).
adv(usually,freq,usually).
adv(very,degree,very).
adv(well,manner,well).
adv(wrongly,manner,badly).
adv(yesterday,time,yesterday).
adv(youngly,manner,youngly).

adv(up,part,up).
adv(for,part,for).

%Per comma connecting adverbs may come in the front of sentences
%following by a comma. Eg: "However, we were sleeping."
adv(accordingly,connecting(comma),accordingly).
adv(besides,connecting(comma),besides).
adv(consequently,connecting(comma),consequently).
adv(furthermore,connecting(comma),furthermore).
adv(hence,connecting(comma),hence).
adv(however,connecting(comma),however).
adv(likewise,connecting(comma),likewise).
adv(moreover,connecting(comma),moreover).
adv(nevertheless,connecting(comma),nevertheless).
adv(nonetheless,connecting(comma),nonetheless).
adv(otherwise,connecting(comma),otherwise).
adv(quickly,connecting(comma),quickly).
adv(still,connecting(comma),still).
adv(therefore,connecting(comma),therefore).
adv(thus,connecting(comma),thus).
adv(so,connecting(comma),so).


infPart(to).
