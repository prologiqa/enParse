:- module(enGen,[toSentence/2, toSentence//1]).

:- use_module(prolog(dictUtils/prolog/pl2dict)).


switchOut:- false.

/** <module> English text generator module

The module traverses the syntax tree,
and generates an English sentence
in form of a word list.

creation: 10-Oct-2019

@author Kili�n Imre
*/

%Syntax tree format
%
%Sentences:
%----------
%compound(CONN,CLAUSE1,CLAUSE2) CONN: connective, CLAUSE: clause
%CLAUSE a pure clause
%?(CLAUSE) yes/no interrogative clause (without interrogative)
%?(WH,CLAUSE) wh interrogative clause (with interrogative word)
%
%Clauses:
%--------
%clause(KIND,DIR,FREE,NPS,VP,POLARITY)))
% - direct clause with/out free arguments
%
%ARGUMENTS - a Prolog list of NounPhrase arguments
%FREEARGS - a Prolog list of free arguments (NP-s or adverbs)
%
%NounPhrases(NPs)
%----------------
%... is a Prolog list of possibly cumulated noun phrases
%... with the same case
%
%np(NP,PERS,GENDER,CASE)
%PERS: sg(1..3), pl(1..3)
%GENDER: male;female;neutral
%CASE: nom, acc, gen
%
%poss(NP,OWNER) - possessive noun phrase structure, owner in front
%poss(NP,OWNER) - possessive noun phrase structure, owner at the end
% - NP and OWNER are (Numeral)NounPhrasesy
%
%numeral(KIND,NR,NP) - numeral noun phrase structure
%KIND: ord;card;indef
% NR the value of numeral, if any (otherwise the numeral
% itself)
%
%adj(ADJ,NP) - ADJ: an adjective, NP: an adjective noun phrase
%noun(N) - a pure noun
% pronoun(N,HUMAN,GENDER,CASE) a pronoun
%
%VerbalPhrases(VPs)
%------------------
%vp(VERB,ADV,MODE,TENSE,ARGUMENTS,FREE)
%
% VERB: verb construction - (with auxiliaries)
% MOOD:imperative;conditional;
%      declarative;interrogative(yn);interrogative(wh)
% TENSE:present;present(perfect);present(continuous); -
% past;past(perfect);past(continuous) -
% future;future(perfect);future(continuous)....
%

:- use_module('../parser').

%:- use_module(enDict).
%dynamic import - module should be explicitely loaded by boot
%and here only the module name is used (instead of file name)
:- add_import_module(enGen,enDict,end).

:- use_module(library(apply)).
:- use_module(library(occurs)).


interj2English(IJ)--> {interj(IJ,WORD)}, [WORD].

art2English(ART,KIND,NUM)--> {art(KIND,ART,NUM)}, [ART].

det2English(DET)--> {atom(DET)}, !, [DET].
det2English(DET)--> {is_list(DET)}, !, DET.

conn2English(connect(CONJ,KIND))-->
   {conj(CONJ,WORD,KIND)}, !, [WORD].
conn2English(PUNCT)-->
   {punct(PUNCT)}, !, [PUNCT].
conn2English(CONN)--> {conj(CONN,WORD,_)}, [WORD].


adv2English([])--> ! .
adv2English(ADVLIST)--> {is_list(ADVLIST), adv_(_,_,_,ADVLIST)}, !, ADVLIST.
adv2English([ADV|LIST])--> !, adv2English(ADV), adv2English(LIST).
adv2English(adv(ADV,_,base))--> adv2English(ADV).
adv2English(adv(ADV,_,base))--> adv2English(ADV), ! .
adv2English(ADV)--> {atom(ADV), adv_(_,_,_,ADV)}, !, [ADV].

adjGrade(base,base).
adjGrade(more,comp).
adjGrade(comp,comp).
adjGrade(most,super).
adjGrade(most,super).
adjGrade(super,super).

adj2English([],X,X):- ! .
adj2English([ADJ])-->  adj2English(ADJ), !.
adj2English([ADJ|ADJLIST])-->  adj2English(ADJ), !, [','],
   adj2English(ADJLIST).

adj2English(ADJ)--> {atom(ADJ), adj_(ADJ,base,ADJECT,_)}, !, [ADJECT].
adj2English(adj(ADJ))--> adj2English(ADJ), !.
adj2English(adj(verb(VERB,present(participle)),base))-->
  {verb_(VERB,present(participle),_,INGFORM,_SEM)}, [INGFORM].
adj2English(adj(verb(VERB,past(participle)),base))-->
  {verb_(VERB,past(participle),_,EDFORM,_SEM)}, [EDFORM].
adj2English(adj(ADJ,GRAD))-->
  {once(adjGrade(GRAD,GRADE)),adj_(ADJ,GRADE,ADJECT,_SEM)},
  ({is_list(ADJECT)}->ADJECT;
  {term_string(ADJECT,ADJECTIV),atom_string(ADJECTIVE,ADJECTIV)},
    [ADJECTIVE]).
adj2English(adjP(ADV,GRADE,ADJ))-->
  adv2English(ADV), !, adj2English(adj(ADJ,GRADE)).
adj2English(adjP(KIND,NR,UNIT,ADJ))--> !, {NR=1->NUM=sg;NUM=pl},
  np2English(numeral(KIND,NR,NUM/3,common(UNIT,NUM/3,CASE)),NUM/3,_,CASE),
  adj2English(adj(ADJ)).


num2ord(N,NUMBER):- LAST is N rem 10,
    (LAST=:=1->atom_concat(N,'-st',NUMBER);
     LAST=:=2->atom_concat(N,'-nd',NUMBER);
     LAST=:=3->atom_concat(N,'-rd',NUMBER);
     atomic_concat(N,'-th',NUMBER)).

num2English(card,NUMR,_)--> {atom(NUMR)}, !, [NUMR].
num2English(card,N,pl)--> {atom_number(NR,N)}, !, [NR].
num2English(ord,N,sg)--> !, {num2ord(N,CARD)}, [CARD].
%I had 'many' children.
num2English(indef,INDEF,NUM)--> {indef(_,INDEF,NUM,_,_)},
   {atomic(INDEF)}, [INDEF].

%%     wrappers for verb(V) structure
%verb2English(+VERB,+KIND,+PERSNUM,-FR)
%+VERB: infinite
%KIND:inf,sg/3,present,past,perfect,participle
%PERSNUM: sg/1..pl/3
%verb2English('',_KIND,_NUMPERS,_FRAME)--> !, [].
verb2English('',_KIND,_NUMPERS,[])--> !, [].
verb2English(V- ?(PART),KIND,NUMPERS,FRAME)--> {atom(V)}, !,
  {verb_(V,KIND,NUMPERS,FORM,_)}, !, [FORM],
  {verbAPartFrame(_,V-PART,FRAME)}.
verb2English([V-PART],KIND,NUMPERS,FRAME)--> !,
   verb2English(V-PART,KIND,NUMPERS,FRAME).
verb2English(V-PART,KIND,NUMPERS,FRAME)--> {atom(V)},
  {verb_(V,KIND,NUMPERS,FORM,_)}, !, [FORM],
  [PART], {verbAPartFrame(_,V-PART,FRAME)}.
verb2English([V],KIND,NUMPERS,FRAME)--> {atom(V)},
  {verb_(V,KIND,NUMPERS,FORM,SEM)}, !, [FORM], {verbAPartFrame(SEM,V,FRAME)}.
verb2English([A|V],KIND,NUMPERS,FRAME)--> {atom(A), atom(V)},
  {verb_(V,KIND,NUMPERS,FORM,SEM)}, !, [FORM], {verbAPartFrame(SEM,V,FRAME)}.
verb2English(verb(VERB,KIND,_NUM_PERS),KIND,inf,FRAME)-->
 !, verb2English(VERB,KIND,inf,FRAME).
verb2English(verb(VERB,KIND,NUMPERS),KIND,NUMPERS,FRAME)-->
 !, verb2English(VERB,KIND,NUMPERS,FRAME).
verb2English(VERB,KIND,NUMPERS,FRAME)-->
 {verb_(VERB,KIND,NUMPERS,FORM,SEM)}, [FORM], {verbAPartFrame(SEM,VERB,FRAME)}.

%nondeterministically evaluates possessive pronouns
possPronoun2English(sg/3,HUMAN,_GENDER)--> {HUMAN==false}, !,
    {persPronoun(sg/3,neutral,gen,PRONOUN)}, [PRONOUN].
possPronoun2English(NUMPERS,_HUMAN,GENDER)-->
    {persPronoun(NUMPERS,GENDER,gen,PRONOUN)}, [PRONOUN].

demPronoun2English(DEM)--> {atomic(DEM), det(_,DEM,_,_)}, !, [DEM].
demPronoun2English(DEM)--> {is_list(DEM), det(_,DEM,_,_)}, DEM.

indefPronoun2English(INDEF,NUM)-->
   {atomic(INDEF), indefPronoun(_,INDEF,NUM)}, !, [INDEF].
indefPronoun2English(INDEF,NUM)-->
   {is_list(INDEF), indefPronoun(_,INDEF,NUM)}, INDEF.

proper2English(PROPER,_CLASS,_NUM,_CASE)-->
  {string(PROPER), atom_string(ATOM,PROPER)}, !, [ATOM].
proper2English([PRO|PER],CLASS,NUM,CASE)-->
  {proper_([PRO|PER],NUM,CASE,FORM,CLASS,_SEM)}, !, FORM.
proper2English([PRO|PER],_CLASS,_NUM,_CASE)-->
  !, [PRO|PER].
proper2English(PROP,CLASS,NUM,CASE)-->
  {proper_(PROP,NUM,CASE,PROPER,CLASS,_SEM)}, !, [PROPER].
proper2English(PROPER,_CLASS,_NUM,_CASE)-->
  {atom(PROPER)}, !, [PROPER].


np2English(VAR,_NUMP,_GENDER,_CASE)--> {var(VAR)}, [], !.
np2English('',_NUMP,_GENDER,_CASE)--> [], !.
np2English(INTERR,_NUMP,_GENDER,_CASE)--> {functor(INTERR,interr,_)}, [], !.
np2English(actpart(NP,VP),NUMP,GENDER,CASE)-->
  !, np2English(NP,NUMP,GENDER,CASE),
  vp2English(VP,'',straight,NUMP,_POL).
np2English(passpart(NP,VP),NUMP,GENDER,CASE)-->
  !, np2English(NP,NUMP,GENDER,CASE),
  vp2English(VP,'',straight,NUMP,_POL).
np2English(that(subject,NP,VP),NUMP,GENDER,CASE)-->
  !, np2English(NP,NUMP,GENDER,CASE), [that],
  vp2English(VP,'',straight,NUMP,_POL).
np2English(that(pendent,NP,CL),NUMP,GENDER,CASE)-->
  !, np2English(NP,NUMP,GENDER,CASE), [that],
  clause2English(CL,_).
np2English(infinite(NP,VP),NUMP,GENDER,CASE)-->
  !, np2English(NP,NUMP,GENDER,CASE), [to],
  vp2English(VP,'',straight,NUMP,_POL).

np2English(pronoun(DEM),_NUMP,_GENDER,_CASE)-->
  demPronoun2English(DEM).
np2English(refl(PERSNUM,GENDER),PERSNUM,GENDER,_)-->
     {reflPronoun(PERSNUM,GENDER,ENGLISH)}, !, [ENGLISH].
np2English(refl(REFL,GENDER,CASE,NP),NUMPERS,GENDER,CASE)-->
  np2English(NP,NUMPERS,GENDER,CASE),
  {reflPronoun(REFL,GENDER,ENGLISH)}, !, [ENGLISH].
np2English(wh(WH),_NUMP,_GENDER,_CASE)--> [WH], !.
np2English(both(NP1,NP2),pl/3,GENDER,CASE)--> !,
  np2English(NP1,_NUMPERS1,GENDER,CASE), [and],
  np2English(NP2,_NUMPERS2,GENDER,CASE), [both].
np2English(coordinating(CONN,[NP1,NP2]),pl/3,GENDER,CASE)--> !,
  np2English(coordinating(CONN,NP1,NP2),_NUMPERS,GENDER,CASE).
np2English(coordinating(CONN,[NP1,NP2|NPS]),pl/3,GENDER,CASE)--> !,
  np2English(NP1,sg/3,GENDER,CASE), [','],
  np2English(coordinating(CONN,[NP2|NPS]),pl/3,GENDER,CASE).
np2English(coordinating(CONN,NP1,NP2),_NUMPERS,GENDER,CASE)--> !,
  np2English(NP1,_NUMPERS1,GENDER,CASE), conn2English(CONN),
  np2English(NP2,_NUMPERS2,GENDER,CASE).
np2English(correlative(CONN,NP1,NP2),NUMPERS,GENDER,CASE)--> !,
  {correlative(CONN,C1,C2,nounPhrase)}, conn2English(C1),
  np2English(NP1,_NUMPERS1,GENDER,CASE), conn2English(C2),
  np2English(NP2,NUMPERS,GENDER,CASE).
np2English(post(NP,POST),NUMPERS,GENDER,CASE)--> !,
  np2English(NP,NUMPERS,GENDER,CASE), np2English(POST,_,_,acc).
np2English(pronoun(PRONOUN,GENDER),_NUMP,_GENDER,_CASE)-->
%if OWNER is a person???????
  {persPronoun(PRONOUN,GENDER,poss,FORM)}, !, [FORM].
np2English(poss(OWNED,NUMPERS,PRONOUN,G),NUMPERS,G,CASE)--> !,
  possPronoun2English(PRONOUN,_HUMAN,G), np2English(OWNED,NUMPERS,G,CASE).
np2English(poss(OWNED,NUMPERS,OWNER),NUMPERS,GENDER,CASE)-->
  np2English(OWNED,NUMPERS,GENDER,CASE), [of], np2English(OWNER,_NUM,_,acc), ! .
np2English(poss(OWNED,NUMPERS,OWNER),NUMPERS,GENDER,CASE)-->
  np2English(OWNER,_NUM,_,gen), np2English(OWNED,NUMPERS,GENDER,CASE), ! .
np2English(postposs(OWNED,OWNER,OWNERSGENDER),NUMPERS,GENDER,CASE)--> !,
  np2English(OWNED,NUMPERS,GENDER,CASE), [of],
  {persPronoun(OWNER,OWNERSGENDER,poss,FORM)}, [FORM].
np2English(persposs(OWNED,OWNER),NUMPERS,GENDER,CASE)--> !,
  np2English(OWNER,_PERS,GENDER,gen), np2English(OWNED,NUMPERS,GENDER,CASE).
np2English(numcomp(NP1,<,NP2),NUMPERS,GENDER,CASE)-->
  [less], np2English(NP1,NUMPERS,GENDER,CASE), !, [than],
  np2English(NP2,_NUMP,_G2,CASE).
np2English(numcomp(NP1,>,NP2),NUMPERS,GENDER,CASE)-->
  [more], np2English(NP1,NUMPERS,GENDER,CASE), !, [than],
  np2English(NP2,_NUMP,_G2,CASE).
np2English(prep(PREP,ARG),NUMPERS,GENDER,CASE)-->
  !, [PREP], {ignore(once(CASE=..[PREP,CAS];CAS=CASE))},
  (np2English(ARG,NUMPERS,GENDER,CAS)->{true};
  adv2English(ARG)).
np2English(postp(POST,NP),NUMPERS,GENDER,CASE)--> !,
  np2English(NP,NUMPERS,GENDER,CASE), [POST].
np2English(adj(A,numeral(card,1,sg,det(DET,NP))),NUMPERS,GENDER,CASE)-->
  {A\=adjP(super,_)}, !, [a], adj2English(A), [one,of], det2English(DET),
  np2English(NP,NUMPERS,GENDER,CASE).
np2English(adj(A,numeral(card,1,sg,det(DET,NP))),NUMPERS,GENDER,CASE)-->
  {A=adjP(super,_)}, !, adj2English(A), [one,of], det2English(DET),
  np2English(NP,NUMPERS,GENDER,CASE).
np2English(adj(A,det(DET,NP)),NUMPERS,GENDER,CASE)--> !,
  adj2English(A), [of], det2English(DET), np2English(NP,NUMPERS,GENDER,CASE).
np2English(adj(A,numeral(card,1,sg,NP)),sg,_GENDER,_CASE)-->
  {var(NP)}, !, adj2English(A), [one].
np2English(adj(A,_GRADE,NP),NUMPERS,GENDER,CASE)-->
  adj2English(A), !, np2English(NP,NUMPERS,GENDER,CASE).
% np2English(det(ADV,D,NUMPERS,NP),NUMPERS,GENDER,CASE)--> !,
% adv2English(ADV),
%  np2English(det(D,NUMPERS,NP),NUMPERS,GENDER,CASE).
%eg. "all of" demands pl, but "his money" is sg
%eg. "either of" "my parents"
np2English(det(D,NUMPERSNP,NP),NUMPERSNP,GENDER,CASE)-->
  det2English(D), np2English(NP,_NUMPERS,GENDER,CASE).
np2English(art(ART,NUM/3,KIND,NP),NUM/3,GENDER,CASE)--> !,
  art2English(ART,KIND,NUM), np2English(NP,NUM/3,GENDER,CASE).
np2English(numeral(KIND,NUMERAL,adv(ADV,AK,base),_NUM,NP),
           NUM/3,GENDER,CASE)--> !,
  ({AK=comp}->num2English(KIND,NUMERAL,NUM),adv2English(adv(ADV,AK,base)),
    np2English(NP,NUM/3,GENDER,CASE);
  adv2English(adv(ADV,AK,base)), num2English(KIND,NUMERAL,NUM),
    np2English(NP,NUM/3,GENDER,CASE)).
np2English(numeral(KIND,1,_NUM,NP),NUMPERS,GENDER,CASE)-->
  !, num2English(KIND,1,_), np2English(NP,NUMPERS,GENDER,CASE).
np2English(numeral(ord,N,_NUM,NP),NUM/3,GENDER,CASE)-->
  !, num2English(ord,N,NUM),
  np2English(NP,NUM/3,GENDER,CASE).
np2English(numeral(KIND,N,NUM/3,NP),NUM/3,GENDER,CASE)-->
  !, num2English(KIND,N,NUM), np2English(NP,NUM/3,GENDER,CASE).
np2English(#(NP,NR),NUM/3,GENDER,CASE)-->
  !, np2English(NP,NUM/3,GENDER,CASE), num2English(card,NR,_).
np2English(quantity(NR,UNIT,abbr,NUM/3,CASE),NUM/3,_GENDER,CASE)-->
  num2English(card,NR,NUME), {ignore(NUME=NUM)},
  {once(measure(_,UNIT,_,_))}, !,
  ({atom(UNIT)}->[UNIT];{is_list(UNIT)}, UNIT).
np2English(quantity(NR,UNIT0,full,NUM/3,CASE),NUM/3,_GENDER,CASE)-->
  num2English(card,NR,NUME), {ignore(NUME=NUM)}, {once(measure(UNIT0,_,_,_))},
  {noun_(UNIT0,NUM,CASE,UNIT,_)}, !,
  ({atom(UNIT)}->[UNIT];{is_list(UNIT)}, UNIT).
np2English(common(NOUN,NUM0/3,CASE0),NUM/3,_GENDER,CASE)-->
  {atom(NOUN), \+ \+ CASE0=CASE, ignore(NUM0=NUM)},
  {noun_(NOUN,NUM,CASE,FORM,_SEM)}, !, [FORM].
np2English(common([NOUN|NOUNS],NUM0/3,CASE0),NUM/3,_GENDER,CASE)-->
  {\+ \+ CASE0=CASE, ignore(NUM0=NUM)},
  {noun_([NOUN|NOUNS],NUM,CASE,FORM,_SEM)}, !, FORM.
np2English(common([NOUN1,NOUN2],NUM0/3,CASE0),NUM/3,_GENDER,CASE)-->
  {\+ \+ CASE0=CASE, ignore(NUM0=NUM)},
  {noun_(NOUN1,sg,nom,FORM1,_SEM1), noun_(NOUN2,NUM,CASE,FORM2,_SEM2)}, !,
  [FORM1,FORM2].
np2English(common(NOUN1-NOUN2,NUM0/3,CASE0),NUM/3,_GENDER,CASE)-->
  {\+ \+ CASE0=CASE, ignore(NUM0=NUM)},
  {noun_(NOUN1-NOUN2,NUM,CASE0,F1-F2,_SEM1)}, !, [F1,-,F2].
np2English(proper(PROPER,CLASS,NUM0/3,CASE0),NUM/3,_GENDER,CASE)-->
  {\+ \+ CASE0=CASE, ignore(NUM0=NUM)},
  proper2English(PROPER,CLASS,NUM,CASE0).
np2English(noun(LIST,_NUM,_CASE),_,_,_)--> {is_list(LIST)}, !, LIST .
np2English(pronoun(NUM/PERS,GENDER,CASE0),NUM/PERS,GENDER,_CASE)-->
  {persPronoun(NUM/PERS,GENDER,CASE0,FORM)}, !, [FORM].
np2English(indef(INDEF,NUM/3),NUM/3,_GENDER,_CASE)--> !,
  indefPronoun2English(INDEF,NUM).
np2English(NP,_NUM,_,_CASE)--> {atom(NP)}, !, [NP].

case(nom).
case(acc).
case(gen).

nps2English([],_,_)--> !, {true}.
nps2English(coordinating(CONN,NP,NPS),NUMPERS,CASE)--> !,
   np2English(NP,NUMPERS,_,CASE), [CONN], nps2English(NPS,NUMPERS,CASE).
nps2English([NP|NPS],NUMPERS,CASE)--> !,
  np2English(NP,NUMPERS,_,CASE), nps2English(NPS,NUMPERS,CASE).
nps2English(np(NP,NUMPERS,GENDER,CASE),NUMPERS,CASE)-->
  np2English(NP,NUMPERS,GENDER,CASE), !.
nps2English(NP,NUMPERS,CASE)-->
  np2English(NP,NUMPERS,_GENDER,CASE), !.

ref2English(ADV)--> adv2English(ADV), !.
ref2English(ADJ)--> adj2English(ADJ), !.
ref2English(NP)--> np2English(NP,_,_,_), !.
ref2English(CL)--> clause2English(CL,declarative).

arg2English(_,INTERR)--> {INTERR =..[interr|_]}, !.
arg2English(_,interj(IJ))--> interj2English(IJ), !.
arg2English(CASE,adv(ADV,NP))-->
   {compound(NP)}, !, adv2English(ADV), arg2English(CASE,NP).

arg2English(_,ADV)--> adv2English(ADV), !.
arg2English(_,ADJ)--> adj2English(ADJ), !.
arg2English(_,CONJ)--> conn2English(CONJ), !.
arg2English(_FRAMES,art(ART,KIND,ADJP))--> art2English(ART,KIND,_NUM),
       adj2English(ADJP), !.
arg2English(FRAME,art(ART,KIND,NP))--> art2English(ART,KIND,NUM),
       np2English(NP,NUM/3,_GENDER,FRAME), !.
arg2English(_CASE,ord(1))--> !, [first].
arg2English(_CASE,ord(2))--> !, [second].
arg2English(_CASE,ord(3))--> !, [third].
arg2English(_CASE,ord(N))--> num2English(ord,N,_).
arg2English(CASE,connected(CONN,NPP1,NPP2))-->
    arg2English(CASE,NPP1), conn2English(CONN), arg2English(CASE,NPP2).

arg2English(_FRAMES,comp(REF1,<,REF2))-->
       ref2English(REF1), !, [than], ref2English(REF2).
arg2English(_FRAMES,comp(REF1,>,REF2))-->
       ref2English(REF1), !, [than], ref2English(REF2).
arg2English(_FRAMES,comp(REF1,=,REF2))-->
       [as], ref2English(REF1), !, [as], ref2English(REF2).
arg2English(clause(wh),connect(IP,CLAUSE))-->
       !, ip2English(IP,_KIND),
       arg2English(clause,CLAUSE).
arg2English(clause(C),connect(CONNECT,CLAUSE))-->
       {C \= wh}, !, conn2English(CONNECT),
       arg2English(clause,CLAUSE).
arg2English(clause,clause(KIND,DIR,FREE,NP,VP,NUMPERS,POL))--> !,
     clause2English(clause(KIND,DIR,FREE,NP,VP,NUMPERS,POL),declarative).
arg2English(verbal,verbal(VP))--> !,
   vp2English(VP,'',straight,sg/_P,_POL).
arg2English(verbal(to),verbal(to,VP))--> !, [to],
   vp2English(VP,'',straight,sg/_P,_POL).
arg2English(verbal(ing),verbal(ing,VP))--> !,
   vp2English(VP,'',straight,sg/_P,_POL).
arg2English(verbal(wh),verbal(IP,VP))--> !,
   ip2English(IP,_KIND), [to],
   vp2English(VP,'',straight,sg/_P,_POL).
arg2English(CASE,NP)-->
    {nonvar(CASE), CASE=..[PRE,PURECASE],
    NP = prep(PRE,_)}, !, np2English(NP,_NUMPERS,_GENDER,PURECASE).
arg2English(CASE,NP)-->
    np2English(NP,_NUMPERS,_GENDER,CASE).

%invoked by args2English//1
arg2English(ARG)--> arg2English(_,ARG).

%args2English(FRAMES,ARGS)
%+ARGS: argument values
%+FRAMES: frame descriptions
%
args2English(_,VAR)--> {var(VAR)}, !, {fail}.
args2English(CASES,ARGS)-->foldl(arg2English,CASES,ARGS).

%args2English(_,[])--> !.
%args2English([CASE|CASES],[ARG|ARGS])--> !,
%    arg2English(CASE,ARG), args2English(CASES,ARGS).

opt1Args2English([?(FRAME)|FRAMES],ARGUMENTS)-->
   args2English([FRAME|FRAMES],ARGUMENTS), !.
opt1Args2English([?(_)|FRAMES],ARGUMENTS)-->
   args2English(FRAMES,ARGUMENTS).

args2English(ARGS)-->foldl(arg2English,ARGS).

frees2English(FREES)-->args2English(FREES).

optNegation(+,X,X).
optNegation(-)--> [not].

callNT(NT,X0,X):- call(NT,X0,X).

ifStraight(straight,NT,NUMPERS,CASE)--> {NT\=''}, !,
    callNT(nps2English(NT,NUMPERS,CASE)).
ifStraight(_,_,_,_,X,X).

ifInverted(inverted,NT,NUMPERS,CASE)--> {NT\=''}, !,
    callNT(nps2English(NT,NUMPERS,CASE)).
ifInverted(_,_,_,_,X,X).


ifOpposite(opposite,NT,NUMPERS,CASE)--> {NT\=''}, !,
    callNT(nps2English(NT,NUMPERS,CASE)).
ifOpposite(_,_,_,_,X,X).

/*
%kindOfMood(imperative).
kindOfMood(declarative).
kindOfMood(interrogative(yn)).
kindOfMood(interrogative(wh)).
%kindOfMood(conditional).
*/

kindOfDir(straight).
kindOfDir(inverted).
kindOfDir(opposite).

kindOfPolarity(+).
kindOfPolarity(-).

%auxVerb2English(AUX,DEFAULT,KIND,PERS)
%+AUX: auxiliary verb, optionally empty ('')
%+DEFAULT: default verb in case when AUX=''
%+KIND: - tense of the auxiliary verb
%+PERS: person of verb
%-FRAMES: framelist of verb
auxVerb2English(AUX,_,KIND,PERS)-->
 {aux(AUX)},{verb_(AUX,KIND,PERS,FORM,_)}, [FORM], !.
auxVerb2English(AUX,_,present,_PERS)-->
 {aux(AUX,_,'')}, !, [AUX].
auxVerb2English(AUX,_,past,_PERS)-->
 {aux(AUX,PAST,'')}, !, [PAST].
auxVerb2English(MODAL,_,past,_PERS)-->
 {modal(MODAL,_,_)}, !, [MODAL].
auxVerb2English(AUX,_,KIND,PERS)-->
 {aux(AUX,_,'')}, {verb_(AUX,KIND,PERS,FORM,_)}, !, [FORM].
auxVerb2English(AUX,_,KIND,PERS)-->
 {aux(AUX,_,TO)}, {verb_(AUX,KIND,PERS,FORM,_)}, !, [FORM],[TO].
auxVerb2English(MODAL,_,_KIND,_PERS)-->
 {modal(MODAL,_,_,verb)}, !, [MODAL].
auxVerb2English(MODAL,_,_KIND,_PERS)-->
 {modal(MODAL,_,_,verbal(to))}, !, [MODAL],[to].
auxVerb2English('',DEFAULT,KIND,PERS)-->
 {verb_(DEFAULT,KIND,PERS,FORM,_)}, !, [FORM].
%auxVerb2English(AUX,_,present,_PERS)-->
% {aux(AUX,_,'')}, !, [AUX].
%auxVerb2English(AUX,_,past,_PERS)-->
% {aux(AUX,PAST,'')}, !, [PAST].


aux2English('',_,_)--> !, [] .
%aux2English(V,KIND,PERS)-->
%  {aux(V)},{verb_(V,KIND,PERS,FORM,_)}, [FORM], !.
% {verb_(do,KIND,PERS,FORM,_)}, [FORM], !.
aux2English(AUX,KIND,PERS)-->
 auxVerb2English(AUX,_,KIND,PERS).

%vp(VERB,MADV,MODE,ARGUMENTS,TENSE,FREE)
%+VERB: a verb structure (evtl with auxiliaries)
%+ADV: if the verbal structure had a middle adverb
%?MODE=imperative;conditional;
%      declarative;interrogative(wh);interrogative(yn)
%+ARGUMENTS: verbal arguments
%?MODAL= ????....can, should, would????????
%?TENSE=present;present(perfect);present(participle)...
%?TENSE=past...;future
%+FREE: list of free arguments
%

optGenAux('',_,_)--> [].
optGenAux(AUX,PERS,FORM)-->
   aux2English(AUX,FORM,PERS).

optGenMiddleAdv('')-->[], !.
optGenMiddleAdv(adv(_,neg,base))-->[], !.
optGenMiddleAdv(ADV)-->adv2English(ADV).

% vp2English//11
% vp2English(+VERB,+NPS,+FRAMES,-REM,+ARGS,+AUX,
%            +ADV,+MODE,+POL,+TENSE,+PERS)
% +VERB: atom +NPS: Noun phrase structure (evtly a list of them->NOT
% IMPLEMENTED) +FRAMES: frame specification forced from external (eg.
% from macro) -REM: not processed tail of frame specification list
% (FRAMES-REM is an accumulator pair!!) +ARGS: verbal argument list
% +AUX: auxiliary verb
% +ADV: adverb between auxiliary and verb or ''
% +MODE: declarative;interrogative(yn);interrogative(wh)
% +POL: polarity +;-
% +TENSE: verbal tense structure (see above)
% +PERS: NUMBER(PERSON) structure (sg(1)..pl(3))
%
% The predicate is the working horse of VP generation
%
% declarative: straight word order.
% "My father drives his car."
% interrogative(wh) wrapper: straight order of words, like for
% declaratives
%

:- discontiguous vp2English//9.

%repeated verb wrapper
vp2English(verb([AUX,_AU,_A,VERB],TENSE,NUMPERS),
           NP,ARGUMENTS,_AUX,ADV,MODE,POL,TENSE,NUMPERS)-->
  \+ {isModal(AUX)}, !,
  vp2English(VERB,NP,ARGUMENTS,'',ADV,MODE,POL,TENSE,NUMPERS).
vp2English(verb([AUX,_AU,_A,VERB],TENSE,NUMPERS),
           NP,ARGUMENTS,_AUX,ADV,MODE,POL,TENSE,NUMPERS)-->
  {isModal(AUX)}, !,
  vp2English(VERB,NP,ARGUMENTS,AUX,ADV,MODE,POL,TENSE,NUMPERS).
%repeated verb wrapper
vp2English(verb([AUX,AU-ADV,VERB],TENSE,NUMPERS),
           NP,ARGUMENTS,AUXX,'',MODE,POL,TENSE,NUMPERS)--> !,
  vp2English(verb([AUX,AU-ADV,VERB],TENSE,NUMPERS),
            NP,ARGUMENTS,AUXX,ADV,MODE,POL,TENSE,NUMPERS).
%repeated verb wrapper
vp2English(verb([AUX,_AU,VERB],TENSE,NUMPERS),
           NP,ARGUMENTS,_AUX,ADV,MODE,POL,TENSE,NUMPERS)-->
  \+ {isModal(AUX)}, !,
  vp2English(VERB,NP,ARGUMENTS,'',ADV,MODE,POL,TENSE,NUMPERS).
%repeated verb wrapper
vp2English(verb([AUX,_AU,VERB],TENSE,NUMPERS),
           NP,ARGUMENTS,_AUX,ADV,MODE,POL,TENSE,NUMPERS)-->
  {isModal(AUX)}, !,
  vp2English(VERB,NP,ARGUMENTS,AUX,ADV,MODE,POL,TENSE,NUMPERS).
vp2English(verb([AUX,VERB],TENSE,NUMPERS),
           NP,ARGUMENTS,_,ADV,MODE,POL,TENSE,NUMPERS)-->
  {\+ isModal(AUX), VERB\=''}, !,
  vp2English(VERB,NP,ARGUMENTS,'',ADV,MODE,POL,TENSE,NUMPERS).
vp2English(verb([AUX,VERB],TENSE,NUMPERS),
           NP,ARGUMENTS,_AUX,ADV,MODE,POL,TENSE,NUMPERS)-->
  {once(isModal(AUX);VERB='')}, !,
  vp2English(VERB,NP,ARGUMENTS,AUX,ADV,MODE,POL,TENSE,NUMPERS).
vp2English(verb([VERB],TENSE,NUMPERS),
           NP,ARGUMENTS,AUX,ADV,MODE,POL,TENSE,NUMPERS)--> !,
  vp2English(VERB,NP,ARGUMENTS,AUX,ADV,MODE,POL,TENSE,NUMPERS).
%general case
vp2English(verb(VERB,TENSE,NUMPERS),
           NP,ARGUMENTS,AUX,ADV,MODE,POL,TENSE,NUMPERS)-->
  {atom(VERB)}, !,
  vp2English(VERB,NP,ARGUMENTS,AUX,ADV,MODE,POL,TENSE,NUMPERS).


%%%%%%%%% INFINITE
vp2English(VERB,_NP,ARGUMENTS,'',ADV,straight,+,inf,PERS)--> !,
  optGenMiddleAdv(ADV),
  verb2English(VERB,inf,PERS,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,straight,-,inf,_PERS)--> !,
  vp2English(VERB,NP,ARGUMENTS,AUX,ADV,straight,-,present,pl/3).

%%%%%%%%% present participle (active continuous)
vp2English(VERB,'',ARGUMENTS,'',ADV,
           _MODE,POL,present(participle),PERS)-->
  optNegation(POL), optGenMiddleAdv(ADV),
  verb2English(VERB,present(participle),PERS,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).

%%%%%%%%% past participle (-ed form)
vp2English(VERB,NP,ARGUMENTS,'',ADV,
           MOOD,POL,past(participle),PERS)-->
  {kindOfDir(MOOD), kindOfPolarity(POL)},
  ifStraight(MOOD,NP,PERS,nom),
  optNegation(POL),
  ifInverted(MOOD,NP,PERS,nom),
  optGenMiddleAdv(ADV),
  verb2English(VERB,past(participle),inf,VFRAMES),
  {ignore([nom,acc|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).


%%%%%%%%% ACTIVE PRESENT
%declarative negative simple present w/o auxiliary verb =
%declarative negative simple present w 'do' as auxverb
%FRAMES received from outside are stronger
vp2English(VERB,NP,ARGUMENTS,'',ADV,
           straight,-,present,PERS)-->
 {VERB \= be},
 vp2English(VERB,NP,ARGUMENTS,do,ADV,straight,-,present,PERS).
%declarative negative simple present 'be' w/o auxiliary verb
vp2English(be,NP,ARGUMENTS,'',ADV,
           straight,-,present,PERS)-->
  nps2English(NP,PERS,nom), optGenMiddleAdv(ADV),
  verb2English(be,present,PERS,VFRAMES), [not],
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
%declarative positive simple present w/o auxiliary verb
vp2English(VERB,NP,ARGUMENTS,'',ADV,
           straight,+,present,PERS)-->
  nps2English(NP,PERS,nom), optGenMiddleAdv(ADV),
  verb2English(VERB,present,PERS,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
%declarative simple +/-present with auxiliary verb
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,
           straight,POL,present,PERS)-->
  {kindOfPolarity(POL), AUX \= ''},
  nps2English(NP,PERS,nom),
  aux2English(AUX,present,PERS), optNegation(POL),
  optGenMiddleAdv(ADV),
  verb2English(VERB,inf,inf,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
%interrogative simple +/- present with 'be'
vp2English(be,NP,ARGUMENTS,AUX,ADV,
           DIR,POL,present,PERS)-->
  {once(DIR==inverted;DIR==opposite)},
  {kindOfPolarity(POL)}, auxVerb2English(AUX,be,present,PERS),
  optNegation(POL), ifInverted(DIR,NP,PERS,nom),
  optGenMiddleAdv(ADV),
  ({AUX \= ''}->verb2English(be,inf,inf,_),
      ifOpposite(DIR,NP,PERS,nom), args2English(ARGUMENTS);
   ifOpposite(DIR,NP,PERS,nom), args2English(ARGUMENTS)).
%interrogative simple +/- present without auxiliary verb
%'Does he write a book?'
vp2English(VERB,NP,ARGUMENTS,'',ADV,
           inverted,POL,present,PERS)-->
  {VERB \= be},
  {kindOfPolarity(POL)},
  auxVerb2English('',do,present,PERS), optNegation(POL),
  nps2English(NP,PERS,nom),
  optGenMiddleAdv(ADV),
  verb2English(VERB,inf,inf,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
%interrogative simple +/- present with auxiliary verb
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,
           inverted,POL,present,PERS)-->
  {VERB \= be}, {kindOfPolarity(POL)},
  ({AUX \= ''}->auxVerb2English(AUX,do,present,PERS),
    optNegation(POL),
    nps2English(NP,PERS,nom), optGenMiddleAdv(ADV),
    verb2English(VERB,inf,inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS);
  auxVerb2English('',do,present,PERS), optNegation(POL),
   nps2English(NP,PERS,nom), aux2English(AUX,inf,inf),
   optGenMiddleAdv(ADV),
   verb2English(VERB,inf,inf,VFRAMES),
   {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
   args2English(FRAMES,ARGUMENTS)).
%declarative/interrogative present perfect w or wo auxiliary
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,
           MOOD,POL,present(perfect),PERS)-->
  {kindOfDir(MOOD), kindOfPolarity(POL)},
  ({AUX \= ''}->ifStraight(MOOD,NP,PERS,nom),
    optGenAux(AUX,PERS,present), optNegation(POL),
    verb2English(have,inf,inf,_),  ifInverted(MOOD,NP,PERS,nom),
    optGenMiddleAdv(ADV),
    verb2English(VERB,past(participle),inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS);
  ifStraight(MOOD,NP,PERS,nom), verb2English(have,present,PERS,_),
    optNegation(POL), ifInverted(MOOD,NP,PERS,nom),
    optGenMiddleAdv(ADV),
    verb2English(VERB,past(participle),inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS)).

presentpastCont(present(continuous),present).
presentpastCont(past(continuous),past).

%declarative/interrogative present/past continuous
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,
           MODE,POL,TENSE,PERS)-->
  {presentpastCont(TENSE,TENSE0)},
  {kindOfDir(MODE), kindOfPolarity(POL)}, ifStraight(MODE,NP,PERS,nom),
  ({AUX=''}->
    verb2English(be,TENSE0,PERS,_), optNegation(POL),
    ifInverted(MODE,NP,PERS,nom), optGenMiddleAdv(ADV),
    verb2English(VERB,present(participle),inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS);
  optGenAux(AUX,PERS,TENSE0), optNegation(POL),
    verb2English(be,inf,inf,_),
    ifInverted(MODE,NP,PERS,nom), optGenMiddleAdv(ADV),
    verb2English(VERB,present(participle),inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS)).

perfectContinuous(present(perfect,continuous),present).
perfectContinuous(past(perfect,continuous),past).
%declarative present continuous
%I have/had been eating an apple
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,MODE,POL,
          TENSE,PERS)--> {perfectContinuous(TENSE,TENSE0)}, !,
  {kindOfDir(MODE), kindOfPolarity(POL)}, ifStraight(MODE,NP,PERS,nom),
  ({AUX=''}->
      verb2English(have,TENSE0,PERS,_), optNegation(POL),
    verb2English(be,past(participle),PERS,_),
    ifInverted(MODE,NP,PERS,nom), optGenMiddleAdv(ADV),
    verb2English(VERB,present(participle),inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS);
  optGenAux(AUX,PERS,TENSE0), optNegation(POL),
    verb2English(have,inf,inf,_),
    verb2English(be,past(participle),PERS,_),
    ifInverted(MODE,NP,PERS,nom), optGenMiddleAdv(ADV),
    verb2English(VERB,present(participle),inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS)).

%%%%%%%%%% ACTIVE PAST
%declarative simple past w/o auxiliary verb
%'He wrote this book.'
vp2English(VERB,NP,ARGUMENTS,'',ADV,
           straight,+,past,PERS)-->
  callNT(nps2English(NP,PERS,nom)),
  optGenMiddleAdv(ADV),
  verb2English(VERB,past,PERS,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES),ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
%declarative simple past w auxiliary verb
%'He did not write this book.' ... 'He could not write this book.'
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,
           straight,+,past,PERS)-->
  {AUX \= ''},
  callNT(nps2English(NP,PERS,nom)), aux2English(AUX,past,PERS),
  optGenMiddleAdv(ADV), verb2English(VERB,inf,inf,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
%interrogative simple past with 'be'
vp2English(be,NP,ARGUMENTS,AUX,ADV,
           DIR,+,past,PERS)-->
  {once(DIR==inverted;DIR==opposite)},
  auxVerb2English(AUX,be,past,PERS),
  ifInverted(DIR,NP,PERS,nom),
  optGenMiddleAdv(ADV), {FRAMES=[?(acc)]},
  ({AUX \= ''}->verb2English(be,inf,inf,_), ifOpposite(DIR,NP,PERS,nom),
    opt1Args2English(FRAMES,ARGUMENTS);
    ifOpposite(DIR,NP,PERS,nom), opt1Args2English(FRAMES,ARGUMENTS)).
%interrogative simple past (other than 'be')
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,
           inverted,POL,past,PERS)-->
  {VERB\=be},
  ({AUX \= ''}->
  %'Could he write this book?'
    auxVerb2English(AUX,do,past,PERS), optNegation(POL),
    callNT(nps2English(NP,PERS,nom)),
    optGenMiddleAdv(ADV),
    verb2English(VERB,inf,inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS);
  %'Did he write this book?'
  auxVerb2English(AUX,do,past,PERS), optNegation(POL),
    callNT(nps2English(NP,PERS,nom)),
    optGenMiddleAdv(ADV),
    verb2English(VERB,inf,inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS)).
%declarative/interrogative simple negative past
%'we could not'
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,MODE,-,past,PERS)-->
  {kindOfDir(MODE)}, ifStraight(MODE,NP,PERS,nom),
  auxVerb2English(AUX,do,past,PERS), [not], ifInverted(MODE,NP,PERS,nom),
  optGenMiddleAdv(ADV),
  verb2English(VERB,inf,inf,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
%declarative/interrogative past perfect w or wo aux
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,
           DIR,POL,past(perfect),PERS)-->
  {kindOfDir(DIR), kindOfPolarity(POL)},
    ({AUX \= ''}->ifStraight(DIR,NP,PERS,nom),
    optGenAux(AUX,PERS,past), optNegation(POL),
    ifInverted(DIR,NP,PERS,nom), verb2English(have,inf,inf,_),
    optGenMiddleAdv(ADV),
    verb2English(VERB,past(participle),inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS);
  ifStraight(DIR,NP,PERS,nom), verb2English(have,past,PERS,_),
    ifInverted(DIR,NP,PERS,nom), optGenMiddleAdv(ADV),
    verb2English(VERB,past(participle),inf,VFRAMES),
    {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
    args2English(FRAMES,ARGUMENTS)).


%%%%%%%%%% ACTIVE FUTURE
%declarative/interrogative simple future
vp2English(VERB,NP,ARGUMENTS,AUX,ADV,
           DIR,POL,future,PERS)-->
  {kindOfDir(DIR), kindOfPolarity(POL)}, ifStraight(DIR,NP,PERS,nom),
  verb2English(be,future,PERS,_), optNegation(POL),
  ifInverted(DIR,NP,PERS,nom), optGenMiddleAdv(ADV),
  aux2English(AUX,inf,inf),
  verb2English(VERB,inf,inf,VFRAMES), ifOpposite(DIR,NP,PERS,nom),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
%declarative/interrogative future perfect
vp2English(VERB,NP,ARGUMENTS,_AUX,ADV,
           DIR,POL,future(perfect),PERS)-->
  {kindOfDir(DIR), kindOfPolarity(POL)}, ifStraight(DIR,NP,PERS,nom),
  verb2English(be,future,PERS,_), optNegation(POL),
  ifInverted(DIR,NP,PERS,nom), optGenMiddleAdv(ADV),
  verb2English(have,inf,inf,_),
  verb2English(VERB,past(participle),inf,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
%declarative/interrogative future continuous
vp2English(VERB,NP,ARGUMENTS,_AUX,ADV,
           DIR,POL,future(continuous),PERS)-->
  {kindOfDir(DIR), kindOfPolarity(POL)}, ifStraight(DIR,NP,PERS,nom),
  verb2English(be,future,PERS,_), optNegation(POL),
  ifInverted(DIR,NP,PERS,nom), optGenMiddleAdv(ADV),
  verb2English(be,inf,inf,_),
  verb2English(VERB,present(participle),inf,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).
%declarative/interrogative future perfect continuous
vp2English(VERB,NP,ARGUMENTS,
           _AUX,ADV,DIR,POL,future(perfect,continuous),PERS)-->
  {kindOfDir(DIR), kindOfPolarity(POL)}, ifStraight(DIR,NP,PERS,nom),
  verb2English(be,future,PERS,_), optNegation(POL),
  ifInverted(DIR,NP,PERS,nom), optGenMiddleAdv(ADV),
  verb2English(have,inf,inf,_), verb2English(be,past(participle),inf,_),
  verb2English(VERB,present(participle),inf,VFRAMES),
  {ignore([nom|FRAMES]=VFRAMES), ignore(FRAMES=[])},
  args2English(FRAMES,ARGUMENTS).

%
% PASSIVE SIMPLE PRESENT/PAST
%
presentpast(present).
presentpast(past).
%declarative/straight - interrogative/inverted
vp2English(V,NP,ARGUMENTS,'',ADV,
           DIR,POL,passive(TENSE),PERS)-->
  {presentpast(TENSE)}, !, {kindOfDir(DIR), kindOfPolarity(POL)},
  ifStraight(DIR,NP,PERS,nom), verb2English(be,TENSE,PERS,_),
  optNegation(POL), ifInverted(DIR,NP,PERS,nom), optGenMiddleAdv(ADV),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).
%declarative/straight - interrogative/inverted with modal verb
vp2English(V,NP,ARGUMENTS,AUX,ADV,
           DIR,POL,passive(TENSE),PERS)-->
  {presentpast(TENSE)}, !, {kindOfDir(DIR), kindOfPolarity(POL)},
  ifStraight(DIR,NP,PERS,nom), aux2English(AUX,TENSE,PERS),
  optNegation(POL),
  ifInverted(DIR,NP,PERS,nom),
  optGenMiddleAdv(ADV), verb2English(be,inf,inf,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).
%
% PASSIVE SIMPLE PRESENT/PAST CONTINUOUS
%
%presentpastCont(present(continuous),present).
%presentpastCont(past(continuous),past).

%declarative/straight
vp2English(V,NP,ARGUMENTS,'',ADV,
           straight,POL,passive(TENSE),PERS)-->
  {presentpastCont(TENSE,BETENSE)}, !,
  callNT(nps2English(NP,PERS,nom)), verb2English(be,BETENSE,PERS,_),
  optNegation(POL),    optGenMiddleAdv(ADV),
  verb2English(be,present(participle),PERS,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).
%interrogative/inverted
vp2English(V,NP,ARGUMENTS,'',ADV,
           inverted,POL,passive(TENSE),PERS)-->
  {presentpastCont(TENSE,BETENSE)}, !,
  verb2English(be,BETENSE,PERS,_), optNegation(POL),
  callNT(nps2English(NP,PERS,nom)),     optGenMiddleAdv(ADV),
  verb2English(be,present(participle),PERS,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).


presentpastPerfect(present(perfect),present).
presentpastPerfect(past(perfect),past).

%
% PASSIVE SIMPLE PRESENT/PAST CONTINUOUS
%
%declarative/straight
vp2English(V,NP,ARGUMENTS,'',ADV,
           straight,POL,passive(TENSE),PERS)-->
  {presentpastPerfect(TENSE,HATENSE)}, !,
  callNT(nps2English(NP,PERS,nom)), verb2English(have,HATENSE,PERS,_),
  optNegation(POL),     optGenMiddleAdv(ADV),
  verb2English(be,past(participle),_,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).
%interrogative/inverted
vp2English(V,NP,ARGUMENTS,'',ADV,
           inverted,POL,passive(TENSE),PERS)-->
  {presentpastPerfect(TENSE,HATENSE)}, !,
  verb2English(have,HATENSE,PERS,_), optNegation(POL),
  callNT(nps2English(NP,PERS,nom)),
      optGenMiddleAdv(ADV),
  verb2English(be,past(participle),_,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).

%
% PASSIVE PRESENT/PAST PERFECT CONTINUOUS
%
presentpastPerfectCont(present(perfect,continuous),present).
presentpastPerfectCont(past(perfect,continuous),past).
%declarative/straight
vp2English(V,NP,ARGUMENTS,'',ADV,
           straight,POL,passive(TENSE),PERS)-->
  {presentpastPerfectCont(TENSE,HATENSE)}, !,
  callNT(nps2English(NP,PERS,nom)), verb2English(have,HATENSE,PERS,_),
  optNegation(POL),    optGenMiddleAdv(ADV),
  verb2English(be,past(participle),_,_),
  verb2English(be,present(participle),_,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).
%interrogative/inverted
vp2English(V,NP,ARGUMENTS,'',ADV,
           inverted,POL,passive(TENSE),PERS)-->
  {presentpastPerfectCont(TENSE,HATENSE)}, !,
  verb2English(have,HATENSE,PERS,_), optNegation(POL),
  callNT(nps2English(NP,PERS,nom)),
      optGenMiddleAdv(ADV),
  verb2English(be,past(participle),_,_),
  verb2English(be,present(participle),_,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).


%
% PASSIVE SIMPLE FUTURE
%
%declarative/straight
vp2English(V,NP,ARGUMENTS,'',ADV,
           straight,POL,passive(future),PERS)-->
  !,
  callNT(nps2English(NP,PERS,nom)), verb2English(be,future,PERS,_),
  optNegation(POL),     optGenMiddleAdv(ADV),
  verb2English(be,inf,_,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).
%interrogative/inverted
vp2English(V,NP,ARGUMENTS,'',ADV,
           inverted,POL,passive(future),PERS)-->
  !,
  verb2English(be,future,PERS,_), optNegation(POL),
  callNT(nps2English(NP,PERS,nom)),     optGenMiddleAdv(ADV),
  verb2English(be,inf,_,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).

%
% PASSIVE FUTURE PERFECT CONTINUOUS
%
%declarative/straight
%the apple will be eaten by me.
vp2English(V,NP,ARGUMENTS,'',ADV,
           straight,POL,passive(future(perfect,continuous)),PERS)-->
  !,
  callNT(nps2English(NP,PERS,nom)), verb2English(be,future,PERS,_),
  optNegation(POL), optGenMiddleAdv(ADV),
  verb2English(be,inf,_,_),
  verb2English(be,present(participle),_,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).
%interrogative/inverted
%will the apple be eaten by me?
vp2English(V,NP,ARGUMENTS,'',ADV,
           inverted,POL,passive(future(perfect,continuous)),PERS)-->
  !,
  verb2English(be,future,PERS,_), optNegation(POL),
  callNT(nps2English(NP,PERS,nom)), optGenMiddleAdv(ADV),
  verb2English(be,inf,_,_),
  verb2English(be,present(participle),_,_),
  verb2English(V,past(participle),_,[nom,acc|FRAMES]),
  opt1Args2English([?(by(acc))|FRAMES],ARGUMENTS).


ip2ipp(interr(WH,num,NP,NUMPERS),'',interr(WH,num,NP,NUMPERS)):- !.
ip2ipp(interr(WH,CASE),PRE,interr(WH,KINDX)):- KINDX=..[PRE,CASE],
   once(CASE=nom;CASE=acc), !.
ip2ipp(interr(WH,CASE),'',interr(WH,CASE)):-
   once(CASE=nom;CASE=acc), !.


%interrogative phrase to English
ip2English(interr(WH,num,NP,NUM),CASE)--> {is_list(WH)}, !,
    WH, np2English(NP,NUM,_GENDER,CASE).
%how quick car (do you have?)
ip2English(interr(WH,CASE,NP,NUM),CASE)--> {atom(CASE)}, !,
    [WH], np2English(NP,NUM,_GENDER,CASE).
%under which tree (did you sit)?
ip2English(interr(WH,CASE,NP,NUM),CASE)--> {CASE=..[PREP,NPCASE]}, !,
    [PREP, WH], np2English(NP,NUM,_GENDER,NPCASE).
ip2English(interr(WH,ADJP,adj),adj)--> !,
    [WH], adj2English(ADJP).
%how long (have you been waiting for her?)
ip2English(interr(WH,adv(ADV,KIND,base),adv(KIND)),adv(KIND))-->
    {atom(ADV)}, !, [WH,ADV].
ip2English(interr(WH,adv(KIND)),adv(KIND))--> {atom(KIND)}, !, [WH].
%to whom (did you send a letter?)
ip2English(interr(WH,_NUM,CASE),CASE)-->
    {CASE=..[PREP,_CASE0]}, !, [PREP,WH].
%whom (did you see?)
ip2English(interr(WH,_NUM,CASE),CASE)-->  !, [WH].
ip2English(interr(WH,KIND),KIND)--> {nonvar(KIND), functor(KIND,PREP,1)},
   !, [PREP,WH].
ip2English(interr(WH,KIND),KIND)--> {atom(WH)}, !, [WH].
ip2English(interr(WH,KIND),KIND)--> {is_list(WH)}, !, WH.


isItAreThere(sg)-->[is,it].
isItAreThere(pl)-->[are,there].


%vp2english//6
%vp2English(+VERB,+NPS,+ARGS,+DIR,+POL,+TENSE)-->
% +DIR: straight;inverted
% -REMFRAMES: not yet processed tail of frame list


%
%VP... wrappers
%
%vp2English//5
%vp2English(VP,NPS,DIR,PERSNUM,POL)
%POL: +;-
%DIR: straight;inverted
%
%coordinated list of noun phrases
%general case and renaming to vp2English//11
vp2English(coordinating(_CONJ,[VP]),NPS,DIR,PERSNUM,POL)-->
  !, vp2English(VP,NPS,DIR,PERSNUM,POL).
vp2English(coordinating(CONJ,[VP|VPS]),
           NPS,DIR,PERSNUM,POL)--> !,
 vp2English(VP,NPS,DIR,PERSNUM,POL), [CONJ],
   vp2English(coordinating(CONJ,VPS),'',DIR,PERSNUM,POL).

%wrapper to generate verb particle at the end
%change to a special particle - frames are particle dependent
%V-?(PART) dummy particle wrt frames must be fetched...
vp2English(vp(verb(VL0,TENSE,PERSNUM),ADV,VMODE,TENSE,ARGS,FREE),
           NPS,DIR,PERSNUM,POL)--> {append(V1,[V^PART],VL0)}, !,
           {append(V1,[V- ?(PART)],VL)},
    vp2English(vp(verb(VL,TENSE,PERSNUM),ADV,VMODE,TENSE,ARGS,[]),
           NPS,DIR,PERSNUM,POL), [PART], frees2English(FREE).

%general case and renaming to vp2English//11
vp2English(vp(verb([VERB],TENSE,_),ADV,VMODE,TENSE,ARGUMENTS,FREE),
           NPS,DIR,PERSNUM,POL)--> !,
  vp2English(vp(verb(VERB,TENSE,_),ADV,VMODE,TENSE,ARGUMENTS,FREE),
           NPS,DIR,PERSNUM,POL).
vp2English(vp(verb([AUX|VL],TENSE,PERSNUM),ADV,VMODE,TENSE,ARGS,FREE),
           NPS,DIR,PERSNUM,POL)--> {last(VL,V)}, !,
 ({isModal(AUX)}->
     vp2English(V,NPS,ARGS,AUX,ADV,DIR,POL,TENSE,PERSNUM),
     frees2English(FREE);
 {V==''}->
     vp2English(V,NPS,ARGS,AUX,ADV,DIR,POL,TENSE,PERSNUM),
     frees2English(FREE);
 vp2English(V,NPS,ARGS,'',ADV,DIR,POL,TENSE,PERSNUM),
     frees2English(FREE)).

vp2English(vp(verb(VERB,TENSE,_),ADV,VMODE,TENSE,ARGUMENTS,FREE),
           NPS,DIR,PERSNUM,POL)--> !,
 ({isModal(VERB)}->
    vp2English('',NPS,ARGUMENTS,VERB,ADV,DIR,POL,TENSE,PERSNUM),
    frees2English(FREE);
  vp2English(VERB,NPS,ARGUMENTS,'',ADV,DIR,POL,TENSE,PERSNUM),
    frees2English(FREE)) .

%general case and renaming to vp2English//11
vp2English(vp(VERB,ADV,VMODE,TENSE,ARGUMENTS,FREE),
           NPS,DIR,PERSNUM,POL)-->
 vp2English(VERB,NPS,ARGUMENTS,'',ADV,DIR,POL,TENSE,PERSNUM),
 frees2English(FREE) .
%short auxiliary word instead of VP
vp2English(AUX,NPS,DIR,PERSNUM,POL)--> !, {atom(AUX)},
   vp2English('',NPS,[],AUX,'',DIR,POL,_TENSE,PERSNUM).


clause2English(wh(WH),interrogative(wh))--> {atom(WH)}, !, [WH].
clause2English(wh(WH),interrogative(wh))--> {is_list(WH)}, !, WH.
clause2English(interr(WH,NP),interrogative(wh))--> !,
  ip2English(interr(WH,NP),_KIND), isItAreThere(sg).
clause2English(interr(WH,NP,NUM),interrogative(wh))--> !,
  ip2English(interr(WH,NP,NUM),_KIND), isItAreThere(NUM).
clause2English(?(wh(what),clause(interrogative(wh),DIR,FREE,_,VP,NUM/3,POL)),
               interrogative(wh))--> !,
 vp2English(VP,wh(what),DIR,NUM/3,POL), frees2English(FREE).
clause2English(?(wh(who),clause(_/interrogative(wh),DIR,FREE,_,VP,NUM/3,POL)),
               interrogative(wh))--> !,
  vp2English(VP,wh(who),DIR,NUM/3,POL), frees2English(FREE).
clause2English(?(IPP,clause(_/interrogative(about),
                            inverted,FREE,NPS,_VP,NUMPERS,+)),
   interrogative(about))-->
  !, ip2English(IPP,_CASE),  np2English(NPS,NUMPERS,_G,acc), frees2English(FREE).
  %when verbal argument is questioned
%'Whom did you write a letter to?' (preposition at the end)
%'who are you?' (who: predicate, pendent argument for 'be'
clause2English(?(IPP,CL),interrogative(KIND))-->
  {sub_var(IPP,CL), CL=clause(_/interrogative(KIND),DIR,FREE,NPS,VP,NUMPERS,POL)},
  {once(KIND=wh;KIND=subj)},
  {ip2ipp(IP,PRE,IPP)}, !,
  ip2English(IP,_), frees2English(FREE),
  vp2English(VP,NPS,DIR,NUMPERS,POL),
  ({PRE == ''} -> []; [PRE]).
%'Who wrote this book?' - the subject is questioned
clause2English(?(IP,clause(_/interrogative(subj),DIR,FREE,NPS,VP,NUMPERS,POL)),
               interrogative(subj))-->
  ip2English(IP,CASE), {ignore(CASE=nom)}, !, frees2English(FREE),
  vp2English(VP,NPS,DIR,NUMPERS,POL).
%'Who is Baby Roo's mother?' - the predicate is questioned
clause2English(?(IP,clause(_/interrogative(pred),inverted,[],NPS,VP,NUMPERS,POL)),
               interrogative(pred))-->
  ip2English(IP,CASE), {ignore(CASE=nom)}, !,
  vp2English(VP,NPS,inverted,NUMPERS,POL).
%'When did he write this book?'
clause2English(?(IP,clause(_/interrogative(wh),DIR,FREE,NPS,VP,NUMPERS,POL)),
               interrogative(wh))--> !,
  ip2English(IP,_), frees2English(FREE),
  vp2English(VP,NPS,DIR,NUMPERS,POL).
clause2English(?(WH,clause(_/interrogative(wh),
                           DIR,FREE,'',VP,NUMPERS,POL)),interrogative(wh))-->
  !, vp2English(VP,wh(WH),DIR,NUMPERS,POL),
  frees2English(FREE).
clause2English(?(clause(_/interrogative(yn),
                        DIR,FREE,NPS,VP,NUMPERS,POL)),interrogative(yn))-->
  !, vp2English(VP,NPS,DIR,NUMPERS,POL),
  frees2English(FREE).
clause2English(?(CLAUSE),interrogative(INTER))--> !,
 clause2English(CLAUSE,interrogative(INTER)).
clause2English(clause(_/KIND,DIR,FREE,NPS,PRED,NUMPERS,POL),KIND)-->
  frees2English(FREE), vp2English(PRED,NPS,DIR,NUMPERS,POL), !.
clause2English(interj(IJ),declarative)--> !,
  interj2English(IJ).

addr2English(proper(PROPER,CLASS,NUM,CASE))-->
   proper2English(PROPER,CLASS,NUM,CASE).


:- op(1050,yfx,<-).

sentence2English(preclause(ADDR,CL))-->
  !, addr2English(ADDR), [','],
  clause2English(CL,KIND), {senType2Punct(KIND,PUNCT)}, [PUNCT].
sentence2English(postclause(CL,POSTCL))-->
  !, clause2English(CL,KIND), [','],
  (addr2English(POSTCL)-> {senType2Punct(KIND,PUNCT)}, [PUNCT];
  {POSTCL=interj(IJ)}, interj2English(IJ), {senType2Punct(KIND,PUNCT)}, [PUNCT];
  clause2English(POSTCL,_)).
sentence2English(questag(',',CL1,CL2))-->
  !, clause2English(CL1,declarative), [','],
  clause2English(CL2,declarative), ['?'].
sentence2English(connected(ADV,CL))-->
  adv2English(ADV), !, [','],
  clause2English(CL,KIND), {senType2Punct(KIND,PUNCT)}, [PUNCT].
sentence2English(correlated(CONN,CL1,CL2))-->
  {correlative(CONN,C1,straight,C2,straight)}, !,
  [C1], clause2English(CL1,declarative), [C2],
  clause2English(CL2,declarative), ['.'].
sentence2English(correlated(CONN,CL1,CL2))-->
  {correlative(CONN,C1,inverse,C2,straight)}, !,
  [C1], clause2English(CL1,declarative), [','], [C2],
  clause2English(CL2,declarative), ['.'].
sentence2English(correlated(CONN,CL1,CL2))-->
  {correlative(CONN,C1,C2,inverse,C3,straight)}, !,
  [C1,C2], clause2English(CL1,declarative), [C3],
  clause2English(CL2,declarative), ['.'].
sentence2English(compound(CONN,CL1,CL2))-->
  clause2English(CL1,declarative), !,
  conn2English(CONN), sentence2English(CL2).
sentence2English(complex(CONN,MAIN->SUB))-->
  !, clause2English(MAIN,KIND),
  conn2English(CONN), clause2English(SUB,declarative),
  {once(senType2Punct(KIND,PUNCT))}, [PUNCT].
sentence2English(complex(CONN,SUB<-MAIN))-->
  !, conn2English(CONN), clause2English(SUB,declarative), [','],
  clause2English(MAIN,KIND),
  {once(senType2Punct(KIND,PUNCT))}, [PUNCT].
sentence2English(incomplete(INTERR,KIND))--> !, ip2English(INTERR,_),
    {once(senType2Punct(KIND,PUNCT))}, [PUNCT].
sentence2English(CLAUSE)-->
  clause2English(CLAUSE,KIND), {once(senType2Punct(KIND,PUNCT))}, [PUNCT].

senType2Punct(interrogative(_),'?').
senType2Punct(declarative,'.').
senType2Punct(exclamatory,'!').
senType2Punct(imperative(strong),'!').
senType2Punct(imperative(weak),'.').

%!  toSentence(+TREE:expr)// is nondet.
% The DCG procedure generates an English text (list of words) from the
% parameter syntax tree. It must be called through the phrase/2..3
% wrapper
%
toSentence(TREE)-->
  sentence2English(TREE), ! .

%!  toSentence(+TREE:expr,ENGLISH:list) is nondet.
% The Prolog procedure generates an English text (list of words) from
% the parameter syntax tree. It can be called directly from
% Prolog
%
% @arg TREE the syntax tree/dict of syntax tree
% @arg ENGLISH a list of English words
toSentence(DICT,ENGLISH):- is_dict(DICT), dict2pl(enParser:dict,DICT,TREE),
  toSentence(TREE,ENGLISH), ! .
toSentence(TREE,ENGLISH):-
   toSentence(TREE,ENGLIST0,[]), tokenTrans(ENGLIST0,ENGLIST),
   atomics_to_string(ENGLIST,ENGLISH),
   debug(genEnglish,'EngSentence:~w\n',[ENGLISH]).



vowel('a').
vowel('e').
vowel('o').
vowel('i').

tokenTran([],[]):- !.
tokenTran([TOKEN],[TOKEN]):- ! .
tokenTran([a,WORD|TOKENS],[an,' '|TOKENLIST]):-
   sub_atom(WORD,0,1,_,FIRST), vowel(FIRST), !,
   tokenTran([WORD|TOKENS],TOKENLIST).
tokenTran([T10,T20,T30|TOKENS],[T1|TOKENLIST]):-
   transcribe(T1,T2,T10,T20,T30), !,
   tokenTran([T2|TOKENS],TOKENLIST).
tokenTran([T10,T20|TOKENS],[T1|TOKENLIST]):-
   transcribe(T1,T2,T10,T20), !,
   tokenTran([T2|TOKENS],TOKENLIST).
tokenTran([TOKEN,PUNCT],[TOKEN,PUNCT]):-
   punct(PUNCT), \+ punctWordTag(PUNCT), !.
tokenTran([TOKEN,PUNCT|TOKENS],[TOKEN,PUNCT,' '|TOKENLIST]):-
   punct(PUNCT), \+ punctWordTag(PUNCT), !,
   tokenTran(TOKENS,TOKENLIST).
tokenTran([TOKEN,PUNCT|TOKENS],[TOKEN,PUNCT|TOKENLIST]):-
   punctWordTag(PUNCT), !, tokenTran(TOKENS,TOKENLIST).
tokenTran([TOKEN0,TOKEN10|TOKENS],[TOKEN0,' ',TOKEN1|TOKENLIST]):- !,
   tokenTran([TOKEN10|TOKENS],[TOKEN1|TOKENLIST]).

capitalize(T,TCAP):-
   sub_string(T,0,1,_,FIRST), atom_concat(FIRST,REST,T),
   string_upper(FIRST,FIRSTUP), atom_concat(FIRSTUP,REST,TCAP).

tokenTrans([T00,T10|TOKENS],[T0CAP,T1,' '|TOKENLIST]):-
   transcribe(T0,T1,T00,T10), !, capitalize(T0,T0CAP),
   tokenTran(TOKENS,TOKENLIST).
tokenTrans([T0|TOKENS],TOKENLIST):-
   capitalize(T0,T0CAP), tokenTran([T0CAP|TOKENS],TOKENLIST).


start:-
  S=[the,quick,',',brown,fox,jumps,over,the,lazy,dog,'.'],
  start(S).

start(STRING,CALL):- string(STRING), !,
  parser:string_to_tokens(STRING,TOKENS),
  parser:tokens2Line(SENTENCE,TOKENS,[]),
  start(SENTENCE,CALL).
start(S,CALL):- is_list(S), !, %later the complete forest!!
  parseThisSentence(S,_TREES,CALL).
start(TESTFILE,CALL):-
  enParser:enParseTestfile(TESTFILE,CALL).

start(PARM):- start(PARM,\=).



















